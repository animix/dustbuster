<?php

$servername = "localhost";
$username = "root";
$pass = "claveEye3##";
$dbname = "dustbuster";


$startdate = '2018-07-30 00:00:00';
$enddate   = '2018-08-06 00:00:00';

// Volcado de promedios
echo "polygon_id;pm100".PHP_EOL;
//echo "polygon_id;pm100;fechahora".PHP_EOL;

try {
	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $pass);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$sql = "SELECT polygon_id, horasalida, pm10 from evento_mapa where horasalida >= '". $startdate ."' and horasalida < '". $enddate ."'".
		"and tipoevento=1 and polygon_id is not NULL";
	$results = [];

	// Captura de datos brutos
	foreach ($conn->query($sql) as $row){
		$polygon_id = $row['polygon_id'];
		$horasalida = $row['horasalida'];
		$pm10       = $row['pm10'];

		$results[$polygon_id][] = $pm10;

		// Volcado de datos brutos
		//echo $polygon_id.";".round($pm10).";".$horasalida.PHP_EOL;
	}
	

        
	// Calculo de promedios
	$finalresults = [];
	foreach ($results as $key => $values){
		$average = array_sum($values) / count($values);
		$finalresults[] = [$key, $average];

		// csv output
		echo $key.";".round($average).PHP_EOL;
	}
        

} catch(PDOException $e) {
	echo $sql.PHP_EOL.$e->getMessage();
}
