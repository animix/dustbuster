<?php
$u = time('now');
$d = DateTime::createFromFormat('U', $u);

// Switch to false in production environment
$dry_run = false;

function publishFile($file){
	global $dry_run;

	$d = new DateTime();
	$cmd = "sudo mv \"".$file."\" ../web/images/";
	echo "[".$d->format("d-m-Y H:i:s")."] publishFile(): '". $cmd ."'".PHP_EOL;
	if ($dry_run == false){
		system($cmd);
		system("/usr/bin/james-spence \"publishFile(): ".$file."\"");
	}
}

// Main loop
$fotos = glob('../web/upload/QB*');
foreach ($fotos as $foto){
	publishFile($foto);
}
//echo "".PHP_EOL;


require 'batch.php';

?>
