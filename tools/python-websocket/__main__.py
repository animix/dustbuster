from SimpleWebSocketServer import SimpleSSLWebSocketServer, WebSocket
import _thread
import time

counter = 0

def dataRefreshThread():
    while True:
        global counter
        counter = counter + 1
        print(counter)
        time.sleep(1)


class SimpleEcho(WebSocket):

    def handleMessage(self):
        global counter
        # echo message back to client
        self.sendMessage("%s (%d)" % (self.data, counter))

    def handleConnected(self):
        print(self.address, 'connected')

    def handleClose(self):
        print(self.address, 'closed')



_thread.start_new_thread(dataRefreshThread, ())

server = SimpleSSLWebSocketServer('', 8000, SimpleEcho, 
	'/etc/letsencrypt/archive/report.eye3.cl/fullchain9.pem',
	'/etc/letsencrypt/archive/report.eye3.cl/privkey9.pem')


server.serveforever()

