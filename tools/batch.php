<?php
/*
 * batch.php : batch processing of gzipped sql dump files
 * USAGE: php-cli batch.php
 */


// Mind the final slash/ in all paths
//
$dumpFilePath = "../web/upload/";
$tmpFilePath = "./tmp/";
$doneFilePath = "./done/";
$mysql_user = "root";  // <-- fill in
$mysql_pass = "claveEye3##";  // <-- fill in
$mysql_db   = "dustbuster";  // <-- fill in

function findGpsIdBase( $text ){
    $blocks = explode('`id_maleta`) VALUES (', $text);
    $line   = $blocks[1];
    $blocks = explode("'", $line);
    return substr($blocks[0], 0, -1);
}

function applyRegexps( $sql, $dumpFileName, $processedFileName )
{
    $gpsid_base = findGpsIdBase($sql);

    // reemplazando en gpsdata
    $gpsdata = preg_replace("/id_maleta`\) VALUES \([0-9]+,'/",
                            "id_maleta`) VALUES (@gps:=@gps+1,'",
                            $sql);

    // reemplazando en pmdata
    $pmdata = preg_replace("/recorrido`\) VALUES \([0-9]+,/",
                           "recorrido`) VALUES (@id:=@id+1,",
                           $gpsdata);


    // reemplazando gpsid en sightdata -- dumps con NULLbug
    $nullbugdata = preg_replace('/NULL,NULL,(\d+)\)/',
                                "NULL,NULL,$1+@gpsidbase)",
                                $pmdata);

    // reemplazando gpsid en photodata
    $photodata = preg_replace("/comment`\) VALUES \((\d+),'/",
                              "comment`) VALUES ($1+". $gpsid_base ."1+@gpsidbase,'",
                              $nullbugdata);

    // reemplazando gpsid en sightdata -- dumps sin NULLbug
    $sightdata = preg_replace("/(\d+:\d+:\d+','\d+',\d+)/",
                              "$1-". $gpsid_base ."+1+@gpsidbase",
                              $photodata);

    // reemplazando sightdata.id en sightdata
    $final = preg_replace("/\((\d+),/",
                          "(@sightid:=@sightid+1,",
                          $sightdata);

    // El encabezado que captura el gpsdata.id mas alto para empezar a sumar
    $newsql = "-- gpsdata shows initial id [". $gpsid_base ."], will offset sightdata/photodata accordingly \n".
              "SELECT @id:= COALESCE(MAX(id),0) FROM gpsdata;\n".
              "SELECT @sightid:= COALESCE(MAX(id),0) FROM sightdata;\n".
              "SET @gps= @id;\n".
              "SET @sight= @id;\n".
              "SET @gpsidbase= @id;\n".
              "SET @iniciomonitoreo= @id+1;\n\n";

    $footersql = "\nSELECT @fechainiciomonitoreo:= fecha FROM gpsdata WHERE id=@iniciomonitoreo;\n".
                 "\nSELECT @finmonitoreo:= COALESCE(MAX(id),0) FROM gpsdata;\n".
                 "--INSERT INTO monitoreo(gpsid_inicial, gpsid_final, nombre, fecha, dump, preprocesado) VALUES (@iniciomonitoreo, @finmonitoreo, CONCAT('Monitoreo del ',@iniciomonitoreo,' al ',@finmonitoreo), @fechainiciomonitoreo, '". $dumpFileName ."','". $processedFileName ."');\n";
    return $newsql.$final.$footersql;
}


function systemcmd( $cmd ) {
    $d = new DateTime();
    echo "[".$d->format("d-m-Y H:i:s")."] <batch.php> Executing [$cmd]".PHP_EOL;
    system( $cmd ); // comment for dry-run testing
}


// main()

// Collect full path for all files ending with ".sql.gz"
//
$allfiles = scandir( $dumpFilePath );
$dumpFiles = [];
foreach( $allfiles as $file ) {
    if( stripos( strrev( $file ), 'zg.lqs.') === 0 ) {
        // [fullpath + filename, filename]
        $dumpFiles[] = [$dumpFilePath.$file, $file];
    }
}

// No files? Why bother?
if ($dumpFiles == []){
    //$d = new DateTime();
    //echo "[".$d->format("d-m-Y H:i:s")."] <batch.php> No dumpfiles, nothing to do.".PHP_EOL;
	//echo "CWD is [".getcwd()."]".PHP_EOL;
    die();
}

// For each dump file: gunzip, apply regexp, deploy into mysql.
//
$count = 0;
$countProcessed = 0;
foreach( $dumpFiles as $dumpFile)
{
    $gunzippedFile = substr( $dumpFile[1], 0, -3 );
    $processedFile = preg_replace( "/MONITOR/", "MONITOR-PROCESSED", $gunzippedFile );

    // Only confirmed dumps reach the database. Discarded dumps
    // are simply moved away without any further processing.
    //
    if( stripos( $dumpFile[1], 'MONITOR' ) === 0 )
    {
        // Copy dump file to tmp path, then gunzip
        systemcmd( "cp $dumpFile[0] ". $tmpFilePath.$dumpFile[1] );
        systemcmd( "gunzip ". $tmpFilePath.$dumpFile[1] );

        // Apply regexp to gunzipped file, save in a new file
        $sql = file_get_contents( $tmpFilePath.$gunzippedFile );

        $newSql = applyRegexps( $sql, $gunzippedFile .".gz", $processedFile );
        file_put_contents( $tmpFilePath.$processedFile, $newSql );

        // Deploy new file into mysql
        systemcmd( "mysql -u ".$mysql_user." -p".$mysql_pass." ".$mysql_db." < ".$tmpFilePath.$processedFile );
        $countProcessed++;
    }

    // Move original gzipped dump to a new location
    systemcmd( "mv $dumpFile[0] ".$doneFilePath );
    echo PHP_EOL;
    $count++;
}

$d = new DateTime();
echo "[".$d->format("d-m-Y H:i:s")."] ".$countProcessed." confirmed dumps processed".PHP_EOL;
echo "[".$d->format("d-m-Y H:i:s")."] ".($count - $countProcessed)." dumps ignored (discarded?)".PHP_EOL;
echo "[".$d->format("d-m-Y H:i:s")."] ".$count." dumps moved to ".$doneFilePath.PHP_EOL.PHP_EOL;
system("/usr/bin/james-spence \"batch.php: ".$d->format("d-m-Y H:i:s")." -- ". $countProcessed ." confirmed dumps processed, ".($count - $countProcessed)." dumps ignored, ".$count." dumps moved to ".$doneFilePath."\"");

?>

