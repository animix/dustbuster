import MySQLdb
import sys
import math
from geopy.distance import great_circle, vincenty

DB_HOST = 'localhost'
DB_NAME = 'dustbuster'
DB_USER = 'root'
DB_PASS = 'claveEye3##'


def run_query( query = "" ):
	conn = MySQLdb.connect( *[DB_HOST, DB_USER, DB_PASS, DB_NAME] )
	cursor = conn.cursor()
	cursor.execute( query )

	if query.upper().startswith('SELECT'):
		data = cursor.fetchall()
	else:
		conn.commit()
		data = None

	cursor.close()
	conn.close()

	return data


query  = 'SELECT * from monitoreo where id = %d' % int(sys.argv[1])
result = run_query(query)[0]
uuid   = result[7]

query    = 'SELECT latitude, longitude from gpsdata where latitude!=0 and id_sesion = "%s" and speed > 10 order by fecha' % (uuid)
result   = run_query(query)

# result contains the whole point set at thid point
distance = 0
lastpoint = result[0]

for newpoint in result:
	#d = vincenty(newpoint, lastpoint, ellipsoid='GRS-67').meters
	d = vincenty(lastpoint, newpoint).meters

	if d >= 0.1:
		distance = distance + d
	lastpoint = newpoint

## ERASE: Originally meant to populate initial database
query = 'UPDATE monitoreo SET distancia=%d WHERE id=%d' % (int(round(distance)), int(sys.argv[1]))
result = run_query(query)
print query
## ERASE

print int(round(distance))
