<?php
$servername = "localhost";
$username = "root";
$pass = "claveEye3##";
$dbname = "dustbuster";

function splitDateRange($daterange){
            if ($daterange == null){
                    $startDate = new DateTime();
                    $endDate = new DateTime();
            } else {
                    // Expecting "YYYY-MM-DD"
                    $range = explode(',', $daterange);
                    $startDate = new DateTime($range[0]);
                    if (array_key_exists(1, $range)){
                            $endDate = new DateTime($range[1]);
                    } else {
                            $endDate = new DateTime($range[0]);
                    }
            }
            $startDate->setTime(0,0,0);
            $endDate->setTime(23,59,59);
	return array(
		"start" => $startDate,
		"end" => $endDate
	);
}




//$startdate = '2018-09-03 00:00:00'; //'2018-07-23 00:00:00';
//$enddate   = '2018-09-10 00:00:00'; //'2018-07-30 00:00:00';
//$daterange['start'] = new DateTime($startdate);
//$daterange['end'] = new DateTime($enddate);

$daterange = splitDaterange($_GET["fechas"]);
$startdate = $daterange["start"]->format("Y-m-d ").' 00:00:00';
$enddate = $daterange["end"]->format("Y-m-d ").' 00:00:00';


header('Content-type: text/csv');
header('Content-Disposition: attachment; filename="regadores-en-area-pala-'.
	$daterange['start']->format("Y-m-d").'-'.$daterange['end']->format("Y-m-d").'.csv";');

$fh = @fopen( 'php://output', 'w' );

//header('Content-Disposition: attachment; filename="regadores-en-area-pala.csv";');


/*
echo "<br>startdate:[". $startdate ."]<br>".PHP_EOL;
echo "<br>enddate:[". $enddate ."]<br>".PHP_EOL;
*/

// Volcado de promedios
//echo "polygon_id;pm100".PHP_EOL;
//$sql = "SELECT id, geofence, vehicle, event, date, registered from geofence_data where date >= '". $startdate ."' and date < '". $enddate ."' ".
//                "and event=2 and geofence=59 order by id desc";


// Solia ser el geocerco 59, ahora es el geocerco 62
$sql = "SELECT * FROM `geofence_data` where (vehicle=56 or vehicle=85 or vehicle=86 or vehicle=122) and event=2 and date between '".
	$startdate ."' and '". $enddate ."' and geofence=62 order by id desc";

try {
	$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $pass);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// Paso 1: identificar todas las salidas
//	$sql = "SELECT id, geofence, vehicle, event, date, registered from geofence_data where date >= '". $startdate ."' and date < '". $enddate ."' ".
//		"and event=2 and geofence=59 order by id desc";
		

	$results = [];
	//echo "geofence,truck_id,enter,exit,seconds".PHP_EOL;
	fputcsv($fh, ["geofence","truck_id","enter","exit","seconds"]);
	foreach ($conn->query($sql) as $row){
		$id = $row['id'];
		$geofence = $row['geofence'];
		$vehicle = $row['vehicle'];
		$event = $row['event'];
		$date = $row['date'];
		$registered = $row['registered'];

		$results[] = $row;

		// buscar el ingreso anterior a la salida mas reciente, cuadrando geocerco y vechiculo
		$sql = "SELECT id,geofence, vehicle, event, date, registered from geofence_data where geofence=".$geofence." and vehicle=".$vehicle." and event=1 and date < '".$date ."' order by id desc limit 1";
		foreach ($conn->query($sql) as $match){
			$fechaingreso = $match['date'];
		}

		// geofence, truck_id, enter, exit, seconds
		$seconds = strtotime($date) - strtotime($fechaingreso);
		fputcsv($fh, [$geofence, $vehicle, $fechaingreso, $date, $seconds]);
		//echo $geofence .",". $vehicle .",". $fechaingreso .",". $date .",". $seconds.PHP_EOL;

	}
	//echo count($results)." results".PHP_EOL;

        /*
	// Calculo de promedios
	$finalresults = [];
	foreach ($results as $key => $values){
		$average = array_sum($values) / count($values);
		$finalresults[] = [$key, $average];

		// csv output
		echo $key.";".round($average).PHP_EOL;
	}
        */

} catch(PDOException $e) {
	//echo $sql.PHP_EOL.$e->getMessage();
}

	fclose($fh);
?>
