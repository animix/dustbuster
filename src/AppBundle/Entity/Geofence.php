<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Geofence
 *
 * @ORM\Table(name="geofence")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GeofenceRepository")
 */
class Geofence implements JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="pattern", type="string", length=16, nullable=true)
     */
    private $pattern;

    /**
     * @var polygon
     *
     * @ORM\Column(name="geometry", type="polygon", nullable=true)
     */
    private $geometry;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=16, nullable=true)
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz", nullable=true)
     */
    private $date;

    /**
     * @var integer
     * @ORM\Column(name="timestamp", type="integer", nullable=true)
     */
    private $timestamp;

    /**
     * @var boolean
     * @ORM\Column(name="rtr", type="boolean", nullable=true)
     */
    private $rtr;

    /**
     * @var boolean
     * @ORM\Column(name="rtc", type="boolean", nullable=true);
     */
    private $rtc;

    /**
     * @var boolean
     * @ORM\Column(name="ltr", type="boolean", nullable=true);
     */
    private $ltr;

    /**
     * @var boolean
     * @ORM\Column(name="ltc", type="boolean", nullable=true);
     */
    private $ltc;


    public function jsonSerialize(){
        $latLngArray = $this->geometry->getRing(0)->toArray();
        return array(
            "id" => $this->id,
            "name" => $this->name,
            "pattern" => $this->pattern,
            "type" => $this->type,
            "date" => $this->date->format("Y-m-d H:i:s"),
            "timestamp" => $this->timestamp,
            "geometry" => $latLngArray,
            "rtr" => $this->rtr,
            "rtc" => $this->rtc,
            "ltc" => $this->ltc,
            "ltr" => $this->ltr
        );
    }

    /**
     * Get geometry
     */
    public function getGeoJsonArray($properties){
        $latLngArray = $this->geometry->getRing(0)->toArray();
        $area_properties = [
            "area_id" => $this->id,
            "name" => $this->name,
            "pattern" => $this->pattern,
            "type" => $this->type,
            "date" => $this->date->format("Y-m-d H:i:s"),
            "timestamp" => $this->timestamp,
            "rtr" => $this->rtr,
            "rtc" => $this->rtc,
            "ltr" => $this->ltr,
            "ltc" => $this->ltc
        ];
        return array(
            "type" => "Feature",
            "geometry" => array(
                "type" => "Polygon",
                "coordinates" => array($latLngArray)
            ),
            "properties" => array_merge($properties, $area_properties)
        );
    }

    public function getSQLInsert(){
        $verts = $this->geometry->getRing(0)->toArray();
        $sql = "INSERT INTO geofence (id,name,type,pattern,date,geometry,rtr,rtc,ltc,ltr) VALUES(".
            $this->id.",'".
            $this->name ."','".
            $this->type ."','".
            $this->pattern ."','".
            $this->date->format("Y-m-d H:i:s") ."', GeomFromText('POLYGON((";

	if ($this->rtr == false){ 
		$rtr = 0;
	} else {
		$rtr = 1;
	}
	if ($this->rtc == false){
		$rtc = 0;
	} else {
		$rtc = 1;
	}
	if ($this->ltr == false){
		$ltr = 0;
	} else {
		$ltr = 1;
	}
	if ($this->ltc == false){
		$ltc = 0;
	} else {
		$ltc = 1;
	}


        foreach ($verts as $lnglat){
            $sql = $sql. $lnglat[0] ." ". $lnglat[1]. ",";
        }
        $sql = rtrim($sql, ",");
        $sql = $sql ."))'),".
		$rtr. ",".
		$rtc. ",".
		$ltc. ",".
		$ltr. ");".PHP_EOL;
        return $sql;
    }

    public function getKml(){
        $verts = $this->geometry->getRing(0)->toArray();

        $coordinates = "";
        foreach ($verts as $lnglat){
            $coordinates = $coordinates. $lnglat[0] .",". $lnglat[1] .",0".PHP_EOL;
        }
        return '<Placemark>'.PHP_EOL.
            '       <name>'. $this->name. ' (id '. $this->id .', '. $this->type .')</name>'.PHP_EOL.
            '       <styleUrl>#moving</styleUrl>'.PHP_EOL.
            '       <description></description>'.PHP_EOL.
            '       <Polygon>'.PHP_EOL.
            '               <outerBoundaryIs>'.PHP_EOL.
            '                       <LinearRing>'.PHP_EOL.
            '                               <coordinates>'.PHP_EOL. $coordinates.
            '                               </coordinates>'.PHP_EOL.
            '                       </LinearRing>'.PHP_EOL.
            '               </outerBoundaryIs>'.PHP_EOL.
            '       </Polygon>'.PHP_EOL.
            '</Placemark>'.PHP_EOL;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return IrrigationArea
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set pattern
     *
     * @param string $pattern
     *
     * @return IrrigationArea
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;

        return $this;
    }

    /**
     * Get pattern
     *
     * @return string
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * Set geometry
     *
     * @param polygon $geometry
     *
     * @return IrrigationArea
     */
    public function setGeometry($geometry)
    {
        $this->geometry = $geometry;

        return $this;
    }

    /**
     * Get geometry
     *
     * @return polygon
     */
    public function getGeometry()
    {
        return $this->geometry;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return IrrigationArea
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return IrrigationArea
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}

