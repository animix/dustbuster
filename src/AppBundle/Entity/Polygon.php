<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Polygon
 *
 * @ORM\Table(name="polygon")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PolygonRepository")
 */
class Polygon implements JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=60, nullable=true)
     */
    private $name;

    /**
     * @var polygon
     *
     * @ORM\Column(name="geometry", type="polygon", nullable=true)
     */
    private $geometry;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Polygon
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set geometry
     *
     * @param polygon $geometry
     * @return Polygon
     */
    public function setGeometry($geometry)
    {
        $this->geometry = $geometry;
        return $this;
    }

    /**
     * Get geometry
     *
     * @return polygon 
     */
    public function getGeometry()
    {
        return $this->geometry;
    }

    /**
     * Get geometry
     */
    public function getGeoJsonArray($properties){
		$latLngArray = $this->geometry->getRing(0)->toArray();

		return array(
			"type" => "Feature",
			"geometry" => array(
				"type" => "Polygon",
				"coordinates" => array($latLngArray)
			),
			"properties" => $properties
		);
	}

	public function jsonSerialize(){
		$latLngArray = $this->geometry->getRing(0)->toArray();
		return array(
			"id" => $this->id,
			"name" => $this->name,
			"geometry" => $latLngArray
		);
	}

	public function getSQLInsert(){
		$verts = $this->geometry->getRing(0)->toArray();
		$sql = "INSERT INTO polygon (id,geometry) VALUES(".
			$this->id.", GeomFromText('POLYGON((";

		foreach ($verts as $lnglat){
			$sql = $sql. $lnglat[0] ." ". $lnglat[1]. ",";
		}
		$sql = rtrim($sql, ",");
		$sql = $sql ."))'));".PHP_EOL;
		return $sql;
	}

	public function getKml(){
		$verts = $this->geometry->getRing(0)->toArray();

		$coordinates = "";
		foreach ($verts as $lnglat){
			$coordinates = $coordinates. $lnglat[0] .",". $lnglat[1] .",0".PHP_EOL;
		}
		return '<Placemark>'.PHP_EOL.
                                        '       <name>'.$this->id.'</name>'.PHP_EOL.
                                        '       <styleUrl>#moving</styleUrl>'.PHP_EOL.
                                        '       <description></description>'.PHP_EOL.
                                        '       <Polygon>'.PHP_EOL.
					'               <id>'.$this->id.'</id>'.PHP_EOL.
                                        '               <outerBoundaryIs>'.PHP_EOL.
                                        '                       <LinearRing>'.PHP_EOL.
                                        '                               <coordinates>'.PHP_EOL. $coordinates.
                                        '                               </coordinates>'.PHP_EOL.
                                        '                       </LinearRing>'.PHP_EOL.
                                        '               </outerBoundaryIs>'.PHP_EOL.
                                        '       </Polygon>'.PHP_EOL.
                                        '</Placemark>'.PHP_EOL;

	}
}
