<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * PolygonVersionTimestamp
 *
 * @ORM\Table(name="polygon_version_timestamp")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PolygonVersionTimestampRepository")
 */
class PolygonVersionTimestamp implements JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, nullable=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="timestamp", type="bigint", nullable=true)
     */
    private $timestamp;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return VersionTimestamp
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set timestamp
     *
     * @param integer $timestamp
     *
     * @return VersionTimestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return int
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }


	public function JsonSerialize(){
		return [
			"name" => $this->name,
			"timestamp" => $this->timestamp
		];
	}
}

