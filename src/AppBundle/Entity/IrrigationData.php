<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IrrigationData
 *
 * @ORM\Table(name="irrigation_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IrrigationDataRepository")
 */
class IrrigationData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

   /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetimetz", nullable=true)
     */
    private $datetime;


    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="float", nullable=true)
     */
    private $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="lon", type="float", nullable=true)
     */
    private $lon;

    /**
     * @var float
     *
     * @ORM\Column(name="tasa", type="float", nullable=true)
     */
    private $tasa;

    /**
     * @var float
     *
     * @ORM\Column(name="tasareal", type="float", nullable=true)
     */
    private $tasareal;

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float", nullable=true)
     */
    private $total;

    /**
     * @var int
     *
     * @ORM\Column(name="camion", type="integer", nullable=true)
     */
    private $idcamion;

    /**
     * @var int
     *
     * @ORM\Column(name="p1", type="integer", nullable=true)
     */
    private $p1;

    /**
     * @var int
     *
     * @ORM\Column(name="p2", type="integer", nullable=true)
     */
    private $p2;

    /**
     * @var int
     *
     * @ORM\Column(name="p3", type="integer", nullable=true)
     */
    private $p3;

    /**
     * @var float
     *
     * @ORM\Column(name="fl", type="float", nullable=true)
     */
    private $fl;

    /**
     * @var int
     *
     * @ORM\Column(name="pos", type="integer", nullable=true)
     */
    private $pos;

    /**
     * @var float
     *
     * @ORM\Column(name="delta_tiempo", type="integer", nullable=true)
     */
    private $deltatiempo;

    /**
     * @var float
     *
     * @ORM\Column(name="vel", type="float", nullable=true)
     */
    private $vel;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return IrrigationData
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        return $this;
    }

    /**
     * Get lat
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }



    /**
     * Set lat
     *
     * @param float $lat
     *
     * @return IrrigationData
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lon
     *
     * @param float $lon
     *
     * @return IrrigationData
     */
    public function setLon($lon)
    {
        $this->lon = $lon;

        return $this;
    }

    /**
     * Get lon
     *
     * @return float
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Set tasa
     *
     * @param float $tasa
     *
     * @return IrrigationData
     */
    public function setTasa($tasa)
    {
        $this->tasa = $tasa;

        return $this;
    }

    /**
     * Get tasa
     *
     * @return float
     */
    public function getTasa()
    {
        return $this->tasa;
    }

    /**
     * Set tasareal
     *
     * @param float $tasareal
     *
     * @return IrrigationData
     */
    public function setTasareal($tasareal)
    {
        $this->tasareal = $tasareal;

        return $this;
    }

    /**
     * Get tasareal
     *
     * @return float
     */
    public function getTasareal()
    {
        return $this->tasareal;
    }

    /**
     * Set total
     *
     * @param float $total
     *
     * @return IrrigationData
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * Get total
     *
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set idcamion
     *
     * @param integer $idcamion
     *
     * @return IrrigationData
     */
    public function setIdcamion($idcamion)
    {
        $this->idcamion = $idcamion;

        return $this;
    }

    /**
     * Get idcamion
     *
     * @return int
     */
    public function getIdcamion()
    {
        return $this->idcamion;
    }

    /**
     * Set p1
     *
     * @param integer $p1
     *
     * @return IrrigationData
     */
    public function setP1($p1)
    {
        $this->p1 = $p1;

        return $this;
    }

    /**
     * Get p1
     *
     * @return int
     */
    public function getP1()
    {
        return $this->p1;
    }

    /**
     * Set p2
     *
     * @param integer $p2
     *
     * @return IrrigationData
     */
    public function setP2($p2)
    {
        $this->p2 = $p2;

        return $this;
    }

    /**
     * Get p2
     *
     * @return int
     */
    public function getP2()
    {
        return $this->p2;
    }

    /**
     * Set p3
     *
     * @param integer $p3
     *
     * @return IrrigationData
     */
    public function setP3($p3)
    {
        $this->p3 = $p3;

        return $this;
    }

    /**
     * Get p3
     *
     * @return int
     */
    public function getP3()
    {
        return $this->p3;
    }

    /**
     * Set fl
     *
     * @param float $fl
     *
     * @return IrrigationData
     */
    public function setFl($fl)
    {
        $this->fl = $fl;

        return $this;
    }

    /**
     * Get fl
     *
     * @return float
     */
    public function getFl()
    {
        return $this->fl;
    }

    /**
     * Set pos
     *
     * @param integer $pos
     *
     * @return IrrigationData
     */
    public function setPos($pos)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos
     *
     * @return int
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Set vel
     *
     * @param float $vel
     *
     * @return IrrigationData
     */
    public function setVel($vel)
    {
        $this->vel = $vel;

        return $this;
    }

    /**
     * Get vel
     *
     * @return float
     */
    public function getVel()
    {
        return $this->vel;
    }


    /**
     * Set deltatiempo
     *
     * @param int $deltatiempo
     *
     * @return IrrigationData
     */
    public function setDeltatiempo($deltatiempo)
    {
        $this->deltatiempo = $deltatiempo;

        return $this;
    }

    /**
     * Get deltatiempo
     *
     * @return int
     */
    public function getDeltatiempo()
    {
        return $this->deltatiempo;
    }


    /**
     * set datetime
     * @return IrrigationData
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

}

