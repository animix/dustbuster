<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * StationData
 *
 * @ORM\Table(name="station_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StationDataRepository")
 */
class StationData implements JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="station", type="integer", nullable=true)
     */
    private $station;

    /**
     * @var \DateTime
     * @ORM\Column(name="date", type="datetimetz", nullable=true)
     */
    private $date;

    /**
     * @var \DateTime
     * @ORM\Column(name="received", type="datetimetz", nullable=true)
     */
    private $received;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", nullable=true)
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", nullable=true)
     */
    private $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="humidity", type="float", nullable=true)
     */
    private $humidity;

    /**
     * @var float
     *
     * @ORM\Column(name="temperature", type="float", nullable=true)
     */
    private $temperature;

    /**
     * @var float
     *
     * @ORM\Column(name="pressure", type="float", nullable=true)
     */
    private $pressure;

    /**
     * @var float
     *
     * @ORM\Column(name="windspeed", type="float", nullable=true)
     */
    private $windspeed;

    /**
     * @var int
     *
     * @ORM\Column(name="winddirection", type="integer", nullable=true)
     */
    private $winddirection;

    /**
     * @var float
     *
     * @ORM\Column(name="pm10pcc", type="float", nullable=true)
     */
    private $pm10pcc;

    /**
     * @var float
     *
     * @ORM\Column(name="pm25pcc", type="float", nullable=true)
     */
    private $pm25pcc;

    /**
     * @var float
     *
     * @ORM\Column(name="pm1pcc", type="float", nullable=true)
     */
    private $pm1pcc;


    /**
     * @var float
     *
     * @ORM\Column(name="pm10ugm3", type="float", nullable=true)
     */
    private $pm10ugm3;


    /**
     * @var float
     *
     * @ORM\Column(name="pm25ugm3", type="float", nullable=true)
     */
    private $pm25ugm3;


    /**
     * @var float
     *
     * @ORM\Column(name="pm1ugm3", type="float", nullable=true)
     */
    private $pm1ugm3;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set received
     * @param \DateTime $r
     * @return StationData
     */
    public function setReceived($r){
        $this->received = $r;
        return $this;
    }

    /**
     * Get date
     * @return \DateTime
     */
    public function getReceived(){
        return $this->received;
    }


    /**
     * Set date
     * @param \DateTime $date
     * @return StationData
     */
    public function setDate($d){
        $this->date = $d;
        return $this;
    }

    /**
     * Get date
     * @return \DateTime
     */
    public function getDate(){
        return $this->date;
    }


    /**
     * Set station
     *
     * @param integer $station
     *
     * @return StationData
     */
    public function setStation($station)
    {
        $this->station = $station;
        return $this;
    }


    /**
     * Get station
     *
     * @return int
     */
    public function getStation()
    {
        return $this->station;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return StationData
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return StationData
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set humidity
     *
     * @param float $humidity
     *
     * @return StationData
     */
    public function setHumidity($humidity)
    {
        $this->humidity = $humidity;

        return $this;
    }

    /**
     * Get humidity
     *
     * @return float
     */
    public function getHumidity()
    {
        return $this->humidity;
    }

    /**
     * Set temperature
     *
     * @param float $temperature
     *
     * @return StationData
     */
    public function setTemperature($temperature)
    {
        $this->temperature = $temperature;

        return $this;
    }

    /**
     * Get temperature
     *
     * @return float
     */
    public function getTemperature()
    {
        return $this->temperature;
    }

    /**
     * Set pressure
     *
     * @param float $pressure
     *
     * @return StationData
     */
    public function setPressure($pressure)
    {
        $this->pressure = $pressure;

        return $this;
    }

    /**
     * Get pressure
     *
     * @return float
     */
    public function getPressure()
    {
        return $this->pressure;
    }

    /**
     * Set windspeed
     *
     * @param float $windspeed
     *
     * @return StationData
     */
    public function setWindspeed($windspeed)
    {
        $this->windspeed = $windspeed;

        return $this;
    }

    /**
     * Get windspeed
     *
     * @return float
     */
    public function getWindspeed()
    {
        return $this->windspeed;
    }

    /**
     * Set winddirection
     *
     * @param integer $winddirection
     *
     * @return StationData
     */
    public function setWinddirection($winddirection)
    {
        $this->winddirection = $winddirection;

        return $this;
    }

    /**
     * Get winddirection
     *
     * @return int
     */
    public function getWinddirection()
    {
        return $this->winddirection;
    }

    /**
     * Set pm10pcc
     *
     * @param float $pm10pcc
     *
     * @return StationData
     */
    public function setPm10pcc($pm10pcc)
    {
        $this->pm10pcc = $pm10pcc;
        return $this;
    }

    /**
     * Get pm10pcc
     *
     * @return float
     */
    public function getPm10pcc()
    {
        return $this->pm10pcc;
    }

    /**
     * Set pm25pcc
     *
     * @param float $pm25pcc
     *
     * @return StationData
     */
    public function setPm25pcc($pm25pcc)
    {
        $this->pm25pcc = $pm25pcc;
        return $this;
    }

    /**
     * Get pm25pcc
     *
     * @return float
     */
    public function getPm25pcc()
    {
        return $this->pm25pcc;
    }

    /**
     * Set pm1pcc
     * @param float $pm1pcc
     * @return StationData
     */
    public function setPm1pcc($pm1pcc)
    {
        $this->pm1pcc = $pm1pcc;
        return $this;
    }

    /**
     * Get pm1pcc
     * @return float
     */
    public function getPm1pcc()
    {
        return $this->pm1pcc;
    }

    /**
     * Set pm10ugm3
     * @param float $pm10ugm3
     * @return StationData
     */
    public function setPm10ugm3($pm10ugm3)
    {
        $this->pm10ugm3 = $pm10ugm3;
        return $this;
    }

    /**
     * Get pm10ugm3
     * @return float
     */
    public function getPm10ugm3()
    {
        return $this->pm10ugm3;
    }


    /**
     * Set pm25ugm3
     * @param float $pm25ugm3
     * @return StationData
     */
    public function setPm25ugm3($pm25ugm3)
    {
        $this->pm25ugm3 = $pm25ugm3;
        return $this;
    }

    /**
     * Get pm25ugm3
     * @return float
     */
    public function getPm25ugm3()
    {
        return $this->pm25ugm3;
    }

    /**
     * Set pm1ugm3
     * @param float $pm1ugm3
     * @return StationData
     */
    public function setPm1ugm3($pm1ugm3)
    {
        $this->pm1ugm3 = $pm1ugm3;
        return $this;
    }

    /**
     * Get pm1ugm3
     * @return float
     */
    public function getPm1ugm3()
    {
        return $this->pm1ugm3;
    }








    public function JsonSerialize(){
        return [
            "id" => $this->id,
            "station" => $this->station,
            "date" => $this->date,
            "received" => $this->received,
            "latitude" => $this->latitude,
            "longitude" => $this->longitude,
            "temperature" => $this->temperature,
            "pressure" => $this->pressure,
            "humidity" => $this->humidity,
            "windspeed" => $this->windspeed,
            "winddirection" => $this->winddirection,
            "pm1_pcc" => $this->pm1pcc,
            "pm10_pcc" => $this->pm10pcc,
            "pm25_pcc" => $this->pm25pcc,
            "pm1_ugm3" => $this->pm1ugm3,
            "pm10_ugm3" => $this->pm10ugm3,
            "pm25_ugm3" => $this->pm25ugm3
        ];
    }
}


