<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * CampData
 *
 * @ORM\Table(name="camp_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CampDataRepository")
 */
class CampData implements JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="vehicle", type="integer", nullable=true)
     */
    private $vehicle;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", nullable=true)
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", nullable=true)
     */
    private $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="speed", type="float", nullable=true)
     */
    private $speed;

    /**
     * @var float
     *
     * @ORM\Column(name="distance", type="float", nullable=true)
     */
    private $distance;

    /**
     * @var float
     *
     * @ORM\Column(name="pm10pcc", type="float", nullable=true)
     */
    private $pm10pcc;

    /**
     * @var float
     *
     * @ORM\Column(name="pm25pcc", type="float", nullable=true)
     */
    private $pm25pcc;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz", nullable=true)
     */
    private $date;


    public function JsonSerialize(){
        return [
            "id" => $this->id,
            "date" => $this->date->format("Y-m-d H:i:s"),
            "truck" => $this->vehicle,
            "lat" => $this->latitude,
            "lon" => $this->longitude,
            "speed" => $this->speed,
            "dist" => $this->distance,
            "pm10" => $this->pm10pcc,
            "pm25" => $this->pm25pcc
        ];
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vehicle
     *
     * @param integer $vehicle
     *
     * @return CampData
     */
    public function setVehicle($vehicle)
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    /**
     * Get vehicle
     *
     * @return int
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return CampData
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return CampData
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set speed
     *
     * @param float $speed
     *
     * @return CampData
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * Get speed
     *
     * @return float
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * Set distance
     *
     * @param float $distance
     *
     * @return CampData
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * Get distance
     *
     * @return float
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set pm10pcc
     *
     * @param float $pm10pcc
     *
     * @return CampData
     */
    public function setPm10pcc($pm10pcc)
    {
        $this->pm10pcc = $pm10pcc;

        return $this;
    }

    /**
     * Get pm10pcc
     *
     * @return float
     */
    public function getPm10pcc()
    {
        return $this->pm10pcc;
    }

    /**
     * Set pm25pcc
     *
     * @param float $pm25pcc
     *
     * @return CampData
     */
    public function setPm25pcc($pm25pcc)
    {
        $this->pm25pcc = $pm25pcc;

        return $this;
    }

    /**
     * Get pm25pcc
     *
     * @return float
     */
    public function getPm25pcc()
    {
        return $this->pm25pcc;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return CampData
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}

