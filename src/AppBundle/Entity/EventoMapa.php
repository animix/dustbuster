<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * EventoMapa
 *
 * @ORM\Table(name="evento_mapa")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventoMapaRepository")
 */
class EventoMapa implements JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\Polygon
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Polygon")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="polygon_id", referencedColumnName="id")
     * })
     */
    private $idpoligono;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="idrecorrido", type="object", nullable=true)
     */
    private $idrecorrido;

    /**
     * @var \stdClass
     *
     * @ORM\Column(name="idvehiculo", type="object", nullable=true)
     */
    private $idvehiculo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="horaregistro", type="datetimetz", nullable=true)
     */
    private $horaregistro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="horaingreso", type="datetimetz", nullable=true)
     */
    private $horaingreso;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="horasalida", type="datetimetz", nullable=true)
     */
    private $horasalida;

    /**
     * @var float
     *
     * @ORM\Column(name="tasaideal", type="float", nullable=true)
     */
    private $tasaideal;

    /**
     * @var float
     *
     * @ORM\Column(name="tasaalcanzada", type="float", nullable=true)
     */
    private $tasaalcanzada;

    /**
     * @var float
     *
     * @ORM\Column(name="velocidadpromedio", type="float", nullable=true)
     */
    private $velocidadpromedio;

    /**
     * @var float
     * @ORM\Column(name="distanciatotal", type="float", nullable=true)
     */
    private $distanciatotal;

    /**
     * @var float
     * @ORM\Column(name="distanciaregada", type="float", nullable=true)
     */
    private $distanciaregada;

    /**
     * @var float
     *
     * @ORM\Column(name="totalregado", type="float", nullable=true)
     */
    private $totalregado;

    /**
     * @var float
     *
     * @ORM\Column(name="pm10", type="float", nullable=true)
     */
    private $pm10;

    /**
     * @var float
     *
     * @ORM\Column(name="pm25", type="float", nullable=true)
     */
    private $pm25;

    /**
     * @var int
     *
     * @ORM\Column(name="tipoevento", type="integer", nullable=true)
     */
    private $tipoevento;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idpoligono
     *
     * @param \AppBundle\Entity\Polygon $idpoligono
     *
     * @return EventoMapa
     */
    public function setIdpoligono($idpoligono)
    {
        $this->idpoligono = $idpoligono;

        return $this;
    }

    /**
     * Get idpoligono
     *
     * @return \AppBundle\Entity\Polygon
     */
    public function getIdpoligono()
    {
        return $this->idpoligono;
    }

    /**
     * Set idrecorrido
     *
     * @param \stdClass $idrecorrido
     *
     * @return EventoMapa
     */
    public function setIdrecorrido($idrecorrido)
    {
        $this->idrecorrido = $idrecorrido;

        return $this;
    }

    /**
     * Get idrecorrido
     *
     * @return \stdClass
     */
    public function getIdrecorrido()
    {
        return $this->idrecorrido;
    }

    /**
     * Set idvehiculo
     *
     * @param \stdClass $idvehiculo
     *
     * @return EventoMapa
     */
    public function setIdvehiculo($idvehiculo)
    {
        $this->idvehiculo = $idvehiculo;

        return $this;
    }

    /**
     * Get idvehiculo
     *
     * @return \stdClass
     */
    public function getIdvehiculo()
    {
        return $this->idvehiculo;
    }

    /**
     * Set horaregistro
     *
     * @param \DateTime $horaregistro
     *
     * @return EventoMapa
     */
    public function setHoraregistro($horaregistro)
    {
        $this->horaregistro = $horaregistro;

        return $this;
    }

    /**
     * Get horaregistro
     *
     * @return \DateTime
     */
    public function getHoraregistro()
    {
        return $this->horaregistro;
    }

    /**
     * Set horaingreso
     *
     * @param \DateTime $horaingreso
     *
     * @return EventoMapa
     */
    public function setHoraingreso($horaingreso)
    {
        $this->horaingreso = $horaingreso;

        return $this;
    }

    /**
     * Get horaingreso
     *
     * @return \DateTime
     */
    public function getHoraingreso()
    {
        return $this->horaingreso;
    }

    /**
     * Set horasalida
     *
     * @param \DateTime $horasalida
     *
     * @return EventoMapa
     */
    public function setHorasalida($horasalida)
    {
        $this->horasalida = $horasalida;

        return $this;
    }

    /**
     * Get horasalida
     *
     * @return \DateTime
     */
    public function getHorasalida()
    {
        return $this->horasalida;
    }

    /**
     * Set tasaideal
     *
     * @param float $tasaideal
     *
     * @return EventoMapa
     */
    public function setTasaideal($tasaideal)
    {
        $this->tasaideal = $tasaideal;

        return $this;
    }

    /**
     * Get tasaideal
     *
     * @return float
     */
    public function getTasaideal()
    {
        return $this->tasaideal;
    }


    /**
     * Set distanciatotal
     * @param float $distanciatotal
     * @return EventoMapa
     */
    public function setDistanciatotal($distanciatotal){
        $this->distanciatotal = $distanciatotal;
        return $this;
    }

    /**
     * Get distanciatotal
     * @return float
     */
    public function getDistanciatotal(){ return $this->distanciatotal; }


    /**
     * Set distanciaregada
     * @param float $distanciaregada
     * @return EventoMapa
     */
    public function setDistanciaregada($distanciaregada){
        $this->distanciaregada = $distanciaregada;
        return $this;
    }

    /**
     * Get distanciaregada
     * @return float
     */
    public function getDistanciaregada(){ return $this->distanciaregada; }

    /**
     * Set tasaalcanzada
     *
     * @param float $tasaalcanzada
     *
     * @return EventoMapa
     */
    public function setTasaalcanzada($tasaalcanzada)
    {
        $this->tasaalcanzada = $tasaalcanzada;

        return $this;
    }

    /**
     * Get tasaalcanzada
     *
     * @return float
     */
    public function getTasaalcanzada()
    {
        return $this->tasaalcanzada;
    }

    /**
     * Set velocidadpromedio
     *
     * @param float $velocidadpromedio
     *
     * @return EventoMapa
     */
    public function setVelocidadpromedio($velocidadpromedio)
    {
        $this->velocidadpromedio = $velocidadpromedio;

        return $this;
    }

    /**
     * Get velocidadpromedio
     *
     * @return float
     */
    public function getVelocidadpromedio()
    {
        return $this->velocidadpromedio;
    }

    /**
     * Set totalregado
     *
     * @param float $totalregado
     *
     * @return EventoMapa
     */
    public function setTotalregado($totalregado)
    {
        $this->totalregado = $totalregado;

        return $this;
    }

    /**
     * Get totalregado
     *
     * @return float
     */
    public function getTotalregado()
    {
        return $this->totalregado;
    }

    /**
     * Set pm10
     *
     * @param float $pm10
     *
     * @return EventoMapa
     */
    public function setPm10($pm10)
    {
        $this->pm10 = $pm10;

        return $this;
    }

    /**
     * Get pm10
     *
     * @return float
     */
    public function getPm10()
    {
        return $this->pm10;
    }

    /**
     * Set pm25
     *
     * @param float $pm25
     *
     * @return EventoMapa
     */
    public function setPm25($pm25)
    {
        $this->pm25 = $pm25;

        return $this;
    }

    /**
     * Get pm25
     *
     * @return float
     */
    public function getPm25()
    {
        return $this->pm25;
    }

    /**
     * Set tipoevento
     *
     * @param integer $tipoevento
     *
     * @return EventoMapa
     */
    public function setTipoevento($tipoevento)
    {
        $this->tipoevento = $tipoevento;

        return $this;
    }

    /**
     * Get tipoevento
     *
     * @return int
     */
    public function getTipoevento()
    {
        return $this->tipoevento;
    }

	public function jsonSerialize(){
		//$chile = new \DateTimezone("Etc/GMT+4");
		$horaregistro = $this->horaregistro;
		$horaingreso = $this->horaingreso;
		$horasalida = $this->horasalida;
		//if ($horaregistro != null) $horaregistro->setTimezone($chile);
		//if ($horaingreso != null) $horaingreso->setTimezone($chile);
		//if ($horasalida != null) $horasalida->setTimezone($chile);


		$nivel = $this->tasaideal;
		if ($this->tipoevento == 1){//$pm10 != null){

		$nivel = 0;
		if (($this->pm10 * 1) > 2999){
            $nivel = 1;
		}
        if (($this->pm10 * 1) > 4999){
            $nivel = 2;
        }

		}
		return array(
			"id" => $this->id,
			"tipoevento" => $this->tipoevento,
			"idpoligono" => ($this->idpoligono == null)? null: $this->idpoligono->getId(),
			"idrecorrido" => ($this->idrecorrido == null)? null: $this->idrecorrido->getId(),
			"horaregistro" => ($horaregistro == null)? null: $horaregistro->format('Y-m-d H:i:s'),
			"horaingreso" => ($horaingreso == null)? null: $horaingreso->format('Y-m-d H:i:s'),
			"horasalida" => ($horasalida == null)? null: $horasalida->format('Y-m-d H:i:s'),
			"tasaideal" => $nivel, // HACK: Originalmente tiene que ser $this->tasaideal,
			"tasaalcanzada" => $this->tasaalcanzada,
			"velocidadpromedio" => $this->velocidadpromedio,
			"totalregado" => $this->totalregado,
                        "distanciatotal" => $this->distanciatotal,
                        "distanciaregada" => $this->distanciaregada,
			"pm10" => $this->pm10 * 1,
			"pm25" => $this->pm25,
		);
	}
}


