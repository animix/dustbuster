<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sightdata
 *
 * @ORM\Table(name="sightdata", indexes={@ORM\Index(name="id_gps", columns={"id_gps"})})
 * @ORM\Entity(repositoryClass="AppBundle\Entity\SightdataRepository")
 */
class Sightdata
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=36, nullable=true)
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=false)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="utctime", type="string", length=10, nullable=true)
     */
    private $utctime;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="string", length=6, nullable=true)
     */
    private $date;

    /**
     * @var \Gpsdata
     *
     * @ORM\ManyToOne(targetEntity="Gpsdata")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_gps", referencedColumnName="id")
     * })
     */
    private $idGps;
 
	/**
     * @var boolean
     *
     * @ORM\Column(name="borrado", type="boolean", length=1, nullable=true)
     */
    private $invalido;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime", nullable=true)
     */
    private $fecha;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", precision=10, scale=6, nullable=true)
     */
    private $latitud;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", precision=10, scale=6, nullable=true)
     */
    private $longitud;

    /**
     * Set latitud
     *
     * @param float $latitud
     * @return Sightdata
     */
    public function setLatitud($latitud)
    {
        $this->latitud = $latitud;

        return $this;
    }

    /**
     * Get latitud
     *
     * @return float
     */
    public function getLatitud()
    {
        return $this->latitud;
    }

    /**
     * Set longitud
     *
     * @param float $longitud
     * @return Sightdata
     */
    public function setLongitud($longitud)
    {
        $this->longitud = $longitud;

        return $this;
    }

    /**
     * Get longitud
     *
     * @return float
     */
    public function getLongitud()
    {
        return $this->longitud;
    }


    /**
     * Set fecha
     * @return Sightdata
     */
    public function setFecha(\DateTime $fecha){
        $this->fecha = $fecha;
        return $this;
    }

    /**
     * Get fecha
     * @return \DateTime
     */
    public function getFecha(){
        return $fecha;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Sightdata
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set utctime
     *
     * @param string $utctime
     * @return Sightdata
     */
    public function setUtctime($utctime)
    {
        $this->utctime = $utctime;

        return $this;
    }

    /**
     * Get utctime
     *
     * @return string 
     */
    public function getUtctime()
    {
        return $this->utctime;
    }

    /**
     * Set date
     *
     * @param string $date
     * @return Sightdata
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return string 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set idGps
     *
     * @param \AppBundle\Entity\Gpsdata $idGps
     * @return Sightdata
     */
    public function setIdGps(\AppBundle\Entity\Gpsdata $idGps = null)
    {
        $this->idGps = $idGps;

        return $this;
    }

    /**
     * Get idGps
     *
     * @return \AppBundle\Entity\Gpsdata 
     */
    public function getIdGps()
    {
        return $this->idGps;
    }
	
	/**
     * Set invalido
     *
     * @param boolean $invalido
     * @return Sightdata
     */
    public function setInvalido($invalido)
    {
        $this->invalido = $invalido;

        return $this;
    }

    /**
     * Get invalido
     *
     * @return boolean 
     */
    public function getInvalido()
    {
        return $this->invalido;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Photodata
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

}
