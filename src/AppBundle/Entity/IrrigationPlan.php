<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IrrigationPlan
 *
 * @ORM\Table(name="irrigation_plan")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IrrigationPlanRepository")
 */
class IrrigationPlan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\User
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="timestamp", type="integer", nullable=true)
     */
    private $timestamp;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="blob", nullable=true)
     */
    private $file;

    /**
     * @var bool
     *
     * @ORM\Column(name="deployed", type="boolean", nullable=true)
     */
    private $deployed;


    /**
     * @var bool
     *
     * @ORM\Column(name="completed", type="boolean", nullable=true)
     */
    private $completed;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\User $user
     *
     * @return IrrigationPlan
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set timestamp
     *
     * @param integer $timestamp
     *
     * @return IrrigationPlan
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    /**
     * Get timestamp
     *
     * @return int
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return IrrigationPlan
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set file
     *
     * @param string $file
     *
     * @return IrrigationPlan
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set completed
     *
     * @param boolean $completed
     *
     * @return IrrigationPlan
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;

        return $this;
    }

    /**
     * Get completed
     *
     * @return bool
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * Set deployed
     *
     * @param boolean $deployed
     *
     * @return IrrigationPlan
     */
    public function setDeployed($deployed)
    {
        $this->deployed = $deployed;
        return $this;
    }

    /**
     * Get deployed
     *
     * @return bool
     */
    public function getDeployed()
    {
        return $this->deployed;
    }

}

