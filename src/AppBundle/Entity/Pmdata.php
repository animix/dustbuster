<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pmdata
 *
 * @ORM\Table(name="pmdata", indexes={@ORM\Index(name="id_tramo", columns={"id_tramo"})})
 * @ORM\Entity
 */
class Pmdata
{
    /**
     * @var float
     *
     * @ORM\Column(name="tsplat_d", type="float", precision=10, scale=0, nullable=true)
     */
    private $tsplat_d;

    /**
     * @var float
     *
     * @ORM\Column(name="pm10lat_d", type="float", precision=10, scale=0, nullable=true)
     */
    private $pm10lat_d;

    /**
     * @var float
     *
     * @ORM\Column(name="pm25lat_d", type="float", precision=10, scale=0, nullable=true)
     */
    private $pm25lat_d;

    /**
     * @var float
     *
     * @ORM\Column(name="pm1lat_d", type="float", precision=10, scale=0, nullable=true)
     */
    private $pm1lat_d;

    /**
     * @var float
     *
     * @ORM\Column(name="tspavg_d", type="float", precision=10, scale=0, nullable=true)
     */
    private $tspavg_d;

    /**
     * @var float
     *
     * @ORM\Column(name="pm10avg_d", type="float", precision=10, scale=0, nullable=true)
     */
    private $pm10avg_d;

    /**
     * @var float
     *
     * @ORM\Column(name="pm25avg_d", type="float", precision=10, scale=0, nullable=true)
     */
    private $pm25avg_d;

    /**
     * @var float
     *
     * @ORM\Column(name="pm1avg_d", type="float", precision=10, scale=0, nullable=true)
     */
    private $pm1avg_d;

    //---- Now data for the dustmate_u
    
        /**
     * @var float
     *
     * @ORM\Column(name="tsplat_u", type="float", precision=10, scale=0, nullable=true)
     */
    private $tsplat_u;

    /**
     * @var float
     *
     * @ORM\Column(name="pm10lat_u", type="float", precision=10, scale=0, nullable=true)
     */
    private $pm10lat_u;

    /**
     * @var float
     *
     * @ORM\Column(name="pm25lat_u", type="float", precision=10, scale=0, nullable=true)
     */
    private $pm25lat_u;

    /**
     * @var float
     *
     * @ORM\Column(name="pm1lat_u", type="float", precision=10, scale=0, nullable=true)
     */
    private $pm1lat_u;

    /**
     * @var float
     *
     * @ORM\Column(name="tspavg_u", type="float", precision=10, scale=0, nullable=true)
     */
    private $tspavg_u;

    /**
     * @var float
     *
     * @ORM\Column(name="pm10avg_u", type="float", precision=10, scale=0, nullable=true)
     */
    private $pm10avg_u;

    /**
     * @var float
     *
     * @ORM\Column(name="pm25avg_u", type="float", precision=10, scale=0, nullable=true)
     */
    private $pm25avg_u;

    /**
     * @var float
     *
     * @ORM\Column(name="pm1avg_u", type="float", precision=10, scale=0, nullable=true)
     */
    private $pm1avg_u;

    
    /**
     * @var \AppBundle\Entity\Gpsdata
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Gpsdata")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_gps", referencedColumnName="id")
     * })
     */
    private $idGps;

	 /**
     * @var \AppBundle\Entity\Graficarmapa
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Graficarmapa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tramo", referencedColumnName="id")
     * })
     */
	
    private $idTramo;

	/**
     * @var float
     *
     * @ORM\Column(name="recorrido", type="float", precision=10, scale=0, nullable=true)
     */
    private $recorrido;

    /**
     * Set tsplat_d
     *
     * @param float $tsplat
     * @return Pmdata
     */
    public function setTsplat_d($tsplat_d)
    {
        $this->tsplat_d = $tsplat_d;

        return $this;
    }

    /**
     * Get tsplat_d
     *
     * @return float 
     */
    public function getTsplat_d()
    {
        return $this->tsplat_d;
    }

    /**
     * Set pm10lat_d
     *
     * @param float $pm10lat_d
     * @return Pmdata
     */
    public function setPm10lat_d($pm10lat_d)
    {
        $this->pm10lat_d = $pm10lat_d;
        return $this;
    }

    /**
     * Get pm10lat_d
     *
     * @return float 
     */
    public function getPm10lat_d()
    {
        return $this->pm10lat_d;
    }

    /**
     * Set pm25lat_d
     *
     * @param float $pm25lat_d
     * @return Pmdata
     */
    public function setPm25lat_d($pm25lat_d)
    {
        $this->pm25lat_d = $pm25lat_d;
        return $this;
    }

    /**
     * Get pm25lat_d
     *
     * @return float 
     */
    public function getPm25lat_d()
    {
        return $this->pm25lat_d;
    }

    /**
     * Set pm1lat_d
     *
     * @param float $pm1lat_d
     * @return Pmdata
     */
    public function setPm1lat_d($pm1lat_d)
    {
        $this->pm1lat_d = $pm1lat_d;
        return $this;
    }

    /**
     * Get pm1lat_d
     *
     * @return float 
     */
    public function getPm1lat_d()
    {
        return $this->pm1lat_d;
    }

    /**
     * Set tspavg_d
     *
     * @param float $tspavg_d
     * @return Pmdata
     */
    public function setTspavg_d($tspavg_d)
    {
        $this->tspavg_d = $tspavg_d;

        return $this;
    }

    /**
     * Get tspavg_d
     *
     * @return float 
     */
    public function getTspavg_d()
    {
        return $this->tspavg_d;
    }

    /**
     * Set pm10avg_d
     *
     * @param float $pm10avg_d
     * @return Pmdata
     */
    public function setPm10avg_d($pm10avg_d)
    {
        $this->pm10avg_d = $pm10avg_d;
        return $this;
    }

    /**
     * Get pm10avg_d
     *
     * @return float 
     */
    public function getPm10avg_d()
    {
        return $this->pm10avg_d;
    }

    /**
     * Set pm25avg_d
     *
     * @param float $pm25avg_d
     * @return Pmdata
     */
    public function setPm25avg_d($pm25avg_d)
    {
        $this->pm25avg_d = $pm25avg_d;
        return $this;
    }

    /**
     * Get pm25avg_d
     *
     * @return float 
     */
    public function getPm25avg_d()
    {
        return $this->pm25avg_d;
    }

    /**
     * Set pm1avg_d
     *
     * @param float $pm1avg_d
     * @return Pmdata
     */
    public function setPm1avg_d($pm1avg_d)
    {
        $this->pm1avg = $pm1avg_d;
        return $this;
    }

    /**
     * Get pm1avg_d
     *
     * @return float 
     */
    public function getPm1avg_d()
    {
        return $this->pm1avg_d;
    }
    
    //--  Now data for the dustmate_u
    /**
     * Set tsplat_u
     *
     * @param float $tsplat
     * @return Pmdata
     */
    public function setTsplat_u($tsplat_u)
    {
        $this->tsplat_u = $tsplat_u;

        return $this;
    }

    /**
     * Get tsplat_u
     *
     * @return float 
     */
    public function getTsplat_u()
    {
        return $this->tsplat_u;
    }

    /**
     * Set pm10lat_u
     *
     * @param float $pm10lat_u
     * @return Pmdata
     */
    public function setPm10lat_u($pm10lat_u)
    {
        $this->pm10lat_u = $pm10lat_u;
        return $this;
    }

    /**
     * Get pm10lat_u
     *
     * @return float 
     */
    public function getPm10lat_u()
    {
        return $this->pm10lat_u;
    }

    /**
     * Set pm25lat_u
     *
     * @param float $pm25lat_u
     * @return Pmdata
     */
    public function setPm25lat_u($pm25lat_u)
    {
        $this->pm25lat_u = $pm25lat_u;
        return $this;
    }

    /**
     * Get pm25lat_u
     *
     * @return float 
     */
    public function getPm25lat_u()
    {
        return $this->pm25lat_u;
    }

    /**
     * Set pm1lat_u
     *
     * @param float $pm1lat_u
     * @return Pmdata
     */
    public function setPm1lat_u($pm1lat_u)
    {
        $this->pm1lat_u = $pm1lat_u;
        return $this;
    }

    /**
     * Get pm1lat_u
     *
     * @return float 
     */
    public function getPm1lat_u()
    {
        return $this->pm1lat_u;
    }

    /**
     * Set tspavg_u
     *
     * @param float $tspavg_u
     * @return Pmdata
     */
    public function setTspavg_u($tspavg_u)
    {
        $this->tspavg_u = $tspavg_u;

        return $this;
    }

    /**
     * Get tspavg_u
     *
     * @return float 
     */
    public function getTspavg_u()
    {
        return $this->tspavg_u;
    }

    /**
     * Set pm10avg_u
     *
     * @param float $pm10avg_u
     * @return Pmdata
     */
    public function setPm10avg_u($pm10avg_u)
    {
        $this->pm10avg_u = $pm10avg_u;
        return $this;
    }

    /**
     * Get pm10avg_u
     *
     * @return float 
     */
    public function getPm10avg_u()
    {
        return $this->pm10avg_u;
    }

    /**
     * Set pm25avg_u
     *
     * @param float $pm25avg_u
     * @return Pmdata
     */
    public function setPm25avg_u($pm25avg_u)
    {
        $this->pm25avg_u = $pm25avg_u;
        return $this;
    }

    /**
     * Get pm25avg_u
     *
     * @return float 
     */
    public function getPm25avg_u()
    {
        return $this->pm25avg_u;
    }

    /**
     * Set pm1avg_u
     *
     * @param float $pm1avg_u
     * @return Pmdata
     */
    public function setPm1avg_u($pm1avg_u)
    {
        $this->pm1avg = $pm1avg_u;
        return $this;
    }

    /**
     * Get pm1avg_u
     *
     * @return float 
     */
    public function getPm1avg_u()
    {
        return $this->pm1avg_u;
    }

    
    /**
     * Set idGps
     *
     * @param \AppBundle\Entity\Gpsdata $idGps
     * @return Pmdata
     */
    public function setIdGps(\AppBundle\Entity\Gpsdata $idGps)
    {
        $this->idGps = $idGps;

        return $this;
    }

    /**
     * Get idGps
     *
     * @return \AppBundle\Entity\Gpsdata 
     */
    public function getIdGps()
    {
        return $this->idGps;
    }

    /**
     * Set idTramo
     *
     * @param \AppBundle\Entity\Graficarmapa $idTramo
     * @return Pmdata
     */
    public function setIdTramo(\AppBundle\Entity\Graficarmapa $idTramo = null)
    {
        $this->idTramo = $idTramo;

        return $this;
    }

    /**
     * Get idTramo
     *
     * @return \AppBundle\Entity\Graficarmapa 
     */
    public function getIdTramo()
    {
        return $this->idTramo;
    }
	
	 /**
     * Set recorrido
     *
     * @param float $recorrido
     * @return Pmdata
     */
    public function setRecorrido($recorrido)
    {
        $this->recorrido = $recorrido;

        return $this;
    }

    /**
     * Get recorrido
     *
     * @return float 
     */
    public function getRecorrido()
    {
        return $this->recorrido;
    }
}
