<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * WateringData
 *
 * @ORM\Table(name="watering_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WateringDataRepository")
 */
class WateringData implements JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="vehicle", type="smallint", nullable=true)
     */
    private $vehicle;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", nullable=true)
     */
    private $latitude;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", nullable=true)
     */
    private $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="speed", type="float", nullable=true)
     */
    private $speed;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz", nullable=true)
     */
    private $date;

    /**
     * @var float
     *
     * @ORM\Column(name="distance", type="float", nullable=true)
     */
    private $distance;

    /**
     * @var int
     *
     * @ORM\Column(name="mode", type="smallint", nullable=true)
     */
    private $mode;

    /**
     * @var bool
     *
     * @ORM\Column(name="rtr", type="boolean", nullable=true)
     */
    private $rtr;

    /**
     * @var bool
     *
     * @ORM\Column(name="rtc", type="boolean", nullable=true)
     */
    private $rtc;

    /**
     * @var bool
     *
     * @ORM\Column(name="ltr", type="boolean", nullable=true)
     */
    private $ltr;

    /**
     * @var bool
     *
     * @ORM\Column(name="ltc", type="boolean", nullable=true)
     */
    private $ltc;

    /**
     * @var bool
     *
     * @ORM\Column(name="forbidden", type="boolean", nullable=true)
     */
    private $forbidden;

	
	  public function JsonSerialize(){
        return [
            "id" => $this->id,
            "vehicle" => $this->vehicle,
            "latitude" => $this->latitude,
            "longitude" => $this->longitude,
            "speed" => $this->speed,
            "distance" => $this->distance,
            "mode" => $this->mode,
            "date" => $this->date->format("Y-m-d H:i:s")
        ];
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vehicle
     *
     * @param integer $vehicle
     *
     * @return WateringData
     */
    public function setVehicle($vehicle)
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    /**
     * Get vehicle
     *
     * @return int
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     *
     * @return WateringData
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     *
     * @return WateringData
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set speed
     *
     * @param float $speed
     *
     * @return WateringData
     */
    public function setSpeed($speed)
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * Get speed
     *
     * @return float
     */
    public function getSpeed()
    {
        return $this->speed;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return WateringData
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set distance
     *
     * @param float $distance
     *
     * @return WateringData
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * Get distance
     *
     * @return float
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * Set mode
     *
     * @param integer $mode
     *
     * @return WateringData
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * Get mode
     *
     * @return int
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Set rtr
     *
     * @param boolean $rtr
     *
     * @return WateringData
     */
    public function setRtr($rtr)
    {
        $this->rtr = $rtr;

        return $this;
    }

    /**
     * Get rtr
     *
     * @return bool
     */
    public function getRtr()
    {
        return $this->rtr;
    }

    /**
     * Set rtc
     *
     * @param boolean $rtc
     *
     * @return WateringData
     */
    public function setRtc($rtc)
    {
        $this->rtc = $rtc;

        return $this;
    }

    /**
     * Get rtc
     *
     * @return bool
     */
    public function getRtc()
    {
        return $this->rtc;
    }

    /**
     * Set ltr
     *
     * @param boolean $ltr
     *
     * @return WateringData
     */
    public function setLtr($ltr)
    {
        $this->ltr = $ltr;

        return $this;
    }

    /**
     * Get ltr
     *
     * @return bool
     */
    public function getLtr()
    {
        return $this->ltr;
    }

    /**
     * Set ltc
     *
     * @param boolean $ltc
     *
     * @return WateringData
     */
    public function setLtc($ltc)
    {
        $this->ltc = $ltc;

        return $this;
    }

    /**
     * Get ltc
     *
     * @return bool
     */
    public function getLtc()
    {
        return $this->ltc;
    }

    /**
     * Set forbidden
     *
     * @param boolean $forbidden
     *
     * @return WateringData
     */
    public function setForbidden($forbidden)
    {
        $this->forbidden = $forbidden;

        return $this;
    }

    /**
     * Get forbidden
     *
     * @return bool
     */
    public function getForbidden()
    {
        return $this->forbidden;
    }
}

