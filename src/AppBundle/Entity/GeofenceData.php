<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * GeofenceData
 *
 * @ORM\Table(name="geofence_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GeofenceDataRepository")
 */
class GeofenceData implements JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="vehicle", type="integer", nullable=true)
     */
    private $vehicle;

    /**
     * @var \AppBundle\Entity\Geofence
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Geofence")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="geofence", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $geofence;

    /**
     * @var int
     *
     * @ORM\Column(name="event", type="integer", nullable=true)
     */
    private $event;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz", nullable=true)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="registered", type="datetimetz", nullable=true)
     */
    private $registered;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vehicle
     *
     * @param integer $vehicle
     *
     * @return GeofenceData
     */
    public function setVehicle($vehicle)
    {
        $this->vehicle = $vehicle;

        return $this;
    }

    /**
     * Get vehicle
     *
     * @return int
     */
    public function getVehicle()
    {
        return $this->vehicle;
    }

    /**
     * Set geofence
     *
     * @param integer $geofence
     *
     * @return GeofenceData
     */
    public function setGeofence($geofence)
    {
        $this->geofence = $geofence;

        return $this;
    }

    /**
     * Get geofence
     *
     * @return int
     */
    public function getGeofence()
    {
        return $this->geofence;
    }

    /**
     * Set event
     *
     * @param integer $event
     *
     * @return GeofenceData
     */
    public function setEvent($event)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return int
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return GeofenceData
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set registered
     *
     * @param \DateTime $registered
     *
     * @return GeofenceData
     */
    public function setRegistered($registered)
    {
        $this->registered = $registered;

        return $this;
    }

    /**
     * Get registered
     *
     * @return \DateTime
     */
    public function getRegistered()
    {
        return $this->registered;
    }


	public function JsonSerialize(){
                if ($this->geofence == null){
                        $geofence = null;
                } else {
                        $geofence = $this->geofence->getName();
                }
		return [
			"id" => $this->id,
			"vehicle" => $this->vehicle,
			"geofence" => $geofence,
                        "event" => $this->event,
                        "date" => $this->date->format("Y-m-d H:i:s"),
                        "registered" => $this->registered->format("Y-m-d H:i:s")
		];
	}
}

