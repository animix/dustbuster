<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * IrrigationMarker
 *
 * @ORM\Table(name="irrigation_marker")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IrrigationMarkerRepository")
 */
class IrrigationMarker implements JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \AppBundle\Entity\IrrigationPlan
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\IrrigationPlan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="irrigationplan_id", referencedColumnName="id")
     * })
     */
    private $irrigationplan;

    /**
     * @var bool
     *
     * @ORM\Column(name="done", type="boolean", nullable=true)
     */
    private $done;

    /**
     * @var \AppBundle\Entity\Polygon
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Polygon")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="polygon_id", referencedColumnName="id")
     * })
     */
    private $polygon;

    /**
     * @var polygon
     *
     * @ORM\Column(name="geometry", type="polygon", nullable=true)
     */
    private $geometry;

    /**
     * @var float
     *
     * @ORM\Column(name="rate", type="float", nullable=true)
     */
    private $rate;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set irrigationplan
     *
     * @param \AppBundle\IrrigationPlan $irrigationplan
     *
     * @return IrrigationMarker
     */
    public function setIrrigationplan($irrigationplan)
    {
        $this->irrigationplan = $irrigationplan;
        return $this;
    }

    /**
     * Get irrigationplan
     *
     * @return \AppBundle\IrrigationPlan
     */
    public function getIrrigationplan()
    {
        return $this->irrigationplan;
    }

    /**
     * Set done
     *
     * @param boolean $done
     *
     * @return IrrigationMarker
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return bool
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set polygon
     *
     * @param \AppBundle\Polygon $polygon
     *
     * @return IrrigationMarker
     */
    public function setPolygon($polygon)
    {
        $this->polygon = $polygon;
        return $this;
    }

    /**
     * Get polygon
     *
     * @return \AppBundle\Polygon
     */
    public function getPolygon()
    {
        return $this->polygon;
    }

    /**
     * Set rate
     *
     * @param float $rate
     *
     * @return IrrigationMarker
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }
    
    /**
     * Set geometry
     *
     * @param polygon $geometry
     * @return IrrigationMarker
     */
    public function setGeometry($geometry)
    {
        $this->geometry = $geometry;
        return $this;
    }

    /**
     * Get geometry
     *
     * @return polygon 
     */
    public function getGeometry()
    {
        return $this->geometry;
    }

	public function JsonSerialize(){
		return array(
			'id'         => $this->id,
			'plan_id'    => $this->irrigationplan->getId(),
			'rate'       => $this->rate,
			'done'       => $this->done,
			'polygon' => $this->polygon->jsonSerialize()
		);
	}
}

