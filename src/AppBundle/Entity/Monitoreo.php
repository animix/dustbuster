<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * Monitoreo
 *
 * @ORM\Table(name="monitoreo")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MonitoreoRepository")
 */
class Monitoreo implements JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @ORM\Column(name="distancia", type="integer", nullable=true)
     */
    private $distancia;


    /**
     * @var integer
     * @ORM\Column(name="id_maleta", type="integer", nullable=true)
     */
    private $idmaleta;

    /**
     * @var string
     *
     * @ORM\Column(name="id_sesion", type="string", length=36, nullable=true)
     */
    private $uuid;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="dump", type="string", length=255, nullable=true)
     */
    private $dump;

    /**
     * @var string
     *
     * @ORM\Column(name="preprocesado", type="string", length=255, nullable=true)
     */
    private $preprocesado;

    /**
     * @var \AppBundle\Entity\Gpsdata
     *
     * @ORM\OneToOne(targetEntity="Gpsdata")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="gpsid_inicial", referencedColumnName="id")
     * })
     */
    private $gpsinicial;

    /**
     * @var \AppBundle\Entity\Gpsdata
     *
     * @ORM\OneToOne(targetEntity="Gpsdata")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="gpsid_final", referencedColumnName="id")
     * })
     */
    private $gpsfinal;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetimetz", nullable=true)
     */
    private $fecha;


    /**
     * Set uuid
     *
     * @param string $uuid
     * @return Monitoreo
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
        return $this;
    }
    /**
     * Get uuid
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Monitoreo
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }


    /**
     * Set dump
     *
     * @param string $dump
     *
     * @return Monitoreo
     */
    public function setDump($dump)
    {
        $this->dump = $dump;

        return $this;
    }

    /**
     * Get dump
     *
     * @return string
     */
    public function getDump()
    {
        return $this->dump;
    }

    /**
     * Set preprocesado
     *
     * @param string $preprocesado
     *
     * @return Monitoreo
     */
    public function setPreprocesado($preprocesado)
    {
        $this->preprocesado = $preprocesado;

        return $this;
    }

    /**
     * Get preprocesado
     *
     * @return string
     */
    public function getPreprocesado()
    {
        return $this->preprocesado;
    }


    /**
     * Set gpsinicial
     *
     * @param \AppBundle\Entity\Gpsdata $gpsinicial
     *
     * @return Monitoreo
     */
    public function setGpsinicial($gpsinicial)
    {
        $this->gpsinicial = $gpsinicial;

        return $this;
    }

    /**
     * Get gpsinicial
     *
     * @return \AppBundle\Entity\Gpsdata
     */
    public function getGpsinicial()
    {
        return $this->gpsinicial;
    }

    /**
     * Set gpsfinal
     *
     * @param \AppBundle\Entity\Gpsdata $gpsfinal
     *
     * @return Monitoreo
     */
    public function setGpsfinal($gpsfinal)
    {
        $this->gpsfinal = $gpsfinal;

        return $this;
    }

    /**
     * Get gpsfinal
     *
     * @return \AppBundle\Entity\Gpsdata
     */
    public function getGpsfinal()
    {
        return $this->gpsfinal;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Monitoreo
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set idmaleta
     * @param integer $idmaleta
     * @return Monitoreo
     */
    public function setIdmaleta($idmaleta){
        $this->idmaleta = $idmaleta;
        return $this;
    }

    /**
     * Get idmaleta
     * @return integer
     */
    public function getIdmaleta(){
        return $this->idmaleta;
    }

    /**
     * Set distancia
     * @param integer $distancia
     * @return Monitoreo
     */
    public function setDistancia($distancia){
        $this->distancia = $distancia;
	return $this;
    }

    /**
     * Get distancia
     * @return integer
     */
    public function getDistancia(){
        return $this->distancia;
    }

    public function jsonSerialize()
    {
        return array(
			'id' => $this->id,
			'id_maleta' => $this->idmaleta,
			'uuid' => $this->uuid,
			'nombre' => $this->nombre,
			'gpsinicial' => ($this->gpsinicial != null)? $this->gpsinicial->getId(): null,
			'gpsfinal' => ($this->gpsfinal != null)? $this->gpsfinal->getId(): null,
			'fecha' => $this->fecha,
			'distancia' => $this->distancia
        );
    }

    public function calculateVincenty($basedir){
	$toolspath = realpath($basedir."/tools");
        $cmd = "python ".$toolspath."/tripdistance ".$this->id;
        return intval(trim(shell_exec($cmd)));
    }
}

