<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Doctrine\ORM\Query\ResultSetMapping;

use AppBundle\Entity\EventoMapa;
use AppBundle\Entity\Polygon;
use AppBundle\Entity\Position;
use AppBundle\Entity\VehicleSignal;

class PositionController extends Controller
{
	/**
	 * @Route("/positiondata/add", name="positiondata_add")
	 */
	public function positionUpdateAction(Request $request){
                $json = json_decode($request->request->get('json'), true);

		$em = $this->getDoctrine()->getManager();

 /*
               $signal = new VehicleSignal();
                $signal->setLat($json['lat'])
                        ->setLon($json['lon'])
			->setIdVehicle($json['id'])
			->setDate(new \DateTime($json['lastupdate']))
                	->setSignal($json['signal']);
                //        ->setIdVehicle($json['id']);
		
                $em->persist($signal);
*/
		$position = $em->getRepository("AppBundle:Position")->findOneByVehicle($json['id']);
		if ($position != null){
			$position->setLat($json['lat'])->setLon($json['lon'])->setLastupdate(new \DateTime($json['lastupdate']));
			$position->setName($json['name']);
			$em->flush();
			return new JsonResponse(["status" => "UPDATE_OK"]);
		}

		$position = new Position();
		$position->setName($json['name'])
			->setLastupdate(new \DateTime($json['lastupdate']))
			->setLat($json['lat'])
			->setLon($json['lon'])
			->setType($json['type'])
			->setVehicle($json['id']);
		$em->persist($position);
		$em->flush();
		return new JsonResponse(["status" => "INSERT_OK"]);
        }

	/**
	 * @Route("/positiondata/last", name="positiondata_last")
	 */
	public function positionLastAction(Request $request){
		$em = $this->getDoctrine()->getManager();
		return new JsonResponse($em->getRepository("AppBundle:Position")->findAll());
	}
}
