<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Doctrine\ORM\Query\ResultSetMapping;

use AppBundle\Entity\WateringData;

class WateringDataController extends Controller
{
    /**
     * @Route("/riego")
     */
    public function riegoAction()
    {
        $data = $this->getDoctrine()->getRepository('AppBundle:WateringData')->findValid();
		return new JsonResponse(array("status" => 'OK', "data" => $data));
    }

}
