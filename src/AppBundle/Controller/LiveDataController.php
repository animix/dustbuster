<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use AppBundle\Entity\GpsData;

class LiveDataController extends Controller
{
    /**
     * @Route("/push/gpsdata/{smartroad_case_id}", name="push_gpsdata")
     */
	public function pushAlertAction(Request $request, $smartroad_case_id){
		$em = $this->getDoctrine()->getManager();
		$data = $request->request->all();
		$json = $data['json'];
		$gpsdata = json_decode($json, true);

		return new JsonResponse(array("status" => 'OK', "gpsdata" => $gpsdata));
	}
}
