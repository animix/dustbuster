<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Doctrine\ORM\Query\ResultSetMapping;

use AppBundle\Entity\EventoMapa;
use AppBundle\Entity\Polygon;
use AppBundle\Entity\CampData;
use AppBundle\Entity\WateringData;

use \Exception;

class EventController extends Controller
{
/*
        private function getGpsDataRange($gpsdata1, $gpsdata2){
                $em = $this->getDoctrine()->getManager();
                $qb = $em->createQueryBuilder();
                $query = $qb->select('gpsdata')
                        ->from('AppBundle\Entity\Gpsdata','gpsdata');

                $query->where('gpsdata.id >= :gpsinicial')
                        ->andWhere('gpsdata.id <= :gpsfinal')
                        ->setParameter('gpsinicial', $gpsdata1)
                        ->setParameter('gpsfinal', $gpsdata2);
                $query = $query->getquery();
                return $query->getResult();
        }
*/


	/**
	 * @Route("/events/monitor/truck/push", name="events_monitor_truck_push")
	 */
	public function eventsMonitorTruckPush(Request $request){
		$data = $request->request->all();
		$json = json_decode($data['json'], true);

		$date = new \DateTime();
		$em = $this->getDoctrine()->getManager();
		$poligono = $em->getRepository("AppBundle:Polygon")->findOneById($json['idpoligono']);

		$newEvent = new EventoMapa();
		// Tipoevento 1 - Monitoreo caex
		// FALTA AGREGAR INFORMACION DE VEHICULO Y RECORRIDO
		$newEvent->setTipoevento(1)
			->setTasaideal($json['tasaideal'])
			->setPm10($json['pm10'])
			->setVelocidadpromedio($json['velocidadpromedio'])
			->setHoraregistro(new \DateTime())
			->setHoraingreso(new \DateTime($json['horaingreso']))
			->setHorasalida(new \DateTime($json['horasalida']))
			->setIdpoligono($poligono)
			->setIdvehiculo($json['vehiculo'])
			->setPm25($json['pm25']);

		// Agregar informacion de puntos individuales, en caso de que exista
		foreach ($json['puntos'] as $punto){
			$campd = new CampData();
			$campd->setVehicle($punto["truck"])
				->setLatitude($punto["lat"])
				->setLongitude($punto["lon"])
				->setSpeed($punto["speed"])
				->setDistance($punto["dist"])
				->setPm10pcc($punto["pm10pcc"])
				->setPm25pcc($punto["pm25pcc"])
				->setDate(new \DateTime($punto["date"]));
			$em->persist($campd);
		}

		$em->persist($newEvent);
		$em->flush();
		return new JsonResponse([
			"status" => "INSERT_OK",
			"record" => $newEvent]);
	}

	/**
	 * @Route("/events/irrigation/truck/push", name="events_irrigation_truck_push")
	 */
	public function eventsIrrigationTruckPush(Request $request){
		$data = $request->request->all();
		$json = json_decode($data['json'], true);

		$date = new \DateTime();
		$em = $this->getDoctrine()->getManager();
		$poligono = $em->getRepository("AppBundle:Polygon")->findOneById($json['idpoligono']);

		$newEvent = new EventoMapa();
		$newEvent->setTipoevento(2)
			->setIdvehiculo($json['vehiculo'])
			->setTasaideal($json['tasaideal'])
			->setTasaalcanzada($json['tasaalcanzada'])
			->setVelocidadpromedio($json['velocidadpromedio'])
			->setHoraregistro(new \DateTime())
			->setHoraingreso(new \DateTime($json['horaingreso']))
			->setHorasalida(new \DateTime($json['horasalida']))
			->setIdpoligono($poligono)
			->setTotalregado($json['totalregado'])
			->setDistanciatotal($json['distanciatotal'])
			->setDistanciaregada($json['distanciaregada']);

		// TODO: Darse la oportunidad de aplicar nuevos tipoevento basado en la actividad informada por el regador:
		// setTipoevento(2) para marcas de riego AUTO
		// setTipoevento(3) para marcas de riego MANUAL
		// setTipoevento(4) para marcas de riego HYBRID
		// setTipoevento(5) para marcas de recorrido de regador SIN REGAR

		try {
			if (array_key_exists("modo_riego", $json)){
				if ($json['modo_riego'] == "MANUAL"){
					$newEvent->setTipoevento(3);
				}
				if ($json['modo_riego'] == "HYBRID"){
					$newEvent->setTipoevento(4);
				}
				if ($json['modo_riego'] == "NONE"){
					$newEvent->setTipoevento(5);
				}
			}

			foreach ($json['puntos'] as $punto){
				$mode = 0;
				if ($punto['mode'] == "AUTO"){
					$mode = 2;
				} elseif ($punto['mode'] == "MANUAL"){
					$mode = 3;
				}

                        	$waterd = new WateringData();
                        	$waterd->setVehicle($punto["truck"])
                                	->setLatitude($punto["lat"])
                                	->setLongitude($punto["lon"])
                                	->setSpeed($punto["speed"])
                                	->setDistance($punto["dist"])
                                	->setMode($mode)
					->setRtr($punto["rtr"] == "1")
					->setRtc($punto["rtc"] == "1")
					->setLtc($punto["ltc"] == "1")
					->setLtr($punto["ltr"] == "1")
					->setForbidden($punto["forbidden"] == "1")
                                	->setDate(new \DateTime($punto["date"]));
                        	$em->persist($waterd);
                	}

		} catch (Exception $e){
			return new JsonResponse([
				"status" => "INSERT_FAIL",
				"exception" => $e->getMessage()
			]);
		}

		//if (array_key_exists("modo_riego", $json)){
		//	$newEvent->setTipoevento(3);
		//}
		/*if (isset($json['modo_riego'])){
			if ($json['modo_riego'] == "MANUAL"){
				$newEvent->setTipoevento(3);
			}
			if ($json['modo_riego'] == "HYBRID"){
				$newEvent->setTipoevento(4);
			}
			if ($json['modo_riego'] == "NONE"){
				$newEvent->setTipoevento(5);
			}
		}*/
		/*if (array_key_exists('modo_riego', $json)){
			if ($json['modo_riego'] == "MANUAL"){
				$newEvent->setTipoevento(3);
			} elseif ($json['modo_riego'] == "HYBRID"){
				$newEvent->setTipoevento(4);
			} elseif ($json['modo_riego'] == "NONE"){
				$newEvent->setTipoevento(5);
			}
		} else {
			// Nada, por defecto era Tipoevento 2 (Riego AUTO)
		}*/

		$em->persist($newEvent);
		$em->flush();
		return new JsonResponse([
			"status" => "INSERT_OK",
			"record" => $newEvent]);
	}



	/**
	 * @Route("/events/irrigation/submit", name="events_irrigation_submit")
	 */
	public function eventsIrrigationSubmit(Request $request){
                $data = $request->request->all();
                $irrigationSummary = $data['irrigationSummary'];

                $date = new \DateTime();
                $user = $this->getUser();

                $em = $this->getDoctrine()->getManager();

                $counter = 0;
                $irrigationSummary = explode(';', $irrigationSummary);
                foreach($irrigationSummary as $tokens){
                        $marker = explode(',', $tokens);
                        $polygon_id = $marker[0];
                        $irrigation_level = $marker[1];
                        $polygon = $em->getRepository('AppBundle:Polygon')->findOneById($polygon_id);

                        $newEvent = new EventoMapa();
                        $newEvent->setIdpoligono($polygon)
				->setHoraregistro($date)
				->setTipoevento(0)
				->setTasaideal($irrigation_level);

                        $em->persist($newEvent);
                        $counter++;
                }
                $em->flush();

                return new JsonResponse(array(
                        'status' => 'OK',
                        'count' => $counter
                ));
	}

	/**
	 * @Route("/events/after/{datetime}", name="events_after_datetime")
	 */
	public function eventsAfterDatetimeAction(Request $request, $datetime = "1970-01-01 00:00:00"){
		$em = $this->getDoctrine()->getManager();
		$events = $em->getRepository('AppBundle:EventoMapa')->findAfter($datetime);
		$x = 0;
		while($x < count($events)){
			//$events[$x]->pm10 = $events[$x]->pm10*10;
			$x = $x + 1;
		}
		return new JsonResponse($events);
	}

	/**
	 * @Route("/events/today", name="events_today")
	 *
	 * Entregamos una combinacion de geometria con eventos
	 */
	public function eventsTodayAction(Request $request){
		$em = $this->getDoctrine()->getManager();

		// Primero: obtener lista de eventos del dia.
		$events = $em->getRepository('AppBundle:EventoMapa')->findAll();

		// Segundo: obtener toda la geometria del mapa. Vamos a ir asociando eventos a esto
		// TO-DO: Obtener unicamente eventos del dia, o eventos posteriores a determinada hora
		$polygons = $em->getRepository('AppBundle:Polygon')->findAll();

		$features = [];
                foreach ($polygons as $polygon){
                        $feature = $polygon->getGeoJsonArray(array(
                                'polygon_id' => $polygon->getId(),
                                'strokeColor' => '#ff0000',
                                'strokeWeight' => '0.5',
                        ));

			// Default settings for polygons with no associated events
			$properties = [
				"event_type" => -1,
				"polygon_id" => $polygon->getId(),
				"strokeColor" => "#444444",
				"strokeWeight" => "0.8",
				"fillColor" => "#444444",
				"fillOpacity" => 0.8,
			];

			foreach ($events as $key => $event){

				// Broken event? No polygon?
				if ($event->getIdpoligono() == null){
					unset($events[$key]);
					continue;
				}

				// Consume events. Event types:
				// 0 - Events forced by user in website
				// 1 - Monitor activity (PM readings)
				// 2 - Irrigation activity
				if ($event->getIdpoligono()->getId() == $polygon->getId()){

					// Event type 0: Forced by user in website. This means irrigation rate orders
					if ($event->getTipoevento() == 0){
						$strokeColor = "#FF0000";
						if ($event->getTasaideal() < 0.7){
							$strokeColor = "#FF8C00";
						}
						if ($event->getTasaideal() == 0){
							$strokeColor = "#00AA00";
						}

						$properties = [
							"event_type" => $event->getTipoevento(),
							"polygon_id" => $polygon->getId(),
							"tasaideal" => $event->getTasaideal(),
							"strokeColor" => $strokeColor,
							"strokeWeight" => "2",
							"fillColor" => $strokeColor,
							"fillOpacity" => 0.7,
						];
						unset($events[$key]);
						continue;

					// Event type 1: Monitor activity. Get PM10 / PM2.5, calculate mean values,
					//               estimate the appropiate irrigation rate, etc.
					} else if ($event->getTipoevento() == 1) {
						/*$properties = [
							"event_type" => $event->getTipoevento(),
							"polygon_id" => $polygon->getId(),
							"strokeColor" => "#FF0000",
							"strokeWeight" => "1",
							"fillColor" => "#0000FF",
							"fillOpacity" => 0.5,
						];*/
				                $strokeColor = "#FF0000";
                                                if ($event->getTasaideal() < 0.7){
                                                        $strokeColor = "#FF8C00";
                                                }
                                                if ($event->getTasaideal() == 0){
                                                        $strokeColor = "#00AA00";
                                                }

                                                $properties = [
                                                        "event_type" => $event->getTipoevento(),
                                                        "polygon_id" => $polygon->getId(),
                                                        "tasaideal" => $event->getTasaideal(),
                                                        "strokeColor" => $strokeColor,
                                                        "strokeWeight" => "2",
                                                        "fillColor" => $strokeColor,
                                                        "fillOpacity" => 0.7,
							"pm10" => $event->getPm10()
                                                ];
						unset($events[$key]);
						continue;

					// Event type 2: Irrigation activity. Get ideal/real irrigation rate,
					//               total flow, enter/exit times, irrigation vehicle id, etc.
					} else if ($event->getTipoevento() == 2) {
						$properties = [
							"event_type" => 2,
							"polygon_id" => $polygon->getId(),
							"strokeColor" => "#0000FF",
							"strokeWeight" => "1",
							"fillColor" => "#0000FF",
							"fillOpacity" => 0.8,
							"totalregado" => $event->getTotalregado(),
							"tasaideal" => $event->getTasaideal(),
							"tasaalcanzada" => $event->getTasaalcanzada(),
							"velocidadpromedio" => $event->getVelocidadpromedio(),
							"horaingreso" => $event->getHoraingreso(),
							"horasalida" => $event->getHorasalida(),
							"horaregistro" => $event->getHoraregistro()
						];
						unset($events[$key]);
						continue;
					}
				}
			}
			$feature['properties'] = $properties;
			$features[] = $feature;
                }
                // TODO: Include photodata infowindows
                $geojson = array(
                        "type" => "FeatureCollection",
                        "features" => $features
                );
                return new JsonResponse($geojson);
	}


        /**
         * @Route("/events/push", name="events_push")
         */
        public function eventsPushAction(Request $request){
		$json = json_decode($request->request->get('json'), true);

                return new JsonResponse($json);
        }

	/**
	 * @Route("/events/monitoreo/{id}", name="events_monitoreo")
	 */
	public function eventsMonitoreoAction(Request $request, $id=null){
                $starttime = time();
                $em = $this->getDoctrine()->getManager();
                $lastuuid = $em->getRepository('AppBundle:Monitoreo')->findOneBy([], ["id" => "DESC"]);


                $sql = "SELECT gpsdata.id as gpsid, ".
                        "polygon.id as polygon_id, ".
                        "gpsdata.latitude as lat, ".
                        "gpsdata.longitude as lng, ".
                        "ST_ASTEXT(polygon.geometry) as geometry, ".
                        "pmdata.pm10lat_d as pm10lat, ".
                        "pmdata.pm25lat_d as pm25lat, ".
                        "UNIX_TIMESTAMP(gpsdata.fecha) as timestamp, ".
                        "gpsdata.fecha as date ".
                        "FROM polygon JOIN gpsdata JOIN pmdata ".
                        "where gpsdata.date='". $lastuuid->getFecha()->format('dmy') ."' ".
                        "and pmdata.id_gps=gpsdata.id ".
                        "and ST_CONTAINS(polygon.geometry, polygonfromtext(concat('point(', gpsdata.longitude, ' ', gpsdata.latitude, ')'))) ".
                        "ORDER BY gpsid DESC";


                $rsm = new ResultSetMapping();
                $rsm->addScalarResult('date','date');
                $rsm->addScalarResult('timestamp','timestamp');
                $rsm->addScalarResult('gpsid', 'gpsid');
                $rsm->addScalarResult('lat', 'lat');
                $rsm->addScalarResult('lng', 'lng');
                $rsm->addScalarResult('polygon_id', 'polygon_id');
                $rsm->addScalarResult('geometry', 'geometry');
                $rsm->addScalarResult('pm10lat', 'pm10lat');
                $rsm->addScalarResult('pm25lat', 'pm25lat');

                $em = $this->getDoctrine()->getManager();
                $query = $em->createNativeQuery($sql, $rsm);
                $result = $query->getResult();


                //$features = $this->pmPolygonsToGeoJson($result);
                $endtime = time();

		$points_polygons = [];
		foreach ($result as $entry){
			$points_polygons[$entry["polygon_id"]][] = [
				"gpsid" => $entry["gpsid"],
				"pm10" => $entry["pm10lat"]
			];
		}

//		return new JsonResponse($points_polygons);
		$final = [];
		foreach ($points_polygons as $key => $polygon){

			$pm10 = 0;
			foreach ($polygon as $pmdata){
				$pm10 += $pmdata["pm10"];
			}
			$final[] = [
				"polygon_id" => $key,
				"pm10" => floor($pm10/count($polygon))
			];


			$polygon_id = $key;
			$pm10 = floor($pm10/count($polygon));
			$irrigation_level = 0;
			if ($pm10 > 699){ // 299
				$irrigation_level = 1;
			}
			if ($pm10 > 1299){ // 599
				$irrigation_level = 2;
			}

			$polygonObject = $em->getRepository("AppBundle:Polygon")->findOneById($polygon_id);

			$date = new \DateTime();
                        $newEvent = new EventoMapa();
                        $newEvent->setIdpoligono($polygonObject)
                                ->setHoraregistro($date)
                                ->setTipoevento(1)
				->setPm10($pm10)
                                ->setTasaideal($irrigation_level);

                        $em->persist($newEvent);
			$em->flush();
		}
		return new JsonResponse($final);
	}
}
