<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\Actividad;


class CalendarController extends Controller
{
	private function eventListHtml($events){
		$html = '<div class="list-group">';
		foreach ($events as $event){
			$html .= '<a href="" class="list-group-item text-ellipsis">';
			$html .= '<span class="label-pill label-warning pull-xs-right">'.$event['time'].'</span>';
			$html .= $event['description'].'</a>';
		}
		$html .= '</div>';
		return $html;
	}

    /**
     * @Route("/calendar/geofence/push", name="calendar_geofence_push")
     */
	public function calendarGeofencePushAction(Request $request){
		$em = $this->getDoctrine()->getManager();
		$data = $request->request->all();
		$json = json_decode($data["json"], true);
		$actividad = new Actividad();
		$actividad->setTitulo($json["title"])
			->setFecha(new \DateTime($json['date']))
			->setObservaciones($json["details"]);
		$em->persist($actividad);
		$em->flush();
		return new JsonResponse([
			"status" => "OK",
			"title" => $json["title"]
		]);
	}


    /**
     * @Route("/push/calendar/activity", name="push_calendar_activity")
     */
	public function pushCalendarActivityAction(Request $request){
		$em = $this->getDoctrine()->getManager();
		$data = $request->request->all();

		$actividad = new Actividad();
		$actividad->setTitulo($data['title'])
			->setFecha(new \DateTime($data['date']))
			->setObservaciones($data['details']);
		$em->persist($actividad);
		$em->flush();

		return new JsonResponse(array("status" => "OK", "title" => $data['title']));
	}


    /**
     * @Route("/calendar/day/{month}/{day}/{year}", name="calendar_day")
     * @Template()
     */
    public function dayAction(Request $request, $month, $day, $year)
    {
		return array("month"=>$month, "day"=>$day, "year"=>$year);
	}
	
    /**
     * @Route("/calendar/events", name="calendar_events")
     */
    public function eventsAction(Request $request)
    {
		$month = $request->query->get('month');
		$year = $request->query->get('year');

		$events = array(
			array(
			    "1/$month/$year",
			    "Actividades del 1/$month/$year",
			    "day/1/$month/$year",
                'Sing.colors["brand-primary"]',
                /*$this->eventListHtml(array(
					array("time" => "06:00", "description" => "Actividad número uno"),
					array("time" => "11:15", "description" => "Actividad número dos, particularmente larga para esto"),
					array("time" => "14:30", "description" => "Actividad número tres"),
					array("time" => "20:50", "description" => "Actividad número cuatro"),
                ))*/
			),
			array(
				"28/$month/$year",
				"Medición del día 28 de cada mes",
				"https://spence.eye3.cl",
			),
			array(
				"1/$month/$year",
				"",
				"",
				"white"
			)

		);
		return new JsonResponse($events);
    }

    /**
     * @Route("/calendar/book/{date}", name="calendar_book")
     * @Template()
     */
    public function bookAction(Request $request, $date = null)
    {
	if ($date == null){
		$date = new \DateTime();
	} else {
		$date = new \DateTime($date);
	}

	$em = $this->getDoctrine()->getManager();
	$actividades = $em->getRepository("AppBundle:Actividad")->findAll();

        // replace this example code with whatever you need
        return $this->render('default/book.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
		'hoy' => $date,
		'actividades' => $actividades,
		'user' => $this->getUser()
        ]);
    }


    /**
     * @Route("/calendar/activity/date/{date}")
     */
	public function calendarActivityDateAction(Request $request, $date = null)
	{
		$startDate = new \DateTime($date);
		$endDate = new \DateTime($date);
		$startDate->setTime(0,0,0);
		$endDate->setTime(23,59,59);

		$qb = $this->getDoctrine()->getManager()->createQueryBuilder();
		$qb->select("datos")
			->from("AppBundle:Actividad", "datos")
			->where("datos.fecha >= :inicio")
			->andWhere("datos.fecha <= :final")
			->orderBy("datos.fecha", "ASC")
			->setParameter("inicio", $startDate)
			->setParameter("final", $endDate);
		$query = $qb->getQuery();
		$data = $query->getResult();

		$html = "";
		foreach ($data as $activity){
			$html .= '<a href="#" class="list-group-item text-ellipsis">'.
				'<span class="label-pill label-success pull-xs-right">'.
				$activity->getFecha()->format('H:i').
				'</span>'. $activity->getTitulo() .'</a>';
		}

		return new Response($html);
	}
}
