<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use AppBundle\Entity\SysLog;
use AppBundle\Entity\Geofence;
use AppBundle\Entity\GeofenceData;
use AppBundle\Entity\VersionTimestamp;

class GeofenceController extends Controller
{

	private function splitDateRange($daterange){
                if ($daterange == null){
                        $startDate = new \DateTime();
                        $endDate = new \DateTime();
                } else {
                        // Expecting "YYYY-MM-DD"
                        $range = explode(',', $daterange);
                        $startDate = new \DateTime($range[0]);
                        if (array_key_exists(1, $range)){
                                $endDate = new \DateTime($range[1]);
                        } else {
                                $endDate = new \DateTime($range[0]);
                        }
                }
                $startDate->setTime(0,0,0);
                $endDate->setTime(23,59,59);
		return array(
			"start" => $startDate,
			"end" => $endDate
		);
	}

        /**
         * @Route("/geofence/after/{datetime}", name="geofence_after_datetime")
         */
        public function geofenceAfterDatetimeAction(Request $request, $datetime = "1970-01-01 00:00:00"){
                $em = $this->getDoctrine()->getManager();
                $events = $em->getRepository('AppBundle:Geofence')->findAfter($datetime);
                return new JsonResponse($events);
        }


	private function secondsToHMS($seconds){
		$H = floor($seconds / 3600);
		$i = ($seconds / 60) % 60;
		$s = $seconds % 60;
		return sprintf("%d:%02d:%02d", $H, $i, $s);
	}

	/**
	 * @Route("/geofence/log/{truck_id}/{daterange}")
	 */
	public function geofenceLogTruckDaterangeAction(Request $request, $truck_id, $daterange){
		$time_start = microtime();
		$em = $this->getDoctrine()->getManager();
		$daterange = $this->splitDateRange($daterange);

		$areas = $em->getRepository("AppBundle:Geofence")->findAll();
		$perm_by_area = [];
		foreach ($areas as $area){
			$perm_by_area[$area->getName()] = 0;
		}

		$events = $em->getRepository("AppBundle:GeofenceData")->findByTruckAndDateRange($truck_id, $daterange["start"], $daterange["end"]);
		$events = array_reverse($events);
		$perm = [];

		$index = count($events) - 1;
		while($index > 0){
			$currEvent = $events[$index];
			// We start reverse count with the geofence abandon event [2],
			// then look for the related geofence entering event [1]
			if ($currEvent->getEvent() == 2){
				$index = $index - 1;
				while(($events[$index]->getEvent() == 1) && ($index > 0)){
					$lastEvent = $events[$index];
					$index = $index - 1;
				}
				if ($currEvent->getGeofence() != null and $lastEvent->getGeofence() != null){
				if ($currEvent->getGeofence() == $lastEvent->getGeofence()){
					$seconds = $currEvent->getDate()->getTimestamp() - $lastEvent->getDate()->getTimestamp();
					$perm[] = [
						"enter_id" => $lastEvent->getId(),
						"leave_id" => $currEvent->getId(),
						"geofence" => $currEvent->getGeofence()->getName(),
						"enter_date" => $lastEvent->getDate()->format("Y-m-d H:i:s"),
						"leave_date" => $currEvent->getDate()->format("Y-m-d H:i:s"),
						"seconds" => $seconds
					];
					$perm_by_area[$currEvent->getGeofence()->getName()] += $seconds;
					$index = $index + 1;
				}}
			}
			$index = $index - 1;
		}

		$time_end = microtime();

		foreach($perm_by_area as $key=>$seconds){
			$perm_by_area[$key] = [
				"hms" => $this->secondsToHMS($perm_by_area[$key]),
				"seconds" => $seconds
			];
		}
		return new JsonResponse([
			"status" => "ok",
			"profiling_time_microseconds" => $time_end - $time_start,
			"daterange" => $daterange,
			"summary" => $perm_by_area,
			"permanence" => $perm,
			//"events" => $events
		]);
	}

	/**
	 * @Route("/geofence/show/all/sql", name="geofence_show_all_sql")
	 */
	public function showAllSqlAction(Request $request){
		$em = $this->getDoctrine()->getManager();
		$polygons = $em->getRepository("AppBundle:Geofence")->findAll();

		$sql = "";
		foreach ($polygons as $polygon){
			$sql = $sql . $polygon->getSQLInsert();
		}
		return new Response($sql);
	}


        /**
         * @Route("/geofence/show/areas", name="geofence_show_areas")
         */
        public function showAreas(Request $request){
                $em = $this->getDoctrine()->getManager();
                $areas = $em->getRepository('AppBundle:Geofence')->findAll();

                $features = [];
                foreach ($areas as $area){
                        $feature = $area->getGeoJsonArray(array(
                                'area_id' => $area->getId(),
                                'strokeColor' => '#000000',
                                'strokeWeight' => '1.5'
                        ));
                        $features[] = $feature;
                }
                $geojson = array(
                        'type' => 'FeatureCollection',
                        'features' => $features
                );
                return new JsonResponse($geojson);
        }

	/**
	 * @Route("/geofence/events/push", name="geofence_events_push")
	 */
	public function geofenceEventsPushAction(Request $request){
		$em = $this->getDoctrine()->getManager();
		$data = $request->request->all();
		$json = json_decode($data['json'], true);

		$registered = new \DateTime();
		$fence = $em->getRepository("AppBundle:Geofence")->findOneById($json["geofence"]);
		$event = new GeofenceData();
		$event->setGeofence($fence)
			->setVehicle($json["vehicle"])
			->setEvent($json["event"])
			->setDate(new \DateTime($json["date"]))
			->setRegistered($registered);
		$em->persist($event);
		$em->flush();
		return new JsonResponse([
			"status" => "OK",
			"registered" => $registered->format("Y-m-d H:i:s")
		]);
	}

	/**
	 * @Route("/geofence/areas/delete", name="geofence_areas_delete")
         */
	public function areasDelete(Request $request){
		$data = $request->request->all();
		$json = json_decode($data['json'], true);
		$dt = new \DateTime();
		$em = $this->getDoctrine()->getManager();
		$geofence = $em->getRepository("AppBundle:Geofence")->findOneById($json['id']);
		$em->remove($geofence);
		$em->flush();
				
		$em = $this->getDoctrine()->getManager();
		$log = new SysLog();
		$log->setModule("Geofence");
		$log->setAction("delete");
		$log->setSubject($json['id']);
		$log->setDetail($this->getUser());
		$em->persist($log);
		$em->flush();

		return new JsonResponse([
			"status" =>  "OK",
			"operation" => "areas_delete",
			"id" => $json['id']
		]);
	}


	/**
	 * @Route("/geofence/timestamp", name="geofence_timestamp")
	 */
	public function geofenceTimestampAction(Request $request){
		$em = $this->getDoctrine()->getManager();
		$result = $em->getRepository("AppBundle:VersionTimestamp")->findOneBy(['name' => 'geofence'],['timestamp'=>'DESC']);
		return new JsonResponse($result);
	}


	/**
	 * @Route("/geofence/visits/daterange/{geofence_id}/{daterange}/{minimum_seconds}", name="geofence_visits_id_daterange")
	 */
	public function visitsDaterangeAction(Request $request, $geofence_id, $daterange, $minimum_seconds = 0){
		$daterangestring = $daterange;
		$daterange = $this->splitDateRange($daterange);
		$em = $this->getDoctrine()->getManager();
		$response = new StreamedResponse();
		$response->headers->set('Content-Type', 'text/csv');
		$em = $this->getDoctrine()->getManager();
		$response->headers->set('Content-Disposition', 'attachment; filename="visits_'.$geofence_id.'-'.$daterangestring.'.csv"');
		$this->_minimum_seconds = $minimum_seconds;

		$visits = $em->getRepository("AppBundle:GeofenceData")->findByGeofenceAndEventAndDaterange($geofence_id, 2, $daterange['start'], $daterange['end']);
		foreach ($visits as $key => $val){
			$gf_enter = $em->getRepository("AppBundle:GeofenceData")->findLastEventBefore($val, 1);
			$result['geofence'] = $val->getGeofence()->getId();
			$result['vehicle'] = $val->getVehicle();
			$result['enter'] = $gf_enter[0]->getDate()->format("Y-m-d H:i:s");
			$result['exit'] = $val->getDate()->format("Y-m-d H:i:s");
			$result['seconds'] = $val->getDate()->getTimestamp() - $gf_enter[0]->getDate()->getTimestamp();
			$results[] = $result;
		}
		$response->setCallback(function() use ($geofence_id, $results){
			echo "geofence,truck_id,enter,exit,seconds".PHP_EOL;
			foreach ($results as $result){
				if ($result['seconds'] < $this->_minimum_seconds) continue;
				echo $result['geofence'].",".
					$result['vehicle'].",".
					$result['enter'].",".
					$result['exit'].",".
					$result['seconds'].PHP_EOL;
			}
		});
                return $response;
	}

	/**
	 * @Route("/geofence/visits/{geofence_id}/{minimum_seconds}", name="geofence_visits_id")
	 */
	public function visitsAction(Request $request, $geofence_id, $minimum_seconds = 0){
		$em = $this->getDoctrine()->getManager();
		$response = new StreamedResponse();
		$response->headers->set('Content-Type','text/csv');
		$em = $this->getDoctrine()->getManager();
		$response->headers->set('Content-Disposition', 'attachment; filename="visits_'.$geofence_id.'.csv"');
		$this->_minimum_seconds = $minimum_seconds;

		$visits = $em->getRepository("AppBundle:GeofenceData")->findByGeofenceAndEvent($geofence_id,2);
		foreach ($visits as $key => $val){
			$gf_enter = $em->getRepository("AppBundle:GeofenceData")->findLastEventBefore($val, 1);
			$result['geofence'] = $val->getGeofence()->getId();
			$result['vehicle'] = $val->getVehicle();
			$result['enter'] = $gf_enter[0]->getDate()->format("Y-m-d H:i:s");
			$result['exit'] = $val->getDate()->format("Y-m-d H:i:s");
			$result['seconds'] = $val->getDate()->getTimestamp() - $gf_enter[0]->getDate()->getTimestamp();
			$results[] = $result;
		}

		$response->setCallback(function() use ($geofence_id, $results){
			echo "geofence,truck_id,enter,exit,seconds".PHP_EOL;
			foreach ($results as $result){
				if ($result['seconds'] < $this->_minimum_seconds) continue;
				echo $result['geofence'].",".
					$result['vehicle'].",".
					$result['enter'].",".
					$result['exit'].",".
					$result['seconds'].
					PHP_EOL;
			}
		});
		return $response;
	}


        /**
         * @Route("/geofence/areas/new", name="geofence_areas_new")
         */
        public function areasNew(Request $request){
                $data = $request->request->all();
                $json = json_decode($data['json'], true);
                $dt = new \DateTime();

		$rtr = ($json['rtr'] == true)? 1: 0;
		$rtc = ($json['rtc'] == true)? 1: 0;
		$ltr = ($json['ltr'] == true)? 1: 0;
		$ltc = ($json['ltc'] == true)? 1: 0;

		if ($json['id'] == 0){
	                $sql = "INSERT INTO geofence(name, pattern, date, type, geometry, rtr, rtc, ltr, ltc) VALUES('".
        	                $json['name'] ."','".
                	        $json['pattern'] ."','".
	                        $dt->format("Y-m-d H:i:s") ."','".
        	                $json['roadtype'] .
                	        "',GeomFromText('POLYGON((". $json['vertices'] ."))'),".
				$rtr .",".
				$rtc .",".
				$ltr .",".
				$ltc .")";
		} else {
			$dt = new \DateTime();
			$sql = "UPDATE geofence set name='". $json['name'] ."',".
				"pattern='". $json['pattern'] ."',".
				"date='". $dt->format("Y-m-d H:i:s") ."',".
				"type='". $json['roadtype'] ."',".
				"rtr=". $rtr .",".
				"rtc=". $rtc .",".
				"ltr=". $ltr .",".
				"ltc=". $ltc ." where id=". $json['id'];
		}

		$ts = $dt->getTimestamp();
		$vt = new VersionTimestamp();
		$vt->setName("geofence")->setTimestamp($ts);
		$em = $this->getDoctrine()->getManager();
		$em->persist($vt);
		$em->flush();

                $conn = $this->getDoctrine()->getConnection();
                $count = $conn->executeUpdate($sql);
		if ($json['id'] == 0){
			$status = "INSERT_OK";
		} else {
			$status = "UPDATE_OK";
		}
                return new JsonResponse([
                        "status" => $status,
                        "sql" => $sql
                ]);
        }

}

