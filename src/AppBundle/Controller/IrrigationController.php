<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

use Doctrine\ORM\Query\ResultSetMapping;

use AppBundle\Entity\IrrigationPlan;
use AppBundle\Entity\IrrigationMarker;

class IrrigationController extends Controller
{

    /**
     * @Route("/irrigation/plan/collect/{id}", name="irrigation_plan_collect")
     * @Template()
	 * Receive irrigation plan
     */
    public function irrigationPlanCollectAction(Request $request, $id)
    {
		$em = $this->getDoctrine()->getManager();
		$plan = $em->getRepository('AppBundle:IrrigationPlan')->findOneByDeployed(false);

		if ($plan != null){
			$markers = $em->getRepository('AppBundle:IrrigationMarker')->findByIrrigationplan($plan);

			return new JsonResponse(array(
				"status" => "irrigate",
				"irrigationtruck_id" => $id,
				"plan_id" => $plan->getId(),
				"markers" => $markers
			));
		} else {

			// No irrigation plan, nothing to do
			return new JsonResponse(array(
				"status" => "idle",
				"irrigationtruck_id" => $id
			));
		}
	}

    /**
     * @Route("/irrigation/plan/submit", name="irrigation_plan_submit")
     * @Template()
	 * Receive irrigation plan
     */
    public function irrigationPlanSubmitAction(Request $request)
    {
		$data = $request->request->all();
		$irrigationSummary = $data['irrigationSummary'];
		$timestamp = time();
		$date = new \DateTime();
		$user = $this->getUser();

		$em = $this->getDoctrine()->getManager();
		$irrigationPlan = new IrrigationPlan();
		$irrigationPlan->setUser($user)
			->setDeployed(false)
			->setCompleted(false)
			->setDate($date)
			->setTimestamp($timestamp)
			->setFile(null);			
		$em->persist($irrigationPlan);
		$em->flush();

		$counter = 0;
		$irrigationSummary = explode(';', $irrigationSummary);
		foreach($irrigationSummary as $tokens){
			$marker = explode(',', $tokens);
			$polygon_id = $marker[0];
			$irrigation_level = $marker[1];
			$polygon = $em->getRepository('AppBundle:Polygon')->findOneById($polygon_id);
			
			$irrigationMarker = new IrrigationMarker();
			$irrigationMarker->setIrrigationplan($irrigationPlan)
				->setPolygon($polygon)
				->setGeometry(null)
				->setRate($irrigation_level)
				->setDone(false);
			
			$em->persist($irrigationMarker);
			$counter++;
		}
		$em->flush();

		return new JsonResponse(array(
			'status' => 'OK', 
			'count' => $counter
		));
    }
}
