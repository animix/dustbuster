<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

use AppBundle\Entity\CampData;

class DefaultController extends Controller
{
        private function splitDateRange($daterange){
                if ($daterange == null){
                        $startDate = new \DateTime();
                        $endDate = new \DateTime();
                } else {
                        // Expecting "YYYY-MM-DD"
                        $range = explode(',', $daterange);
                        $startDate = new \DateTime($range[0]);
                        if (array_key_exists(1, $range)){
                                $endDate = new \DateTime($range[1]);
                        } else {
                                $endDate = new \DateTime($range[0]);
                        }
                }
                $startDate->setTime(0,0,0);
                $endDate->setTime(23,59,59);
                return array(
                        "start" => $startDate,
                        "end" => $endDate
                );
        }


    /**
     * @Route("/dashboard", name="dashboard")
	 * Dashboard page
     */
    public function indexAction(Request $request)
    {

	$date = new \DateTime();
	                $qb = $this->getDoctrine()->getManager()->createqueryBuilder();
                $qb->select("pmdata")
                        ->from("AppBundle\Entity\Pmdata","pmdata")
                        ->join("pmdata.idGps","gpsdata")
                        ->where("gpsdata.date = :fecha")
			->andWhere("gpsdata.speed >= 10")
                        ->setParameter("fecha", $date->format("dmy"));
                $query = $qb->getquery();
                $result = $query->getResult();

                $totalpm10 = 0;
                $totalpm25 = 0;
                $pm10_low    = 0;
                $pm10_medium = 0;
                $pm10_high   = 0;
                $pm25_low    = 0;
                $pm25_medium = 0;
                $pm25_high   = 0;

                foreach ($result as $pmdata){
                        $pm25lat = $pmdata->getPm25lat_d();
                        $pm10lat = $pmdata->getPm10lat_d();
                        $totalpm10 += $pm10lat;
                        $totalpm25 += $pm25lat;

                        if ($pm10lat < 300){
                                $pm10_low ++;
                        } else {
                                if ($pm10lat < 600){
                                        $pm10_medium ++;
                                } else {
                                        $pm10_high ++;
                                }
                        }
                        if ($pm25lat < 300){
                                $pm25_low ++;
                        } else {
                                if ($pm25lat < 600){
                                        $pm25_medium ++;
                                } else {
                                        $pm25_high ++;
                                }
                        }
                }

	$items = count($result);
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
			'dia' => $date->format("d/m"),
                        'date' => $date,
                        'points' => count($result),
                        'pm10_average' => ($items == 0)? 0: floor($totalpm10 / $items),
                        'pm25_average' => ($items == 0)? 0: floor($totalpm25 / $items),
                        'pm10_low' =>     ($items == 0)? 0: floor($pm10_low / $items * 100),
                        'pm10_medium' =>  ($items == 0)? 0: floor($pm10_medium / $items * 100),
                        'pm10_high' =>    ($items == 0)? 0: floor($pm10_high / $items * 100),
                        'pm25_low' =>     ($items == 0)? 0: floor($pm25_low / $items * 100),
                        'pm25_medium' =>  ($items == 0)? 0: floor($pm25_medium / $items * 100),
                        'pm25_high' =>    ($items == 0)? 0: floor($pm25_high / $items * 100),
                        'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/map", name="map")
	 * PM Map page
     */
    public function mapPageAction(Request $request)
    {
        return $this->render('default/map.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
		'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/weather/{station_id}/{daterange}", name="weather")
     */
    public function weatherPageAction(Request $request, $station_id, $daterange = null)
    {
	$em = $this->getDoctrine()->getManager();
	$data = $em->getRepository("AppBundle:StationData")->findByStationAndDateRange($station_id, $daterange);

	$daterange = $this->splitDateRange($daterange);
	return $this->render('default/weather.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'user' => $this->getUser(),
		"station_id" => $station_id,
		"daterange" => $daterange,
		"data" => $data
        ]);
    }

    /**
     * @Route("/irrigation", name="irrigation")
	 * Irrigation charts page
     */
    public function irrigationPageAction(Request $request)
    {
        return $this->render('default/irrigation.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/rawdata", name="rawdata")
	 * Raw data page
     */
    public function rawDataPageAction(Request $request)
    {
		$em = $this->getDoctrine()->getManager();
		$recorridos = $em->getRepository('AppBundle:Monitoreo')->findBy([],['id' => 'ASC']);

        return $this->render('default/rawdata.html.twig', [
			'monitoreos' => $recorridos,
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/roads", name="roads")
	 * PM Map page
     */
    public function roadsPageAction(Request $request)
    {
        return $this->render('default/roads.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/editor", name="editor")
	 * Polygon editor page
     */
    public function editorPageAction(Request $request)
    {
        return $this->render('default/editor.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/areas", name="areas")
     */
    public function areasPageAction(Request $request)
    {
        return $this->render('default/areas.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/planning", name="planning")
     */
    public function planningPageAction(Request $request)
    {
        return $this->render('default/planning.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/plan", name="plan")
	 * Irrigation planning page
     */
    public function planPageAction(Request $request)
    {
        return $this->render('default/plan.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/player", name="player")
     * Replay a monitor trip on a map
     */
    public function playerPageAction(Request $request)
    {
        return $this->render('default/player.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/valvecontrol", name="valvecontrol")
     * Valve control demo page
     */
    public function valveControlAction(Request $request)
    {
        return $this->render('default/valvecontrol.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/live", name="live2")
     */
    public function live2Action(Request $request){
        return $this->render('default/live.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
                'user' => $this->getUser()
        ]);
    }


    /**
     * @Route("/", name="homepage")
     * Google Maps-based live Site view demo page
     */
    public function liveAction(Request $request){
	if ($this->getUser()->getUsername() == "kaltire"){
	        return $this->render('default/map.html.twig', [
        	    'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
                	'user' => $this->getUser()
	        ]);
	}

        return $this->render('default/live.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
		'user' => $this->getUser()
        ]);
    }


    /**
     * @Route("/tablet", name="tablet")
     * Google Maps-based live Site view, for tablet consumption
     */
    public function tabletAction(Request $request){
        return $this->render('default/tablet-live.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'user' => $this->getUser()
        ]);
    }


    /**
     * @Route("/nmea-kml/permalink/{uid}", name="nmea_kml_permalink")
     */
    public function nmeaKmlPermalinkAction(Request $request, $uid){
        $dumppath = realpath($this->getParameter('kernel.root_dir').'/../web/permalinks/'.$uid);
	$dataset = explode("\n", file_get_contents($dumppath));
//	return new JsonResponse(["status" => "finding bug"]);

	$response = new StreamedResponse();
	$response->setCallback(function() use (&$dataset, &$uid){
            echo '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL.
                '<kml xmlns="http://www.opengis.net/kml/2.2" '.
                'xmlns:gx="http://www.google.com/kml/ext/2.2" '.
                'xmlns:kml="http://www.opengis.net/kml/2.2" '.
                'xmlns:atom="http://www.w3.org/2005/Atom">'.PHP_EOL;

		echo '<Document>'.PHP_EOL;

		echo '<Placemark><name>0</name><LineString><coordinates>'.PHP_EOL;
		$line_count = 0;
		$counter = 0;
		$blocks = 0;
		$parts = explode(",", $dataset[0]);
		$lastlat = $parts[0];
		$lastlng = $parts[1];
		foreach ($dataset as $line){
			if (($line_count % 2) == 0) {
				$line_count++;
				continue;
			}
			if (trim($line) != ""){
				$parts = explode(",", $line);
				$lat = $parts[0];
				$lng = $parts[1];
				if (abs($lastlat - $lat) < 0.1 && abs($lastlng - $lng) < 0.1){
					echo $parts[1].",".$parts[0].",0 ".PHP_EOL;
					$lastlat = $lat;
					$lastlng = $lng;
				}
			}
			$line_count++;
			if ($line_count > 3000){
				$blocks++;
				echo "</coordinates></LineString></Placemark>".PHP_EOL;
				echo "<Placemark><name>Block ".$blocks." (3000 points max)</name><LineString><coordinates>".PHP_EOL;
				$line_count = 0;
			}
		}
		echo '</coordinates></LineString></Placemark>'.PHP_EOL;
		echo '</Document></kml>'.PHP_EOL;
	});
	$response->setStatusCode('200');
	$response->headers->set('Content-Type', 'application/vnd.google-earth.kml+xml; charset=utf-8');
	$response->headers->set('Content-Disposition', 'attachment; filename="Dataset '.$uid.'.kml"');

        return $response;
    }

    /**
     * @Route("/nmea-polygen/permalink/{uid}", name="nmea_polygen_permalink")
     */
    public function nemaPolygenPermalinkAction(Request $request, $uid){
        $dumppath = realpath($this->getParameter('kernel.root_dir').'/../web/permalinks/'.$uid);
        $points = [];
        $nmea = explode("\n", file_get_contents($dumppath));
	$lastdata = explode(",", $nmea[0]);
        foreach ($nmea as $line){
            if ($line != ""){
                $newdata = explode(",", $line);
                if ((abs($newdata[0] - $lastdata[0]) < 0.5) && (abs($newdata[1]-$lastdata[1]) < 0.5)){
                    $points[] = $newdata;
                    $lastdata = $newdata;
                }
            }
        }
        return $this->render('default/nmea-polygen.html.twig',[
            "files" => [],//"dumppath" => $dumppath, "files" => $files],
            "user" => $this->getUser(),
            "polygons" => true,
            "nmea" => [
                "name" => $uid,
                "text" => "",
                "points" => $points
        ]]);
    }

    /**
     * @Route("/nmea-kml/{vehicle}/daterange/{daterange}", name="nmea_kml_daterange")
     */
    public function nmeaKmlDaterangeAction(Request $request, $vehicle, $daterange){
        $points = [];
        $nmea = [];
        $range = $this->splitDateRange($daterange);
        $em = $this->getDoctrine()->getManager();
        $results = $em->getRepository("AppBundle:CampData")->findByVehicleAndDateRange($vehicle, $range);
        $points = [];
        foreach ($results as $r){
            $points[] = [$r->getLatitude(), $r->getLongitude(), $r->getDistance()];
        }

        $response = new StreamedResponse();
        $response->setCallback(function() use (&$results, &$vehicle){
            echo '<?xml version="1.0" encoding="UTF-8"?>'. PHP_EOL.
                 '<kml xmlns="http://www.opengis.net/kml/2.2" '.PHP_EOL.
                 'xmlns:gx="http://www.google.com/kml/ext/2.2" '.PHP_EOL.
                 'xmlns:kml="http://www.opengis.net/kml/2.2" '.PHP_EOL.
                 'xmlns:atom="http://www.w3.org/2005/Atom">'. PHP_EOL;
            echo '<Document>'.PHP_EOL;
            echo '<Placemark><name></name><LineString><coordinates>'.PHP_EOL;

            $line_count=0;
            $counter = 0;
            $blocks = 0;
            $lastlat = $results[0]->getLatitude();
            $lastlng = $results[0]->getLongitude();

            foreach ($results as $result){
                if (($line_count % 2) == 0){
                    $line_count++;
                    continue;
                }
                $lat = $result->getLatitude();
                $lng = $result->getLongitude();
		if (abs($lastlat - $lat) < 1 && abs($lastlng - $lng) < 1){
                     echo $lng.",".$lat.",0 ";
		}
                $line_count ++;
                if($line_count > 3000){
                    $blocks++;
                    echo '</coordinates></LineString></Placemark>'.PHP_EOL;
                    echo '<Placemark><name>Block '.$blocks.' (3000 points max)</name><LineString><coordinates>'.PHP_EOL;
                    $line_count = 0;
                }
            }
            echo '</coordinates></LineString></Placemark>'.PHP_EOL;
            echo '</Document></kml>'.PHP_EOL;
        });

        $response->setStatusCode('200');
        $response->headers->set('Contents-Type', 'application/vnd.google-earth.kml+xml; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="'.$vehicle.' '.$daterange. '.kml"');
	return $response;
    }

    /**
     * @Route("/nmea-polyline/{vehicle}/daterange/{daterange}", name="nmea_polyline_daterange")
     */
    public function nmeaPolylineDaterangeAction(Request $request, $vehicle, $daterange){
        $points = [];
        $nmea = [];
	$range = $this->splitDateRange($daterange);
	$em = $this->getDoctrine()->getManager();
	$results = $em->getRepository("AppBundle:CampData")->findByVehicleAndDateRange($vehicle, $range);
	$points = [];
	foreach ($results as $r){
            $points[] = [$r->getLatitude(), $r->getLongitude(), $r->getDistance()];
	}
        return $this->render('default/nmea-polygen.html.twig',[
            "files" => [],
            "user" => $this->getUser(),
            "polygons" => false,
            "nmea" => [
                "name" => "camp data: (".$vehicle.")",
                "text" => "",
                "points" => $points
            ]]);
    }

    /**
     * @Route("/nmea-polyline/permalink/{uid}", name="nmea_polyline_permalink")
     */
    public function nemaPolylinePermalinkAction(Request $request, $uid){
        $dumppath = realpath($this->getParameter('kernel.root_dir').'/../web/permalinks/'.$uid);
        $points = [];
        $nmea = explode("\n", file_get_contents($dumppath));
        $lastdata = explode(",", $nmea[0]);
        foreach ($nmea as $line){
            if ($line != ""){
                $newdata = explode(",", $line);
                if ((abs($newdata[0] - $lastdata[0]) < 0.5) && (abs($newdata[1]-$lastdata[1]) < 0.5)){
                    $points[] = $newdata;
                    $lastdata = $newdata;
                }
            }
        }
        return $this->render('default/nmea-polygen.html.twig',[
            "files" => [],//"dumppath" => $dumppath, "files" => $files],
            "user" => $this->getUser(),
            "polygons" => false,
            "nmea" => [
                "name" => $uid,
                "text" => "",
                "points" => $points
        ]]);
    }


    /**
     * @Route("/nmea-polygen", name="nmea_polygen")
     */
    public function nmeaPolygenAction(Request $request){
        $files = $request->files;
        $nmea = [];
        $name = "";
        $points = [];
        $uid  = "";
	$dump = "";
        foreach ($files as $uploadedFile){
            $name = $uploadedFile->getClientOriginalName();
            $path = $uploadedFile->getRealPath();
            if (strpos(strrev($name), "2zb.") == 0){
                //system("mv ".$path." ".$path.".bz2");
                system("bunzip2 ".$path);
		$nmea = explode("\n",file_get_contents($path.".out"));
            } else {
                $nmea = explode("\n",file_get_contents($path));
            }
            if ($nmea != []){
                $uid = uniqid();
                $dumppath = $this->getParameter('kernel.root_dir').'/../web/permalinks/'.$uid;
                $dumpfile = fopen($dumppath,"w");
            }

            foreach ($nmea as $line){
                $data = ["latitude" => null, "longitude" => null, "speed" => null];
                if (strpos($line, "RMC") != false){
                    $parts = explode(',', $line);
                    $latitude = substr($parts[3],0,2);
                    $decimal_latitud = substr($parts[3], 2);
                    $data['latitude']  = (($parts[4]=='S')?-1:1) * ($latitude+($decimal_latitud/60));

                    $longitude = substr($parts[5],0,3);
                    $decimal_longitude = substr($parts[5], 3);
                    $data['longitude'] = (($parts[6]=='W')?-1:1) * ($longitude+($decimal_longitude/60));

                    $data["speed"] = $parts[7] * 1.852; // knots -> km/h
                }
		if (($data["latitude"] != null) && ($data["longitude"] != null)){
                    if ($data["speed"] > 4){
                        $points[] = [$data["latitude"], $data["longitude"], $data["speed"]];
                        fwrite($dumpfile, $data["latitude"] .",". $data["longitude"] .",". $data["speed"] .",".PHP_EOL);
                    }
                }
            }
	}
	if ($uid != ""){
            fclose($dumpfile);
        } else {
            $dumppath = "";
        }
        return $this->render('default/nmea-polygen.html.twig',[
            "files" => ["dumppath" => $dumppath, "files" => $files],
            "user" => $this->getUser(),
            "polygons" => true,
            "nmea" => [
                "name" => $name,
                "text" => $nmea,
                "points" => $points
        ]]);

    }



    /**
     * @Route("/polygen/{id}", name="polygen")
     * Google Maps-based Polygon generator demo page
     */
    public function polygenAction(Request $request, $id = null)
    {
	$em = $this->getDoctrine()->getManager();
	$trips = $this->getDoctrine()->getRepository('AppBundle:Monitoreo');

	if ($id == null){
		$lastmon = $trips->findOneBy([],['id' => 'DESC']);
	} else {
		$lastmon = $trips->findOneBy(['id' => $id]);
		if ($lastmon == null){
			return new JsonResponse([ "error" => "No existe recorrido con ID ".$id ]);
		}
	}

	$gpsdata = $em->getRepository('AppBundle:GpsData')->findBy(['uuid' => $lastmon->getUuid()],['fecha' => 'DESC']);

	return $this->render('default/polygen.html.twig',[
		'trips' => $trips->findBy([],['id' => 'DESC']),
		'current_trip' => $lastmon,
		'gpsdata' => $gpsdata,
	]);
    }

    /**
     * @Route("/assign_uuid_to_old_trips")
     */
    public function assignUUIDToOldTripsAction(Request $request){
	$em = $this->getDoctrine()->getManager();
	$trips = $em->getRepository("AppBundle:Monitoreo")->findAll();

	$result = "".PHP_EOL;
        $sql = "----".PHP_EOL;
	foreach ($trips as $monitoreo){

		// Thanks to the one true god above at http://stackoverflow.com/questions/2040240/php-function-to-generate-v4-uuid
		$uuid = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),  // 32 bits for "time_low"
			mt_rand( 0, 0xffff ), // 16 bits for "time_mid"
			mt_rand( 0, 0x0fff ) | 0x4000, // 16 bits for "time_hi_and_version" (version 4)
			// two most significant bits holds zero and one for variant DCE1.1
			mt_rand( 0, 0x3fff ) | 0x8000, // 8 bits for "clk_seq_hi_res", 8 for "clk_seg_low"
			mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ) // 48 bits for "node"
		);
		//if ($monitoreo->getUuid() == null){
			$monitoreo->setUuid($uuid);
			$result .= $monitoreo->getNombre() ." gets UUID ".$uuid.PHP_EOL;
		//}
		$em->flush();

		$sql .= "UPDATE gpsdata set id_sesion='".$uuid."' where id >= ". $monitoreo->getGpsinicial()->getId().
				" and id <= ".$monitoreo->getGpsfinal()->getId().";".PHP_EOL;
	}
	return new Response($result . $sql);
    }
}
