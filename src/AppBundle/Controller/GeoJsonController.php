<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use Doctrine\ORM\Query\ResultSetMapping;

class GeoJsonController extends Controller
{
	private $_lastPolygon = null;
	private $_lastVertices = [];

	private function getGpsDataRange($gpsdata1, $gpsdata2){
		$em = $this->getDoctrine()->getManager();
		$qb = $em->createQueryBuilder();
		$query = $qb->select('gpsdata')
			->from('AppBundle\Entity\Gpsdata','gpsdata');

		$query->where('gpsdata.id >= :gpsinicial')
			->andWhere('gpsdata.id <= :gpsfinal')
			->setParameter('gpsinicial', $gpsdata1)
			->setParameter('gpsfinal', $gpsdata2);
		$query = $query->getquery();
		return $query->getResult();
	}


	private function rot2d($sin, $cos, $xy){
		$newX = ($xy[0] * $cos) - ($xy[1] * $sin);
		$newY = ($xy[0] * $sin) + ($xy[1] * $cos);
		return [$newX, $newY];
	}


	private function isGpsDataInsideLastPolygon($gpsdata){
		if ($this->_lastPolygon == null){
			return false;
		}
		$lat = $gpsdata->getLatitud();
		$lng = $gpsdata->getLongitud();
		$sql = "SELECT ST_CONTAINS(".
			$this->_lastPolygon .", GeomFromText('POINT(".
			$lng ." ". $lat .")')) as inside";

		//$sql = "SELECT ST_CONTAINS(PolygonFromText('POLYGON((-1 -1, -1 1, 1 1, 1 -1, -1 -1))'), GeomFromText('POINT(0 0.9)')) as inside;";

		$rsm = new ResultSetMapping();
		$rsm->addScalarResult('inside', 'inside');

		$em = $this->getDoctrine()->getManager();
		$query = $em->createNativeQuery($sql, $rsm);
		$result = $query->getResult();

		if ($result[0]['inside'] == '1') return true;
		return false;
	}


	private function getPolygonFromPosition($gpsdata, $lastLat, $lastLng){

		$lat = $gpsdata->getLatitud();
		$lng = $gpsdata->getLongitud();
		$distance = 0.00008;  // Adjust to get bigger/smaller polygons

		// First polygon (square)
		if ($lastLat == 0){
			$a = [$lng - $distance, $lat - $distance];
			$b = [$lng + $distance, $lat - $distance];
			$c = [$lng + $distance, $lat + $distance];
			$d = [$lng - $distance, $lat + $distance];
			$e = [$lng - $distance, $lat - $distance];
			$this->_lastVertices = [$a, $b, $c, $d, $e];

			$this->_lastPolygon = "PolygonFromText('POLYGON((". 
				$a[0] ." ". $a[1] .",".
				$b[0] ." ". $b[1] .",".
				$c[0] ." ". $c[1] .",".
				$d[0] ." ". $d[1] .",".
				$e[0] ." ". $e[1] ."))')";
			return $this->Polygon(
				['description' => 'BEGIN',
				 'gpsid' => $gpsdata->getId(),
				 'strokeColor' => '#ff0000',
				 'strokeWeight' => '1.5',
				 'sqlspatial' => $this->_lastPolygon
/*				 'kml_placemark' => 
					'<Placemark>'.PHP_EOL.
					'	<name></name>'.PHP_EOL.
					'	<styleUrl>#start</styleUrl>'.PHP_EOL.
					'	<description></description>'.PHP_EOL.
					'	<Polygon>'.PHP_EOL.
					'		<gx:drawOrder>1</gx:drawOrder>'.PHP_EOL.
					'		<coordinates>'. $pmdata->getIdGps()->getLongitud() .','. $pmdata->getIdGps()->getLatitud() .",0 " .'</coordinates>'.PHP_EOL.
					'	</Polygon>'.PHP_EOL.
					'</Placemark>'.PHP_EOL*/
				],
				[ $this->_lastVertices ]);
		} else {
			$cad = ($lng - $lastLng);
			$cop = ($lat - $lastLat);
			$hip = sqrt(($cad*$cad) + ($cop*$cop));

			if ($hip == 0) { return null; } // Please do not divide by zero

			$sin = $cad / $hip;
			$cos = $cop / $hip;

			$angle1 = asin($sin);
			$angle2 = acos($cos);

			//$a = $this->rot2d($sin, $cos, [- $distance, - $distance]);
			$b = $this->rot2d($sin, $cos, [+ $distance, - $distance]);
			//$c = $this->rot2d($sin, $cos, [+ ($distance*1.5), 0]);
			$c = $this->rot2d($sin, $cos, [+ $distance, + $distance]);
			$d = $this->rot2d($sin, $cos, [- $distance, + $distance]);
			//$e = $this->rot2d($sin, $cos, [- $distance, - $distance]);

			//$a = [$a[1]+ $lng, $a[0]+ $lat];
			$a = $this->_lastVertices[1];
			$b = [$b[1]+ $lng, $b[0]+ $lat];
			$c = [$c[1]+ $lng, $c[0]+ $lat];
			$d = $this->_lastVertices[2];
			$e = $a;
			//$d = [$d[1]+ $lng, $d[0]+ $lat];
			//$e = [$e[1]+ $lng, $e[0]+ $lat];

			$this->_lastVertices = [$a, $b, $c, $d, $e];

			$this->_lastPolygon = "PolygonFromText('POLYGON((". 
				$a[0] ." ". $a[1] .",".
				$b[0] ." ". $b[1] .",".
				$c[0] ." ". $c[1] .",".
				$d[0] ." ". $d[1] .",".
				$e[0] ." ". $e[1] ."))')";

			return $this->Polygon(
				['description' => 'MOVING',
				 'gpsid' => $gpsdata->getId(),
				 'strokeColor' => '#000000',
				 'strokeWeight' => '1.5'
				],
				[[ $a, $b, $c, $d, $e ]]);
		}
	}


	private function Point($properties, $lng, $lat){
		return array(
			"type" => "Feature",
			"geometry" => array(
				"type" => "Point",
				"coordinates" => array( $lng, $lat )
			),
			"properties" => $properties
		);
	}

	// "properties" follow Google Map properties: 
	// [ http://www.w3schools.com/googleapi/google_maps_overlays.asp ]
	private function LineString($properties, $latLngArray){
		return array(
			"type" => "Feature",
			"geometry" => array(
				"type" => "LineString",
				"coordinates" => $latLngArray
			),
			"properties" => $properties
		);
	}


	// "properties" follow Google Map properties: 
	// [ http://www.w3schools.com/googleapi/google_maps_overlays.asp ]
	private function Polygon($properties, $latLngArray){
		return array(
			"type" => "Feature",
			"geometry" => array(
				"type" => "Polygon",
				"coordinates" => $latLngArray
			),
			"properties" => $properties
		);
	}


	/**
	 * @Route("/geojson/roadcreator/{gpsdata1},{gpsdata2}", name="geojson_roadcreator")
	 */
	public function roadCreatorAction(Request $request, $gpsdata1, $gpsdata2)
	{
		$em = $this->getDoctrine()->getManager();
		$features = [];
		$chile = new \DateTimeZone("Chile/Continental"); // 'Chile/Continental' equals GMT-3

		$results = $this-> getGpsDataRange($gpsdata1, $gpsdata2);
		$lastLat = 0;
		$lastLng = 0;

		foreach ($results as $gpsdata){
			if ($this->isGpsDataInsideLastPolygon($gpsdata) == true){
				$polygon = null;
			} else {
				$polygon = $this->getPolygonFromPosition($gpsdata, $lastLat, $lastLng);
			}
			if ($polygon != null){
				$features[] = $polygon;
			}
			$lastLat = $gpsdata->getLatitud();
			$lastLng = $gpsdata->getLongitud();
		}
		// TODO: Include photodata infowindows
		$geojson = array(
			"type" => "FeatureCollection",
			"features" => $features
		);
		return new JsonResponse($geojson);
	}



	/**
	 * @Route("/geojson/measuretrip/{uuid}", name="geojson_measuretrip")
	 */
	public function geoJsonMeasureTrip(Request $request, $uuid)
	{
		$em = $this->getDoctrine()->getManager();
		$features = [];
		$chile = new \DateTimeZone("Chile/Continental"); // 'Chile/Continental' equals GMT-3

		// PMDATA
		$qb = $em->createQueryBuilder();
		$query = $qb->select('pmdata')
			->from('AppBundle\Entity\Pmdata','pmdata')
			->join('pmdata.idGps','gpsdata');

		$query->where('gpsdata.uuid = :uuid')
			//->andWhere('gpsdata.speed >= 10')
			->setParameter('uuid', $uuid);
		$query = $query->getquery();
		$results = $query->getResult();

		foreach ($results as $pmdata){
			$fecha = $pmdata->getIdGps()->getFecha();
			if ($fecha == null){
				$fecha_y_hora = '<b>fecha=null</b>';
			} else {
				$fecha->setTimezone($chile);
				$fecha_y_hora = '<b>'. $fecha->format('d/m/Y H:i:s').'</b>';
			}
			$description = $fecha_y_hora; /*.
				'<hr><b>PM10 aire: </b>'. $pmdata->getPm10lat_u() .
				'<br><b>PM2.5 aire: </b>'. $pmdata->getPm25lat_u().
				'<hr><b>PM10 tierra: </b>'. $pmdata->getPm10lat_d() .
				'<br><b>PM2.5 tierra: </b>'. $pmdata->getPm25lat_d().
				'<hr><b>Velocidad: </b>'. round($pmdata->getIdGps()->getSpeed(), 1) .' Km/h'; */

			$pm10_d = $pmdata->getPm10lat_d();
			$pm10_u = $pmdata->getPm10lat_u();

			$features[] = $this->Point(
				[//'icon_d' => $icon_d,
 				 //'icon_u' => $icon_u,
				 'description'=> $description,
				 'gpsid' => $pmdata->getIdGps()->getId(),
				 'type' => 'pm',
                 'sensor_up' => [
                    'pm10' => round($pm10_u,1),
                    'pm25' => round($pmdata->getPm25lat_u(), 1)
                 ],
                 'sensor_down' => [
                    'pm10' => round($pm10_d,1),
                    'pm25' => round($pmdata->getPm25lat_d(), 1)
                 ],
                 'speed' => round($pmdata->getIdGps()->getSpeed(), 1)
                 ],
				$pmdata->getIdGps()->getLongitud(),
				$pmdata->getIdGps()->getLatitud());
		}

		// PHOTODATA
		$qb = $em->createQueryBuilder();
		$query = $qb->select('photodata')
			->from('AppBundle\Entity\Photodata','photodata')
			->where('photodata.uuid = :uuid')
			->setParameter('uuid', $uuid);
		$query = $query->getquery();
		$results = $query->getResult();

		foreach ($results as $photodata){
			$fecha = $photodata->getFecha();
			if ($fecha != null) {
				$fecha->setTimezone($chile);
				$fecha_y_hora = '<b>'. $fecha->format('d/m/Y H:i:s').'</b>';
			} else {
				$fecha_y_hora = '<b>Foto</b>';
			}
			$features[] = $this->Point(
				['icon' => 'bundles/app/img/photo.png',
				 'description' => $fecha_y_hora .'<hr><div style="width:100%; height:100%; overflow:hidden;"><b>'. $photodata->getComment() .'</b><hr>'.
					'<img style="width:100%; height:100%; overflow:hidden;" src="images/'. $photodata->getFilename() .'"></img></div>',
				 'type' => 'photo'],
				$photodata->getLongitud(),
				$photodata->getLatitud());
		}

		// SIGHTDATA
		$qb = $em->createQueryBuilder();
		$query = $qb->select('sightdata')
			->from('AppBundle\Entity\Sightdata','sightdata')
			->where('sightdata.uuid = :uuid')
			->setParameter('uuid', $uuid);
		$query = $query->getquery();
		$results = $query->getResult();

		foreach ($results as $sightdata){
			//$fecha = $sightdata->getIdGps()->getFecha();
			//$fecha->setTimezone($chile);
			//$fecha_y_hora = '<b>'. $fecha->format('d/m/Y H:i:s').'</b>';
			$fecha_y_hora = '<b>Observacion</b>';
			$features[] = $this->Point(
				['description'=> $fecha_y_hora .'<hr>'. $sightdata->getValue(),
				 'type' => ($sightdata->getValue() == "BACHE")? 'hole':'observation'],
				$sightdata->getLongitud(),
				$sightdata->getLatitud());
		}

		// TODO: Include photodata infowindows
		$geojson = array(
			"type" => "FeatureCollection",
			"features" => $features
		);
		return new JsonResponse($geojson);
	}


    /**
     * @Route("/geojson/test", name="geojson_test")
     */
    public function geoJsonTestAction(Request $request)
    {
		return new JsonResponse(array(
			"type" => "FeatureCollection",
			"features" => array(
				$this->Point([
					"icon" => "https://storage.googleapis.com/support-kms-prod/SNP_2752129_en_v0",
					"description" => '<div style="width:100%; height:100%; overflow:hidden;">Some serious <b>HTML</b><sup>5</sup><hr>'.
					'<div class="sidebar-alerts" style="width: 100%; overflow:hidden; background-color: #333540; text-align: left;">'.
					'	<div class="alert fade in">'.
					'	<a href="#" class="close" data-dismiss="alert" aria-hidden="true">×</a>'.
					'	<span class="text-white fw-semi-bold">PM10</span> <br>'.
					'	<div class="bg-gray-transparent progress-bar">'.
					'		<progress class="progress progress-xs progress-success mt-xs mb-0" value="100" max="100" style="width: 22%"></progress>'.
					'	</div>'.
					'	<small>Bajo (22%)</small>'.
					'</div>'.
					'<img style="width:100%; height:100%; overflow:hidden;" src="https://gitlab.com/uploads/user/avatar/162263/1958094_807822065898317_1214587778_n.jpg"></img></div>'
					], -69.250631, -22.792022),
				$this->Point([], -69.251631, -22.791022),
				$this->LineString([
						"strokeColor" => "red",
						"strokeWeight" => 4,
						"strokeOpacity" => 0.5
					],[
				])
			)
		));
    }
}
