<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Upload;
use AppBundle\Entity\Photodata;
use AppBundle\Entity\Sightdata;
use AppBundle\Entity\Monitoreo;
use AppBundle\Entity\Message;
use AppBundle\Entity\IrrigationData;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class SyncController extends Controller
{

	/**
	 * @route("/announce/startup/{id_maleta}/{id_sesion}/{fecha}", name="announce_startup");
	 */
	public function announceStartupAction(Request $request,
		$id_maleta = null, $id_sesion = null, $fecha = null)
	{
		$em = $this->getDoctrine()->getManager();
		$trip = $em->getRepository("AppBundle:Monitoreo")->findByUuid($id_sesion);

		if ($trip == []){
			$trip = new Monitoreo();
			$trip->setIdmaleta($id_maleta);
			$trip->setUuid($id_sesion);
			$trip->setFecha(new \DateTime());
			$em->persist($trip);
			$em->flush();
			return new JsonResponse([
				"status" => "OK",
				"monitoreo" => $trip,
			]);
		} else {
			return new JsonResponse([
				"status" => "DUPLICATE_UUID",
				"monitoreo" => $trip,
			]);
		}
	}

	/**
	 * @Route("/comment/upload/{uuid}/{date}/{time}/{lat}/{lng}/{message}", name="comment_upload");
	 */
	public function uploadCommentAction(Request $request,
		$uuid = null,$date = null,$time = null,$lat = null,$lng = null,$message = null){

		$comment = new Sightdata();
		$comment->setUuid($uuid)
			->setFecha(new \DateTime())
			->setUtctime($time)
			->setDate($date)
			->setLatitud($lat)
			->setLongitud($lng)
			->setValue($message);

		$em = $this->getDoctrine()->getManager();
		$em->persist($comment);
		$em->flush();

		return new JsonResponse(array(
			"status" => "OK",
			"uuid" => $uuid,
			"date" => $date,
			"time" => $time,
			"lat" => $lat,
			"lng" => $lng,
			"message" => $message,
			"sightdata_id" => $comment->getId()
		));
	}

	/**
	 * @Route("/photo/upload/{uuid}/{date}/{time}/{lat}/{lng}/{message}", name="photo_upload");
	 */
	public function uploadPhotoAction(Request $request,
		$uuid = null,$date = null,$time = null,$lat = null,$lng = null,$message = null){

                $fileBag = $request->files->all();
		//return new Response(var_export($fileBag));
                $uploadedFile = $fileBag['file'];

		//return new Response(var_export($uploadedFile));

                $originalName = $uploadedFile->getclientOriginalName();
                $newLocation = $this->get('kernel')->getRootDir() .'/../web/upload/';
                $newFullPath = $newLocation.$originalName;
                $uploadedFile->move($newLocation, $originalName);

                $document = new Upload();
                $document->setNombre($originalName);
                $em = $this->getDoctrine()->getManager();
                $em->persist($document);

		$now = new \DateTime();
		$photo = new Photodata();
		$photo->setLatitud($lat)
			->setLongitud($lng)
			->setUuid($uuid)
			->setFilename($originalName)
			->setFecha($now)
			->setComment($message);

		$em->persist($photo);
		$em->flush();

		return new JsonResponse(array(
			"status" => "OK",
			"uuid" => $uuid,
			"date" => $date,
			"time" => $time,
			"lat" => $lat,
			"lng" => $lng,
			"message" => $message,
			"photodata_id" => $photo->getId()
		));
	}

        /**
         * @Route("/datablock/upload", name="datablock_upload");
         */
	public function datablockAction(Request $request){
		$fileBag = $request->files->all();
		$uploadedFile = $fileBag['file'];

		$originalName = $uploadedFile->getclientOriginalName();
		$newLocation = $this->get('kernel')->getRootDir() .'/../web/upload/';
		$newFullPath = $newLocation.$originalName;
		$uploadedFile->move($newLocation, $originalName);

		$document = new Upload();
		$document->setNombre($originalName);
		$em = $this->getDoctrine()->getManager();
		$em->persist($document);
		$em->flush();

		return new Response(var_export($document));
	}

	/**
	 * @Template()
	 * //Security("has_role('ROLE_SYNC')")
	 * @Route("/sync", name="sync")
	 */
	public function uploadAction(Request $request)
	{
		$document = new Upload();
		$form = $this->createFormBuilder($document)
			->add('file', FileType::class)
			->getForm();
			$form->add('submit', SubmitType::class, array('label' => 'Subir'));

		$form->handleRequest($request);
			if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$document->setNombre($document->getFile()->getClientOriginalName());
			$document->upload();

			$em->persist($document);
			$em->flush();
			return $this->redirect($this->generateUrl('sync'));
		}
		return array('form' => $form->createView());
	}


	/**
	 * @Route("/valve/set/{value}", name="valve_set")
	 */
	public function valveSetAction(Request $request, $value){
		$valveStatus = file_get_contents("/tmp/valve.txt");
		file_put_contents("/tmp/valve.txt", $value);
		return new JsonResponse([
			"previous_value" => $valveStatus,
			"new_value" => $value,
		]);
	}


	/**
	 * @Route("/valve/status", name="valve_status")
	 */
	public function valveStatusAction(Request $request){
		$valveStatus = file_get_contents("/tmp/valve.txt");
		$countStatus = file_get_contents("/tmp/count.txt");

		file_put_contents("/tmp/count.txt", trim($countStatus) +1);

		return new JsonResponse([
			"valve" => trim($valveStatus),
			"count" => trim($countStatus),
		]);
	}


	/**
	 * @Route("/irrigationdata/add", name="irrigationdata_update")
	 */
	public function irrigationDataUpdateAction(Request $request){
		$json = json_decode($request->request->get('json'), true);
		$em = $this->getDoctrine()->getManager();
		try {
			$irrigationData = new IrrigationData();
			$irrigationData->setDateTime(new \DateTime($json['datetime']))
				->setIdcamion($json['camion'])
				->setLat($json['lat'])
				->setLon($json['lon'])
				->setP1($json['p1'])
				->setP2($json['p2'])
				->setP3($json['p3'])
				->setFl($json['fl'])
				->setPos($json['pos'])
				->setComment($json['comment']);
			if (array_key_exists("time", $json)){
				$irrigationData->setDeltatiempo($json['time']);
			}
			$em->persist($irrigationData);
			$em->flush();

			return new JsonResponse([
				"status" => "INSERT_OK"
			]);
		} catch (DBALException $e) {
			return new JsonResponse([
				"status" => "INSERT_FAILED",
				"message" => $e->getMessage()
			]);
		}
	}

	/**
	 * @Route("/messages/add", name="messages_add")
	 */
	public function messagesAddAction(Request $request){
		$json = json_decode($request->request->get('json'), true);
		$em = $this->getDoctrine()->getManager();
                try {
                        $message = new Message();
                        $message->setDatetime(new \DateTime($json['datetime']))
                                ->setLat($json['lat'])
				->setLon($json['lon'])
				->setMessage($json['message']);
			$em->persist($message);
			$em->flush();
			return new JsonResponse(["status" => "INSERT_OK"]);
		} catch (DBALException $e){
			return new JsonResponse(["status" => "INSERT_FAILED", "message" => $e->getMessage()]);
		}

	}


	/**
	 * @Route("/messages/last/{count}", name="messages_last")
	 */
	public function messagesLastAction(Request $request, $count = 10){
		$em = $this->getDoctrine()->getManager();
		$messages = $em->getRepository("AppBundle:Message")->findBy([],["id" => "DESC"], $count);
		return new JsonResponse($messages);
	}


}

