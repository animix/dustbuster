<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

class MonitorController extends Controller
{
	// StreamedResponse persistent data hack. See getPmdata()
	private $_lastId;

	// Grabbing all pmdata for a given monitor trip.
	// Meant to be reused by output file generators (CSV, Excel, KML...)
	function getPmdata($id_monitoreo, $filter = true){
		$em = $this->getDoctrine()->getManager();
		$monitoreo  = $em->getRepository('AppBundle:Monitoreo')->findOneById($id_monitoreo);
		if ($monitoreo == null){
			return [];
		}
		$uuid = $monitoreo->getUuid();

		// PMDATA
		$em = $this->getDoctrine()->getManager();
		$qb = $em->createQueryBuilder();
		$query = $qb->select('pmdata')
			->from('AppBundle\Entity\Pmdata','pmdata')
			->join('pmdata.idGps','gpsdata')
			->where('gpsdata.uuid = :uuid')
			->orderBy('gpsdata.fecha', 'ASC');

		if ($filter == true){
			$query->andWhere('gpsdata.speed >= 10');
		}

		$query->setParameter('uuid', $uuid);
		$query = $query->getquery();
		return $query->getResult();
	}


    /**
     * @Route("/monitor/excel/{id}", name="monitor_excel")
     */
    public function excelAction(Request $request, $id=null)
    {
		if ($id == null){
			throw new Exception("no id specified");
		}
		$this->_lastId = $id;
		$chile = new \DateTimeZone("GMT-4"); // 'Chile/Continental' equals 'GMT-3'
		$resultados = $this->getPmdata($this->_lastId, false);

		$excel = $this->get('phpexcel')->createPHPExcelObject();
		$excel->setActiveSheetIndex(0);
		$excel->getActiveSheet()->setTitle('Monitoreo '. $id);

		$writer = $this->get('phpexcel')->createWriter($excel, 'Excel5');
		$response = $this->get('phpexcel')->createStreamedResponse($writer);

		// Consuming more memory to make sure PHPExcel runs fast
		$cells = [['Fecha','Latitud','Longitud','PM10 Aire','PM2.5 Aire','PM10 Camino','PM2.5 Camino','Velocidad']];
		foreach ($resultados as $pmdata){
			$cells[] = [
				$pmdata->getIdGps()->getFecha()->setTimezone($chile)->format("d-m-Y H:i:s"),
				$pmdata->getIdGps()->getLatitud(),
				$pmdata->getIdGps()->getLongitud(),
				$pmdata->getPm10lat_u(),
				$pmdata->getPm25lat_u(),
				$pmdata->getPm10lat_d(),
				$pmdata->getPm25lat_d(),
				round($pmdata->getIdGps()->getSpeed(), 1)];
		}
		$excel->getActiveSheet()->fromArray($cells, NULL, 'A1');

		$response->setStatusCode('200');
		$response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=utf-8');
		$response->headers->set('Content-Disposition', 'attachment; filename="Monitoreo '.$id.'.xls"');
		return $response;
	}


	/**
	 * @Route("/monitor/kml/{id}", name="monitor_kml")
	 */
	public function kmlAction(Request $request, $id=null)
    {
		if ($id == null){
			throw new Exception("no id specified");
		}
		$this->_lastId = $id;
		$response = new StreamedResponse();

		$response->setCallback(function(){
			$file = fopen('php://output', 'w+');
			$chile = new \DateTimeZone("GMT-4"); // 'Chile/Continental' equals 'GMT-3'

			// Beware: this one is a bit longer than usual.
			fputs($file,
				'<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL.
				'<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">'.PHP_EOL.
				'<Document>'.PHP_EOL.
				'	<name>Monitoreo '. $this->_lastId .'</name>'.PHP_EOL.

				'	<Style id="pm_red">'.PHP_EOL.
				'		<IconStyle>'.PHP_EOL.
				'			<hotSpot x="0.5"  y="0.5" xunits="fraction" yunits="fraction"/>'.PHP_EOL.
				'			<scale>0.3125</scale>'.PHP_EOL.
				'			<Icon><href>https://spence.eye3.cl/bundles/app/img/pm_red.png</href></Icon>'.PHP_EOL.
				'		</IconStyle>'.PHP_EOL.
				'	</Style>'.PHP_EOL.

				'	<Style id="pm_green">'.PHP_EOL.
				'		<IconStyle>'.PHP_EOL.
				'			<hotSpot x="0.5"  y="0.5" xunits="fraction" yunits="fraction"/>'.PHP_EOL.
				'			<scale>0.3125</scale>'.PHP_EOL.
				'			<Icon><href>https://spence.eye3.cl/bundles/app/img/pm_green.png</href></Icon>'.PHP_EOL.
				'		</IconStyle>'.PHP_EOL.
				'	</Style>'.PHP_EOL.

				'	<Style id="pm_orange">'.PHP_EOL.
				'		<IconStyle>'.PHP_EOL.
				'			<hotSpot x="0.5"  y="0.5" xunits="fraction" yunits="fraction"/>'.PHP_EOL.
				'			<scale>0.3125</scale>'.PHP_EOL.
				'			<Icon><href>https://spence.eye3.cl/bundles/app/img/pm_orange.png</href></Icon>'.PHP_EOL.
				'		</IconStyle>'.PHP_EOL.
				'	</Style>'.PHP_EOL.

				'	<Style id="pm_white">'.PHP_EOL.
				'		<IconStyle>'.PHP_EOL.
				'			<hotSpot x="0.5"  y="0.5" xunits="fraction" yunits="fraction"/>'.PHP_EOL.
				'			<scale>0.3125</scale>'.PHP_EOL.
				'			<Icon><href>https://spence.eye3.cl/bundles/app/img/pm_white.png</href></Icon>'.PHP_EOL.
				'		</IconStyle>'.PHP_EOL.
				'	</Style>'.PHP_EOL);

			$resultados = $this->getPmdata($this->_lastId);

			// LineString recorrido
			fputs($file,
				'<Placemark>'.PHP_EOL.
				'       <name>Polyline Recorrido '. $id .'</name>'.PHP_EOL.
				'       <LineString>'.PHP_EOL.
				'               <coordinates>'.PHP_EOL);
			foreach ($resultados as $pmdata){
				fputs($file, $pmdata->getIdGps()->getLongitud() .','. $pmdata->getIdGps()->getLatitud() .",0 ".PHP_EOL);
			}
			fputs($file,
				'		</coordinates>'.PHP_EOL.
				'	</LineString>'.PHP_EOL.
				'</Placemark>'.PHP_EOL);

			foreach ($resultados as $pmdata){
				// Choose a color based on pm10 values
				$pm10 = $pmdata->getPm10lat_d();
				$fecha = $pmdata->getIdGps()->getFecha();
				if ($fecha != null){
					$texto_fecha = "fecha = null";
				} else {
					$texto_fecha = $fecha->setTimezone($chile)->format("d-m-Y H:i:s");
				}
				$description = '<![CDATA[<b>'. $texto_fecha .
					'</b><br><b>PM10: </b>'. 
					$pmdata->getPm10lat_d(). 
					'<br><b>PM2.5: </b>'. 
					$pmdata->getPm25lat_d() .']]>';

				if ($pm10 <= 299){ 
					$pm_color = "#pm_green"; // 0-299 pcc
				} else {
					if (($pm10 > 299) && ($pm10 < 600)){
						$pm_color = "#pm_orange";
					} else {  // $pm10 >= 600
						if ($pm10 == "6527.9"){
							$pm_color = "#pm_white"; // NO DUSTMATE DATA				
							$description = '<![CDATA[<b>'. 
								$pmdata->getIdGps()->getFecha()
								->setTimezone($chile)->format("d-m-Y H:i:s") .
								'</b><br><b>Recorrido realizado sin dustmate</b>]]>';
						} else {
							$pm_color = "#pm_red";
						}
					}
				}

				fputs($file,
					'<Placemark>'.PHP_EOL.
					'	<name></name>'.PHP_EOL.
					'	<styleUrl>'. $pm_color .'</styleUrl>'.PHP_EOL.
					'	<description>'. $description .PHP_EOL.
					'	</description>'.PHP_EOL.
					'	<Point>'.PHP_EOL.
					'		<gx:drawOrder>1</gx:drawOrder>'.PHP_EOL.
					'		<coordinates>'. $pmdata->getIdGps()->getLongitud() .','. $pmdata->getIdGps()->getLatitud() .",0 " .'</coordinates>'.PHP_EOL.
					'	</Point>'.PHP_EOL.
					'</Placemark>'.PHP_EOL);
			}			

			fputs($file,
				'	</Document>'.PHP_EOL.
				'</kml>'.PHP_EOL);

			fclose($file);
		});
		
		$response->setStatusCode('200');
		$response->headers->set('Content-Type', 'application/vnd.google-earth.kml+xml; charset=utf-8');
		$response->headers->set('Content-Disposition', 'attachment; filename="Monitoreo '.$id.'.kml"');
		return $response;
	}


	
    /**
     * @Route("/monitor/excelxml/{id}", name="monitor_excelxml")
     */
    public function excelXmlAction(Request $request, $id=null)
    {
		if ($id == null){
			throw new Exception("no id specified");
		}
		$this->_lastId = $id;
		$response = new StreamedResponse();

		$response->setCallback(function(){
			$file = fopen('php://output', 'w+');
			$chile = new \DateTimeZone("GMT-4"); // 'Chile/Continental' equals 'GMT-3'

			fputs($file,
				'<?xml version="1.0"?>'.PHP_EOL.
				'<?mso-application progid="Excel.Sheet"?>'.PHP_EOL.
				'<Workbook'.PHP_EOL.
				'   xmlns="urn:schemas-microsoft-com:office:spreadsheet"'.PHP_EOL.
				'   xmlns:o="urn:schemas-microsoft-com:office:office"'.PHP_EOL.
				'   xmlns:x="urn:schemas-microsoft-com:office:excel"'.PHP_EOL.
				'   xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"'.PHP_EOL.
				'   xmlns:html="http://www.w3.org/TR/REC-html40">'.PHP_EOL.
				'  <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">'.PHP_EOL.
				'  </ExcelWorkbook>'.PHP_EOL.
				'  <Styles>'.PHP_EOL.
				'    <Style ss:ID="Default" ss:Name="Normal">'.PHP_EOL.
				'      <Alignment ss:Vertical="Bottom" />'.PHP_EOL.
				'      <Borders />'.PHP_EOL.
				'      <Font />'.PHP_EOL.
				'      <Interior />'.PHP_EOL.
				'      <NumberFormat />'.PHP_EOL.
				'      <Protection />'.PHP_EOL.
				'    </Style>'.PHP_EOL.
				'    <Style ss:ID="s21">'.PHP_EOL.
				'      <Font x:Family="Swiss" ss:Bold="1" />'.PHP_EOL.
				'    </Style>'.PHP_EOL.
				'  </Styles>'.PHP_EOL.
				'  <Worksheet ss:Name="Monitoreo">'.PHP_EOL.
				'    <Table ss:ExpandedColumnCount="2" ss:ExpandedRowCount="5"'.PHP_EOL.
				'	   x:FullColumns="1" x:FullRows="1">'.PHP_EOL.
				'      <Row>'.PHP_EOL.
				'        <Cell ss:StyleID="s21">'.PHP_EOL.
				'          <Data ss:Type="String">Fecha</Data>'.PHP_EOL.
				'        </Cell>'.PHP_EOL.
				'        <Cell ss:StyleID="s21">'.PHP_EOL.
				'          <Data ss:Type="String">Latitud</Data>'.PHP_EOL.
				'        </Cell>'.PHP_EOL.
				'        <Cell ss:StyleID="s21">'.PHP_EOL.
				'          <Data ss:Type="String">Longitud</Data>'.PHP_EOL.
				'        </Cell>'.PHP_EOL.
				'        <Cell ss:StyleID="s21">'.PHP_EOL.
				'          <Data ss:Type="String">Pm10 Aire</Data>'.PHP_EOL.
				'        </Cell>'.PHP_EOL.
				'        <Cell ss:StyleID="s21">'.PHP_EOL.
				'          <Data ss:Type="String">Pm25 Aire</Data>'.PHP_EOL.
				'        </Cell>'.PHP_EOL.
				'        <Cell ss:StyleID="s21">'.PHP_EOL.
				'          <Data ss:Type="String">Pm10 Camino</Data>'.PHP_EOL.
				'        </Cell>'.PHP_EOL.
				'        <Cell ss:StyleID="s21">'.PHP_EOL.
				'          <Data ss:Type="String">Pm25 Camino</Data>'.PHP_EOL.
				'        </Cell>'.PHP_EOL.
				'      </Row>'.PHP_EOL);

			$resultados = $this->getPmdata($this->_lastId);

			foreach ($resultados as $pmdata){
				fputs($file,
					'      <Row>'.PHP_EOL.
					'        <Cell>'.PHP_EOL.
					'          <Data ss:Type="String">'. $pmdata->getIdGps()->getFecha()->setTimezone($chile)->format("d-m-Y H:i:s") .'</Data>'.PHP_EOL.
					'        </Cell>'.PHP_EOL.
					'        <Cell>'.PHP_EOL.
					'          <Data ss:Type="Number">'. $pmdata->getIdGps()->getLatitud() .'</Data>'.PHP_EOL.
					'        </Cell>'.PHP_EOL.
					'        <Cell>'.PHP_EOL.
					'          <Data ss:Type="Number">'. $pmdata->getIdGps()->getLongitud() .'</Data>'.PHP_EOL.
					'        </Cell>'.PHP_EOL.
					'        <Cell>'.PHP_EOL.
					'          <Data ss:Type="Number">'. $pmdata->getPm10lat_u() .'</Data>'.PHP_EOL.
					'        </Cell>'.PHP_EOL.
					'        <Cell>'.PHP_EOL.
					'          <Data ss:Type="Number">'. $pmdata->getPm25lat_u() .'</Data>'.PHP_EOL.
					'        </Cell>'.PHP_EOL.
					'        <Cell>'.PHP_EOL.
					'          <Data ss:Type="Number">'. $pmdata->getPm10lat_d() .'</Data>'.PHP_EOL.
					'        </Cell>'.PHP_EOL.
					'        <Cell>'.PHP_EOL.
					'          <Data ss:Type="Number">'. $pmdata->getPm25lat_d() .'</Data>'.PHP_EOL.
					'        </Cell>'.PHP_EOL.
					'      </Row>'.PHP_EOL);
			}			

			fputs($file, 
				'    </Table>'.PHP_EOL.
				'    <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">'.PHP_EOL.
				'    </WorksheetOptions>'.PHP_EOL.
				'  </Worksheet>'.PHP_EOL.
				'</Workbook>'.PHP_EOL);

			fclose($file);
		});
		
		$response->setStatusCode('200');
		$response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=utf-8');
		$response->headers->set('Content-Disposition', 'attachment; filename="Monitoreo '.$id.'.xlsx"');
		return $response;
	}

	
    /**
     * @Route("/monitor/csv/{id}", name="monitor_csv")
     */
    public function csvAction(Request $request, $id=null)
    {
		if ($id == null){
			throw new Exception("no id specified");
		}
		$this->_lastId = $id;
		$response = new StreamedResponse();
		
		$response->setCallback(function(){
			$file = fopen('php://output', 'w+');
			$chile = new \DateTimeZone("GMT-4"); // 'Chile/Continental' equals 'GMT-3'

			fputcsv($file, [ 'Fecha', 'Latitud', 'Longitud', 'PM10 Aire', 'PM2.5 Aire', 
                            'PM10 Camino', 'PM2.5 Camino', 'Velocidad' ]);

			$resultados = $this->getPmdata($this->_lastId);

			foreach ($resultados as $pmdata){
				$fecha = $pmdata->getIdGps()->getFecha();
				if ($fecha == null){
					$texto_fecha = "null";
				} else {
					$texto_fecha = $fecha->setTimezone($chile)->format("d-m-Y H:i:s");
				}
				fputcsv($file, [
					$texto_fecha, 
					$pmdata->getIdGps()->getLatitud(), 
					$pmdata->getIdGps()->getLongitud(),
					$pmdata->getPm10lat_u(),
					$pmdata->getPm25lat_u(),
					$pmdata->getPm10lat_d(),
					$pmdata->getPm25lat_d(),
					round($pmdata->getIdGps()->getSpeed(), 1)]);
			}			
			fclose($file);
		});
		
		$response->setStatusCode('200');
		$response->headers->set('Content-Type', 'text/csv; charset=utf-8');
		$response->headers->set('Content-Disposition', 'attachment; filename="Monitoreo '.$id.'.csv"');
		return $response;
	}


    /**
     * @Route("/monitor/daterange/{daterange}", name="monitor_daterange")
     */
    public function indexAction(Request $request, $daterange=null)
    {
		if ($daterange != null){
			$fechas = explode(",", $daterange);
			$startDate = new \DateTime($fechas[0]);
			$endDate = new \DateTime($fechas[1]);
		}
		$em = $this->getDoctrine()->getManager();
		$qb = $em->createQueryBuilder();
		$query = $qb->select('monitoreo')
			->from('AppBundle\Entity\Monitoreo','monitoreo');

		if ($daterange != null){
			$query->andWhere('monitoreo.fecha >= :startdate')
				->andWhere('monitoreo.fecha <= :enddate')
				->setParameter('startdate', $startDate)
				->setParameter('enddate', $endDate);
		}
		$query = $query->getquery();
		$results = $query->getResult();
		
		return new JsonResponse($results);
	}

    /**
     * @Route("/monitor/summary/{date}"), name="monitor_summary")
     */
	public function monitorSummaryAction(Request $request, $date=null)
	{
		if ($date == null){
			$date = new \DateTime();
		} else {
			$date = new \DateTime($date);
		}
		$qb = $this->getDoctrine()->getManager()->createqueryBuilder();
		$qb->select("pmdata")
			->from("AppBundle\Entity\Pmdata","pmdata")
			->join("pmdata.idGps","gpsdata")
			->where("gpsdata.date = :fecha")
			->setParameter("fecha", $date->format("dmy"));
		$query = $qb->getquery();
		$result = $query->getResult();

		$totalpm10 = 0;
		$totalpm25 = 0;
		$pm10_low    = 0;
		$pm10_medium = 0;
		$pm10_high   = 0;
		$pm25_low    = 0;
		$pm25_medium = 0;
		$pm25_high   = 0;
		foreach ($result as $pmdata){
			$pm25lat = $pmdata->getPm25lat_u();
			$pm10lat = $pmdata->getPm10lat_u();
			$totalpm10 += $pm10lat;
			$totalpm25 += $pm25lat;

			if ($pm10lat < 300){ 
				$pm10_low ++;
			} else {
				if ($pm10lat < 600){
					$pm10_medium ++;
				} else {
					$pm10_high ++;
				}
			}

			if ($pm25lat < 300){
				$pm25_low ++;
			} else {
				if ($pm25lat < 600){
					$pm25_medium ++;
				} else {
					$pm25_high ++;
				}
			}
		}
		if (count($result) == 0){
			return new JsonResponse(array(
				"date" => $date,
				"points" => 0
			));
		}
		return new JsonResponse(array(
			"date" => $date,
			"points" => count($result),
			"pm10_average" => floor($totalpm10 / count($result)),
			"pm25_average" => floor($totalpm25 / count($result)),
			"pm10_low" => floor($pm10_low / count($result) * 100),
			"pm10_medium" => floor($pm10_medium / count($result) * 100),
			"pm10_high" => floor($pm10_high / count($result) * 100),
			"pm25_low" => $pm25_low,
			"pm25_medium" => $pm25_medium,
			"pm25_high" => $pm25_high
		));
	}

   /**
    * @Route("/monitor/levels/dual_pm10_highchart", name="monitor_dual_levels_mp10_highchart")
    */
    public function levelsDualPm10HighchartAction(Request $request)
    {
        $chile = new \DateTimeZone("Chile/Continental"); // 'Chile/Continental' equals 'GMT-3'
        $trips = $this->getDoctrine()->getRepository('AppBundle:Monitoreo');
        $lastmon = $trips->findOneBy([], ['id' => 'DESC']);

        $em = $this->getDoctrine()->getManager();
	//$qb = $em->createQueryBuilder();
	//$query = $qb->select('monitoreo')
	//	->from('AppBundle\Entity\Monitoreo', 'monitoreo')
	//	->join('gpsdata.idSesion', 'gpsdata')
 

        $qb = $em->createQueryBuilder();
        $query = $qb->select('pmdata')
                ->from('AppBundle\Entity\Pmdata', 'pmdata')
                ->join('pmdata.idGps','gpsdata');

        $query->where('gpsdata.uuid = :id_sesion')
		->orderBy('gpsdata.fecha', 'ASC')
                ->setParameter('id_sesion', $lastmon->getUuid());
        $query = $query->getQuery();
        $pmdata_array = $query->getResult();

        $dates = [];
        $road_data = [];
        $air_data = [];
	$texto_fecha = "";
        foreach ($pmdata_array as $pmdata){
                $fecha = $pmdata->getIdGps()->getFecha();
                if ($fecha != null) {
                        //$fecha->setTimezone($chile);
                        $texto_fecha = ($fecha->getTimestamp() - (3600*4)) * 1000; //format("Y-m-d")."T".$fecha->format("H:i:s")."Z";
			if ($dates == []){ $dates[] = $texto_fecha; }
			$road_data[] = [$texto_fecha, $pmdata->getPm10lat_d()];
			$air_data[] = [$texto_fecha, $pmdata->getPm10lat_u()];
                }
        }
	$dates[] = $texto_fecha;

        return new JsonResponse(array(
		"dates" => ["start" => $dates[0], "finish" => $dates[1]],
		"road_data" => $road_data,
		"air_data" => $air_data,
	));
    }


   /**
    * @Route("/monitor/levels/dual_pm10", name="monitor_dual_levels_mp10")
    */
    public function levelsDualPm10Action(Request $request)
    {
	$chile = new \DateTimeZone("GMT-4"); // 'Chile/Continental' equals 'GMT-3'
	$trips = $this->getDoctrine()->getRepository('AppBundle:Monitoreo');
	$lastmon = $trips->findOneBy([], ['id' => 'DESC']);

	$em = $this->getDoctrine()->getManager();
	$qb = $em->createQueryBuilder();
	$query = $qb->select('pmdata')
		->from('AppBundle\Entity\Pmdata', 'pmdata')
		->join('pmdata.idGps','gpsdata');

	$query->where('gpsdata.uuid = :id_sesion')
		->setParameter('id_sesion', $lastmon->getUuid());
	$query = $query->getQuery();
	$pmdata_array = $query->getResult();

	$chartdata = [];
	foreach ($pmdata_array as $pmdata){
		$fecha = $pmdata->getIdGps()->getFecha();
		if ($fecha != null) {
			$fecha->setTimezone($chile);
			$texto_fecha = $fecha->format("d-m-Y H:i:s");
			$chartdata[] = [
				"y" => $texto_fecha,
				"pm10_up" => $pmdata->getPm10lat_u(),
				"pm10_down" => $pmdata->getPm10lat_d(),
			];
		}
	}

	return new JsonResponse($chartdata);
    }

   /**
     * @Route("/monitor/levels/pm10/{id}", name="monitor_levels_mp10")
     */
    public function levelsPm10Action(Request $request, $id = null)
    {
		$trips = $this->getDoctrine()->getRepository('AppBundle:Monitoreo');
		if ($id == null){
			$lastmon = $trips->findOneBy([],['id' => 'DESC']);
		} else {
			$lastmon = $trips->findOneBy(['id' => $id]);
			if ($lastmon == null){
				return new JsonResponse([ "error" => "No existe recorrido con ID ".$id ]);
			}
		}

		// PMDATA
		$em = $this->getDoctrine()->getManager();
		$qb = $em->createQueryBuilder();
		$query = $qb->select('pmdata')
			->from('AppBundle\Entity\Pmdata','pmdata')
			->join('pmdata.idGps','gpsdata');

		$query->where('gpsdata.uuid = :uuid')
			->andWhere('gpsdata.speed >= 0')
			->setParameter('uuid', $lastmon->getUuid());
		$query = $query->getquery();
		$results = $query->getResult();

		$alto_d = 0;
		$alto_u = 0;

		$aceptable_d = 0;
		$aceptable_u = 0;

		$bajo_d = 0;
		$bajo_u = 0;

		if ($results == []){
			return new JsonResponse(["error" => "Recorrido con ID ".$id." (".$lastmon->getUuid().") no presenta datos de GPS ni material particulado"]);
		}
		$fecha_primer_dato = $results[0]->getIdGps()->getFecha();
		foreach ($results as $pmdata){
			$pm10_d = $pmdata->getPm10lat_d();

			if ($pm10_d < 300){
				$bajo_d++;
			} else {
				if ($pm10_d < 600){
					$aceptable_d++;
				} else {
					$alto_d++;
				}
			}

			$pm10_u = $pmdata->getPm10lat_u();
			if ($pm10_u < 300){
				$bajo_u++;
			} else {
				if ($pm10_u < 600){
					$aceptable_u++;
				} else {
					$alto_u++;
				}
			}
			$test = $pmdata->getIdGps()->getFecha();
			if ($test != null){
				$fecha_ultimo_dato = $test;
			}
		}
		$total_d = $bajo_d + $aceptable_d + $alto_d;
		$total_u = $bajo_u + $aceptable_u + $alto_u;

		$chile = new \DateTimezone("Chile/Continental");
		$fecha_recorrido = $lastmon->getFecha()->setTimezone($chile);
		$fecha_primer_dato->setTimezone($chile);
		$fecha_ultimo_dato->setTimezone($chile);

		return new JsonResponse([
			"id_recorrido" => $lastmon->getId(),
			"fecha_recorrido" => $fecha_recorrido->format("d-m-Y H:i:s"),
			"fecha_primer_dato" => $fecha_primer_dato->format("d-m-Y H:i:s"),
			"fecha_ultimo_dato" => $fecha_ultimo_dato->format("d-m-Y H:i:s"),
			"pm10_down" => [
				'total' => $total_d,
				'aceptable' => $aceptable_d,
				'bajo' => $bajo_d,
				'alto' => $alto_d,
				'aceptable_pct' => round(($aceptable_d / $total_d) * 100, 1),
				'bajo_pct' => round(($bajo_d / $total_d) * 100, 1),
				'alto_pct' => round(($alto_d / $total_d) * 100, 1)
			],
			"pm10_up" => [
				'total' => $total_u,
				'aceptable' => $aceptable_u,
				'bajo' => $bajo_u,
				'alto' => $alto_u,
				'aceptable_pct' => round(($aceptable_u / $total_u) * 100, 1),
				'bajo_pct' => round(($bajo_u / $total_u) * 100, 1),
				'alto_pct' => round(($alto_u / $total_u) * 100, 1)
			]]);
	}

    /**
     * @Route("/monitor/vincenty/{id}", name="monitor_vincenty")
     */
    public function vincentyAction(Request $request, $id=null){
        if ($id == null){
            throw new Exception("no id specified");
        }
	$toolspath = realpath($this->getParameter('kernel.root_dir').'/../tools/');
	$cmd = "python ".$toolspath."/tripdistance ".$id;
	$result = intval(trim(shell_exec($cmd)));

        return new JsonResponse(["distance", $result]);
    }

    /**
     * @Route("/monitor/haversine/{id}", name="monitor_haversine")
     */
    public function haversineAction(Request $request, $id=null){
		if ($id == null){
			throw new Exception("no id specified");
		}
		$points = $this->getPmdata($id, false);
		if ($points == []){
			return new JsonResponse(["distance" => 0]);
		}

		$planet_radius = 6371000;
		$distance = 0;
		$firstpoint = array_shift($points);
		$lastlat = deg2rad($firstpoint->getIdGps()->getLatitud());
		$lastlng = deg2rad($firstpoint->getIdGps()->getLongitud());

		foreach ($points as $pmdata){
			$newlat = deg2rad($pmdata->getIdGps()->getLatitud());
			$newlng = deg2rad($pmdata->getIdGps()->getLongitud());
			$lat_d  = $newlat - $lastlat;
			$lng_d  = $newlng - $lastlng;
			$angle  = 2 * asin(sqrt(pow(sin($lat_d / 2), 2) + cos($lastlat) * cos($newlat) * pow(sin($lng_d / 2), 2)));
			$distance += $angle * $planet_radius;

			$lastlat = $newlat;
			$lastlng = $newlng;
		}
		return new JsonResponse(["distance" => $distance]);
    }


    /**
     * @Route("/monitor/nmea/{id}", name="monitor_fakegps")
     */
    public function fakegpsAction(Request $request, $id=null)
    {
		if ($id == null){
			throw new Exception("no id specified");
		}
		$this->_lastId = $id;
		$response = new StreamedResponse();
		
		$response->setCallback(function(){
			$file = fopen('php://output', 'w+');
			$chile = new \DateTimeZone("GMT-4"); // 'Chile/Continental' equals 'GMT-3'

			// false = switching off 10 km/h filter.
			$resultados = $this->getPmdata($this->_lastId, false);

			foreach ($resultados as $pmdata){
				$gprmc = $pmdata->getIdGps()->getGprmc().PHP_EOL;
				$gpgga = $pmdata->getIdGps()->getGpgga().PHP_EOL;
				
				fputs($file, $gprmc);
				fputs($file, $gpgga);
			}			
			fclose($file);
		});
		
		$response->setStatusCode('200');
		$response->headers->set('Content-Type', 'text/txt; charset=utf-8');
		$response->headers->set('Content-Disposition', 'attachment; filename="NMEA_'.$id.'.txt"');
		return $response;
	}

}
