<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use AppBundle\Entity\StationData;

class StationController extends Controller
{

	private function splitDateRange($daterange){
                if ($daterange == null){
                        $startDate = new \DateTime();
                        $endDate = new \DateTime();
                } else {
                        // Expecting "YYYY-MM-DD"
                        $range = explode(',', $daterange);
                        $startDate = new \DateTime($range[0]);
                        if (array_key_exists(1, $range)){
                                $endDate = new \DateTime($range[1]);
                        } else {
                                $endDate = new \DateTime($range[0]);
                        }
                }
                $startDate->setTime(0,0,0);
                $endDate->setTime(23,59,59);
		return array(
			"start" => $startDate,
			"end" => $endDate
		);
	}

	private function secondsToHMS($seconds){
		$H = floor($seconds / 3600);
		$i = ($seconds / 60) % 60;
		$s = $seconds % 60;
		return sprintf("%d:%02d:%02d", $H, $i, $s);
	}

	/**
	 * @Route("/geofence/log/{truck_id}/{daterange}")
	 */
/*	public function geofenceLogTruckDaterangeAction(Request $request, $truck_id, $daterange){
		$time_start = microtime();
		$em = $this->getDoctrine()->getManager();
		$daterange = $this->splitDateRange($daterange);

		$areas = $em->getRepository("AppBundle:Geofence")->findAll();
		$perm_by_area = [];
		foreach ($areas as $area){
			$perm_by_area[$area->getName()] = 0;
		}

		$events = $em->getRepository("AppBundle:GeofenceData")->findByTruckAndDateRange($truck_id, $daterange["start"], $daterange["end"]);
		$events = array_reverse($events);
		$perm = [];

		$index = count($events) - 1;
		while($index > 0){
			$currEvent = $events[$index];
			// We start reverse count with the geofence abandon event [2],
			// then look for the related geofence entering event [1]
			if ($currEvent->getEvent() == 2){
				$index = $index - 1;
				while(($events[$index]->getEvent() == 1) && ($index > 0)){
					$lastEvent = $events[$index];
					$index = $index - 1;
				}
				if ($currEvent->getGeofence() != null and $lastEvent->getGeofence() != null){
				if ($currEvent->getGeofence() == $lastEvent->getGeofence()){
					$seconds = $currEvent->getDate()->getTimestamp() - $lastEvent->getDate()->getTimestamp();
					$perm[] = [
						"enter_id" => $lastEvent->getId(),
						"leave_id" => $currEvent->getId(),
						"geofence" => $currEvent->getGeofence()->getName(),
						"enter_date" => $lastEvent->getDate()->format("Y-m-d H:i:s"),
						"leave_date" => $currEvent->getDate()->format("Y-m-d H:i:s"),
						"seconds" => $seconds
					];
					$perm_by_area[$currEvent->getGeofence()->getName()] += $seconds;
					$index = $index + 1;
				}}
			}
			$index = $index - 1;
		}

		$time_end = microtime();

		foreach($perm_by_area as $key=>$seconds){
			$perm_by_area[$key] = [
				"hms" => $this->secondsToHMS($perm_by_area[$key]),
				"seconds" => $seconds
			];
		}
		return new JsonResponse([
			"status" => "ok",
			"profiling_time_microseconds" => $time_end - $time_start,
			"daterange" => $daterange,
			"summary" => $perm_by_area,
			"permanence" => $perm,
			//"events" => $events
		]);
	}
*/
	/**
	 * @Route("/geofence/show/all/sql", name="geofence_show_all_sql")
	 */
/*	public function showAllSqlAction(Request $request){
		$em = $this->getDoctrine()->getManager();
		$polygons = $em->getRepository("AppBundle:Geofence")->findAll();

		$sql = "";
		foreach ($polygons as $polygon){
			$sql = $sql . $polygon->getSQLInsert();
		}
		return new Response($sql);
	}
*/

        /**
         * @Route("/geofence/show/areas", name="geofence_show_areas")
         */
  /*      public function showAreas(Request $request){
                $em = $this->getDoctrine()->getManager();
                $areas = $em->getRepository('AppBundle:Geofence')->findAll();

                $features = [];
                foreach ($areas as $area){
                        $feature = $area->getGeoJsonArray(array(
                                'area_id' => $area->getId(),
                                'strokeColor' => '#000000',
                                'strokeWeight' => '1.5'
                        ));
                        $features[] = $feature;
                }
                $geojson = array(
                        'type' => 'FeatureCollection',
                        'features' => $features
                );
                return new JsonResponse($geojson);
        }
*/
	/**
	 * @Route("/geofence/events/push", name="geofence_events_push")
	 */
/*	public function geofenceEventsPushAction(Request $request){
		$em = $this->getDoctrine()->getManager();
		$data = $request->request->all();
		$json = json_decode($data['json'], true);

		$registered = new \DateTime();
		$fence = $em->getRepository("AppBundle:Geofence")->findOneById($json["geofence"]);
		$event = new GeofenceData();
		$event->setGeofence($fence)
			->setVehicle($json["vehicle"])
			->setEvent($json["event"])
			->setDate(new \DateTime($json["date"]))
			->setRegistered($registered);
		$em->persist($event);
		$em->flush();
		return new JsonResponse([
			"status" => "OK",
			"registered" => $registered->format("Y-m-d H:i:s")
		]);
	}
*/

	/**
	 * @Route("/station/data/search/{number}/{daterange}", name="station_data_search")
	 */
	public function stationDataSearchAction(Request $request, $number, $daterange=null){
		$daterange = $this->splitDateRange($daterange);
		return new JsonResponse([
			"status"=>"OK",
			"number"=>$number,
			"daterange" => $daterange
		]);
	}

        /**
         * @Route("/station/data/push", name="station_data_push")
         */
        public function stationDataPushAction(Request $request){
                $data = $request->request->all();
                $json = json_decode($data['json'], true);
                $dt = new \DateTime();

		$em = $this->getDoctrine()->getManager();
		$dato = new StationData();
		$dato->setStation($json["station"])
			->setDate(new \DateTime($json["date"]))
			->setReceived($dt)
			->setLatitude($json["lat"])
			->setLongitude($json["lon"])
			->setHumidity($json["humidity"])
			->setTemperature($json["temperature"])
			->setPressure($json["pressure"])
			->setWindspeed($json["windspeed"])
			->setWinddirection($json["winddirection"])
			->setPm1ugm3($json["pm1_ugm3"])
			->setPm25ugm3($json["pm25_ugm3"])
			->setPm10ugm3($json["pm10_ugm3"])
			->setPm1pcc($json["pm1_pcc"])
			->setPm25pcc($json["pm25_pcc"])
			->setPm10pcc($json["pm10_pcc"]);
		$em->persist($dato);
		$em->flush();
                return new JsonResponse([
                        "status" => "INSERT_OK",
                        "data" => $dato
                ]);
        }

}
