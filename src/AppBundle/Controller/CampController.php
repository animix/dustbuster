<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use AppBundle\Entity\CampData;

class CampController extends Controller
{

	private function splitDateRange($daterange){
                if ($daterange == null){
                        $startDate = new \DateTime();
                        $endDate = new \DateTime();
                } else {
                        // Expecting "YYYY-MM-DD"
                        $range = explode(',', $daterange);
                        $startDate = new \DateTime($range[0]);
                        if (array_key_exists(1, $range)){
                                $endDate = new \DateTime($range[1]);
                        } else {
                                $endDate = new \DateTime($range[0]);
                        }
                }
                $startDate->setTime(0,0,0);
                $endDate->setTime(23,59,59);
		return array(
			"start" => $startDate,
			"end" => $endDate
		);
	}

	private function secondsToHMS($seconds){
		$H = floor($seconds / 3600);
		$i = ($seconds / 60) % 60;
		$s = $seconds % 60;
		return sprintf("%d:%02d:%02d", $H, $i, $s);
	}


	/**
	 * @Route("/csv/particulatemarks/{startdate}/{enddate}", name="csv_particulatemarks_startdate_enddate")
	 */
	public function csvParticulateMarksStartdateEnddateAction(Request $request, $startdate, $enddate){
		$response = new StreamedResponse();
		$this->startdate = $startdate;
		$this->enddate = $enddate;
		$response->setCallback(function(){
			$file = fopen('php://output', 'w+');
			$em = $this->getDoctrine()->getManager();
			$this->startdate_f = $this->startdate." 00:00:00";
			$this->enddate_f = $this->enddate." 00:00:00";

			// green marks
			$sql = "SELECT count(id) from evento_mapa where horasalida >= '".
                                $this->startdate_f ."' and horasalida < '". $this->enddate_f ."' and tipoevento=1 ".
                                "and polygon_id is not null and (pm10*1) < 3000";

			fputcsv($file, ["verde", "amarillo", "rojo"]);
			$statement = $em->getConnection()->prepare($sql);
			$statement->execute();

			$rows = $statement->fetchAll();
			$results = [];
			foreach ($rows as $row){
				//$results[$row['polygon_id']][] = $row['count(id)'];
				$green = $row['count(id)'];
			}


			// yellow marks
			$sql = "SELECT count(id) from evento_mapa where horasalida >= '".
                                $this->startdate_f ."' and horasalida < '". $this->enddate_f ."' and tipoevento=1 ".
                                "and polygon_id is not null and (pm10*1) >= 3000 and (pm10*1) < 5000";

			$statement = $em->getConnection()->prepare($sql);
			$statement->execute();

			$rows = $statement->fetchAll();
			$results = [];
			foreach ($rows as $row){
				//$results[$row['polygon_id']][] = $row['count(id)'];
				$yellow = $row['count(id)'];
			}



			// red marks
			$sql = "SELECT count(id) from evento_mapa where horasalida >= '".
                                $this->startdate_f ."' and horasalida < '". $this->enddate_f ."' and tipoevento=1 ".
                                "and polygon_id is not null and (pm10*1) >= 5000";

			$statement = $em->getConnection()->prepare($sql);
			$statement->execute();

			$rows = $statement->fetchAll();
			$results = [];
			foreach ($rows as $row){
				//$results[$row['polygon_id']][] = $row['count(id)'];
				$red = $row['count(id)'];
			}

			fputcsv($file, [$green, $yellow, $red]);

/*			$finalresults = [];
			foreach ($results as $key => $values){
				$average = array_sum($values) / count($values);
				$finalresults[] = [$key, $average];
				fputcsv($file, [$key, round($average)]);
			}
			*/
			fclose($file);
		});
                $response->setStatusCode('200');
                $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
                $response->headers->set('Content-Disposition','attachment; filename="particulatemarks-'.$this->startdate.'.csv"');
                return $response;
	}


	/**
	 * @Route("/csv/average/{startdate}/{enddate}", name="csv_average_startdate_enddate")
	 */
	public function csvAverageStartdateEnddateAction(Request $request, $startdate, $enddate){
		$response = new StreamedResponse();
		$this->startdate = $startdate;
		$this->enddate = $enddate;
		$response->setCallback(function(){
			$file = fopen('php://output', 'w+');
			$em = $this->getDoctrine()->getManager();
			$this->startdate_f = $this->startdate." 00:00:00";
			$this->enddate_f = $this->enddate." 00:00:00";

			$sql = "SELECT polygon_id, horasalida, pm10 from evento_mapa where horasalida >= '".
                                $this->startdate_f ."' and horasalida < '". $this->enddate_f ."' and tipoevento=1 ".
                                "and polygon_id is not null";

			fputcsv($file, ["polygon_id", "pm100"]);
			$statement = $em->getConnection()->prepare($sql);
			$statement->execute();

			$rows = $statement->fetchAll();
			$results = [];
			foreach ($rows as $row){
				$results[$row['polygon_id']][] = $row['pm10'];
			}
			$finalresults = [];
			foreach ($results as $key => $values){
				$average = array_sum($values) / count($values);
				$finalresults[] = [$key, $average];
				fputcsv($file, [$key, round($average)]);
			}
			fclose($file);
		});
                $response->setStatusCode('200');
                $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
                $response->headers->set('Content-Disposition','attachment; filename="average-'.$this->startdate.'.csv"');
                return $response;
	}

	/**
	 * @Route("/csv/full/{startdate}/{enddate}", name="csv_full_startdate_enddate")
	 */
	public function csvFullStartdateEnddateAction(Request $request, $startdate, $enddate){
		$response = new StreamedResponse();
		$this->startdate = $startdate;
		$this->enddate = $enddate;

		$response->setCallback(function(){
			$file = fopen('php://output', 'w+');

			$em = $this->getDoctrine()->getManager();
			$this->startdate_f = $this->startdate." 00:00:00";
			$this->enddate_f = $this->enddate." 00:00:00";
			$sql = "SELECT polygon_id, horasalida, pm10 from evento_mapa where horasalida >= '".
				$this->startdate_f ."' and horasalida < '". $this->enddate_f ."' and tipoevento=1 ".
				"and polygon_id is not null";
			//fputcsv($file,[$sql]);
			fputcsv($file,["polygon_id", "pm100", "fechahora"]);

			$statement = $em->getConnection()->prepare($sql);
			$statement->execute();

			$rows = $statement->fetchAll();
			foreach ($rows as $row){
				fputcsv($file, [$row['polygon_id'], round($row['pm10']), $row['horasalida']]);
			}
			fclose($file);
		});
		$response->setStatusCode('200');
		$response->headers->set('Content-Type', 'text/csv; charset=utf-8');
		$response->headers->set('Content-Disposition','attachment; filename="full-'.$this->startdate.'.csv"');
		return $response;
		//return new JsonResponse(["status" => "OK", "result" => $statement->fetchAll()]);
	}

	/**
	 * @Route("/campcsv/{vehicle}/{daterange}", name="campcsv_vehicle_daterange")
	 */
	public function campCsvVehicleDaterangeAction(Request $request, $vehicle, $daterange = null){
		$original_daterange = $daterange;
		$daterange = $this->splitDateRange($daterange);
		$em = $this->getDoctrine()->getManager();
		$data = $em->getRepository("AppBundle:CampData")->findByVehicleAndDateRange($vehicle, $daterange);

		$this->_data = $data;

                $response = new StreamedResponse();

                $response->setCallback(function(){
                        $file = fopen('php://output', 'w+');
                        $chile = new \DateTimeZone("GMT-4"); // 'Chile/Continental' equals 'GMT-3'

                        fputcsv($file, [ 'Fecha', 'Latitud', 'Longitud', 'PM100', 'Velocidad' ]);

                        $resultados = $this->_data;

                        foreach ($resultados as $campdata){
                                $fecha = $campdata->getDate();
                                if ($fecha == null){
                                        $texto_fecha = "null";
                                } else {
                                        $texto_fecha = $fecha->setTimezone($chile)->format("d-m-Y H:i:s");
                                }
                                fputcsv($file, [
                                        $texto_fecha,
                                        $campdata->getLatitude(),
                                        $campdata->getLongitude(),
                                        round($campdata->getPm10pcc()),
                                        round($campdata->getSpeed())
				]);
                        }
                        fclose($file);
                });

                $response->setStatusCode('200');
                $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
                $response->headers->set('Content-Disposition', 'attachment; filename="Vehicle-'.$vehicle.'-'.$original_daterange.'.csv"');
                return $response;


		//return new JsonResponse(["status"=>"OK"]);
	}


	/**
	 * @Route("/camp/{vehicle}/{daterange}", name="camp_vehicle_daterange")
	 */
	public function campVehicleDaterangeAction(Request $request, $vehicle, $daterange = null){
		$daterange = $this->splitDateRange($daterange);
		$em = $this->getDoctrine()->getManager();

		$data = $em->getRepository("AppBundle:CampData")->findByVehicleAndDateRange($vehicle, $daterange);

		return $this->render('default/camp.html.twig', [
			'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
			'user' => $this->getUser(),
			"vehicle" => $vehicle,
                        "daterange_start" => $daterange["start"],
			"daterange_end" => $daterange["end"],
			"matches" => count($data),
			"data" => $data
		]);

		return new JsonResponse([
			"status" => "OK",
			"vehicle" => $vehicle,
			"daterange" => $daterange,
			"matches" => count($data),
			"data" => $data
		]);
	}


	/**
	 * @Route("/stats/camp/area", name="stats_camp_area")
	 */
	public function statsCampAreaAction(Request $request){
		$data = $request->request->all();
		$json = json_decode($data['json'], true);
		$daterange_start = new \DateTime($json['daterange_start']);
		$daterange_end = new \DateTime($json['daterange_end']);
		$vertices = $json["vertices"];
		$vehicle = $json["vehicle"];

		$conn = $this->getDoctrine()->getConnection();
		$sql = "SELECT * from camp_data WHERE vehicle=". $vehicle ." AND date >= '".
			$daterange_start->format('Y-m-d H:i:s').
			"' and date <= '".
			$daterange_end->format('Y-m-d H:i:s').
			"' AND distance > 5 AND ST_CONTAINS(GeomFromText('POLYGON((".
			$vertices.
			"))'), POINT(longitude,latitude)) ORDER BY date ASC;";
		$statement = $conn->prepare($sql);
		$statement->execute();
		$results = $statement->fetchAll();

		$count_low = 0;
		$count_medium = 0;
		$count_high = 0;

		$pm10sum = 0;
		$pm25sum = 0;

		$pm10max = 0;
		$pm25max = 0;

		foreach ($results as $point){
			if ($point['pm10pcc'] < 150){
				$count_low++;
			} else if ($point['pm10pcc'] < 400){
				$count_medium++;
			} else {
				$count_high++;
			}
			$pm10sum += $point['pm10pcc'];
			$pm25sum += $point['pm25pcc'];

			if ($point['pm10pcc'] > $pm10max){
				$pm10max = $point['pm10pcc'];
			}
			if ($point['pm25pcc'] > $pm25max){
				$pm25max = $point['pm25pcc'];
			}
		}

		$matches = count($results);
		if ($matches > 0){
			$pm10avg = $pm10sum / $matches;
			$pm25avg = $pm25sum / $matches;
		} else {
			$pm10avg = 0;
			$pm25avg = 0;
		}

		return new JsonResponse([
			"sql" => $sql,
			"matches" => $matches,
			"count_low" => $count_low,
			"count_medium" => $count_medium,
			"count_high" => $count_high,
			"pm10avg" => round($pm10avg,1),
			"pm25avg" => round($pm25avg,1),
			"pm10max" => round($pm10max,1),
			"pm25max" => round($pm25max,1),
			"vehicle" => $vehicle,
			"data" => $results
		]);
	}

/*        public function areasNew(Request $request){
                $data = $request->request->all();
                $json = json_decode($data['json'], true);
                $dt = new \DateTime();
                $sql = "INSERT INTO geofence(name, pattern, date, type, geometry) VALUES('".
                        $json['name'] ."','".
                        $json['pattern'] ."','".
                        $dt->format("Y-m-d H:i:s") ."','".
                        $json['roadtype'] .
                        "',GeomFromText('POLYGON((". $json['vertices'] ."))'))";
                $conn = $this->getDoctrine()->getConnection();
                $count = $conn->executeUpdate($sql);
                return new JsonResponse([
                        "status" => "INSERT_OK",
                        "sql" => $sql
                ]);
        }
*/

        /**
         * @Route("/station/data/push", name="station_data_push")
         */
/*        public function stationDataPushAction(Request $request){
                $data = $request->request->all();
                $json = json_decode($data['json'], true);
                $dt = new \DateTime();

		$em = $this->getDoctrine()->getManager();
		$dato = new StationData();
		$dato->setStation($json["station"])
			->setDate(new \DateTime($json["date"]))
			->setReceived($dt)
			->setLatitude($json["latitude"])
			->setLongitude($json["longitude"])
			->setHumidity($json["humidity"])
			->setTemperature($json["temperature"])
			->setPressure($json["pressure"])
			->setWindspeed($json["windspeed"])
			->setWinddirection($json["winddirection"])
			->setPm10($json["pm10"])
			->setPm25($json["pm25"]);
		$em->persist($dato);
		$em->flush();
*/
                /*$sql = "INSERT INTO geofence(name, pattern, date, type, geometry) VALUES('".
                        $json['name'] ."','".
                        $json['pattern'] ."','".
                        $dt->format("Y-m-d H:i:s") ."','".
                        $json['roadtype'] .
                        "',GeomFromText('POLYGON((". $json['vertices'] ."))'))";
                $conn = $this->getDoctrine()->getConnection();
                $count = $conn->executeUpdate($sql);*/
/*                return new JsonResponse([
                        "status" => "INSERT_OK",
                        "data" => $dato
                ]);
        }
*/
}
