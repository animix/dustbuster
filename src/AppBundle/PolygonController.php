<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

use Doctrine\ORM\Query\ResultSetMapping;

use AppBundle\Entity\GpsdataRepository;

class PolygonController extends Controller
{
	private $_lastPolygon = null;
	private $_lastVertices = [];
	private $_features = [];
	private $_lastPolygonGpsData = null;
	private $_roadPolygonCount = 0;

	private function insertGeometryFromKml($kml){
		$web = $this->get('kernel')->getRootDir()."/../var/logs/dev";
		$web = "/Code/dustbuster/var/logs";
		file_put_contents($web."/fck.sql", "");


		// Removing tessellate tag just in case.  Apparently, removal has no effect.
		$xml = str_replace("<tessellate>1</tessellate>", "", $kml);
		$xml = simplexml_load_string($kml);
		$xml->registerXPathNamespace('x', 'http://www.opengis.net/kml/2.2');
		$placemarks = $xml->xpath("//kml:Placemark");
		$sql = "";
		$total_polygons_inserted = 0;
                        $conn = $this->getDoctrine()->getConnection();
                        //$conn->executeUpdate("delete from evento_mapa");
                        //$conn->executeUpdate("delete from polygon");
                        //$conn->executeUpdate("delete from geofence");
                        //$conn->executeUpdate("alter table polygon AUTO_INCREMENT=1");
                        //$conn->executeUpdate("alter table geofence AUTO_INCREMENT=1");

/*                        $count = 0;
                        foreach($inserts as $sqlinsert){
                                if (trim($sqlinsert) != "") {
                                        $conn->executeUpdate($sqlinsert);
                                        $count++;
                                }
                        }
*/
		$sqldump = "";
		foreach ($placemarks as $placemark){
			if (isset($placemark->name)){
				$name = (string) $placemark->name;
			} else {
				$name = "";
			}
			$sqlcoords = "";

			if (isset($placemark->Polygon->outerBoundaryIs->LinearRing->coordinates)){
				$coords = trim((string) $placemark->Polygon->outerBoundaryIs->LinearRing->coordinates);
				$coords = explode(" ", $coords);
				foreach ($coords as $point){
					$point = explode(",", $point);
					if (!array_key_exists(1, $point)){
						continue;
					}
					$sqlcoords = $sqlcoords .$point[0]." ".$point[1].",";
				}
				$sqlcoords = rtrim($sqlcoords, ",");
				if ($coords == ""){
					continue;
				}
			} else {
				continue;
			}

			// Descartar inmediatamente la geometria nula
			if ($sqlcoords == ""){
				continue;
			}

			//if ($name == ""){
				// Paso 1: Buscar el mismo poligono. Se inserta luego de verificar que no existe.
				$wkt = "GeomFromText(\"POLYGON((". $sqlcoords . "))\")";
				$sql = "SELECT id FROM polygon WHERE ST_EQUALS(polygon.geometry, ".$wkt.")";
				$query = $conn->executeQuery($sql);
				$result = $query->fetchAll();

				// Paso 1: Forzar INSERT, ajustar despues
				if ($result == null) {
					
					$insert = "INSERT INTO polygon(geometry) VALUES(".$wkt.")";
					$sqldump = $sqldump. $insert .PHP_EOL;
					$query = $conn->executeUpdate($insert);
					$total_polygons_inserted ++;
					file_put_contents($web."/fck.sql", $insert.";".PHP_EOL, FILE_APPEND|LOCK_EX);
					continue;
				}
				continue;

				// Paso 2: Buscar poligonos que compartan areas.
				//         - En caso de overlap, se reemplaza geometria de poligono encontrado.
				//	   - Si no hay coincidencias, se crea un nuevo poligono.
				$sql = "SELECT id FROM polygon WHERE ST_INTERSECTS(polygon.geometry, ".$wkt.")";
				$query = $conn->executeQuery($sql);
				$result = $query->fetchAll();
				if ($result != null) {
					$master_polygon = array_shift($result);
					$update = "UPDATE polygon SET geometry = ".$wkt." WHERE polygon.id = ".$master_polygon["id"];
					$query = $conn->executeUpdate($update);
					
					// Si un poligono fue modificado, se busca todos los demas poligonos que tengan overlap.
					// Los historiales de datos de cada poligono encontrado se asignan al poligono modificado,
					// y a continuacion se elimina el poligono que ahora ya no tiene historial asociado.

					foreach ($result as $polygon){
						$update = "UPDATE evento_mapa SET polygon_id=".$master_polygon['id']." WHERE polygon_id = ".$polygon["id"];
						$delete = "DELETE from polygon where id=".$polygon['id'];
						$query = $conn->executeUpdate($update);
						$query = $conn->executeUpdate($delete);
					}
					continue;
				} else {
					$insert = "INSERT INTO polygon(geometry) VALUES(".$wkt.")";
					$query = $conn->executeUpdate($insert);
					continue;
				}

				//return ($result);
			//}

			// Adivina: Seguro que en el KML tienes mas algun poligono nulo o invalido
		}
		$cleanup = "DELETE FROM polygon WHERE geometry IS NULL";
		$query = $conn->executeUpdate($cleanup);
		return $total_polygons_inserted;
	}

	private function eye3KmlToSql($kml){
		$xml = simplexml_load_string($kml);
		$xml->registerXPathNamespace('x', 'http://www.opengis.net/kml/2.2');
		$placemarks = $xml->xpath("//kml:Placemark");
		$sql = "";
		foreach ($placemarks as $placemark){
			if (isset($placemark->name)){
				$name = (string) $placemark->name;
			} else {
				$name = "";
			}
			$sqlcoords = "";
			if (isset($placemark->Polygon->outerBoundaryIs->LinearRing->coordinates)){
				$coords = trim((string) $placemark->Polygon->outerBoundaryIs->LinearRing->coordinates);
        	                $coords = explode(" ", $coords);
       	        	        foreach ($coords as $point){
               	        	        $point = explode(",", $point);
                       	        	$sqlcoords = $sqlcoords .$point[0]." ".$point[1].",";
                       		}
	                        $sqlcoords = rtrim($sqlcoords, ",");
			} else {
				continue;
			}

			// BIS: Bischofita, areas de geofencing NO_REGAR
			// SUB: Camino en subida, areas de geofencing NO_REGAR

			$sqline = "";
			if ($name == "BIS"){
				                                $sqline = "INSERT INTO geofence (pattern,type,date,geometry) VALUES('30-30','NO_REGAR',now()".
                                          ",GeomFromText(\"POLYGON((". $sqlcoords .
                                          "))\")) ON DUPLICATE KEY UPDATE geometry=GeomFromText(\"POLYGON((".
                                          $sqlcoords ."))\");";
			}
			elseif ($name == "SUB" || $name == "C"){
				$sqline = "INSERT INTO geofence (pattern,type,date,geometry) VALUES('30-30','NO_REGAR',now()".
                                          ",GeomFromText(\"POLYGON((". $sqlcoords .
                                          "))\")) ON DUPLICATE KEY UPDATE geometry=GeomFromText(\"POLYGON((".
                                          $sqlcoords ."))\");";
			} else if ($name == "BOT"){
                                $sqline = "INSERT INTO geofence (pattern,type,date,geometry) VALUES('30-30','BOTADERO',now()".
                                          ",GeomFromText(\"POLYGON((". $sqlcoords .
                                          "))\")) ON DUPLICATE KEY UPDATE geometry=GeomFromText(\"POLYGON((".
                                          $sqlcoords ."))\");";
				$sqline = $sqline . "INSERT INTO polgon(geometry) VALUES(GeomFromText(\"POLYGON((". $sqlcoords ."))\"));";
			} else {
			//if ($name != "SUB" && $name != "BIS"){
				$polygon_id = (string) $placemark->Polygon['id'];
				//echo "[".$polygon_id."],";
				if (0){//if ($polygon_id != ""){
	                        	$sqline = "INSERT INTO polygon(id, geometry) VALUES(". $polygon_id .
        	                	          ", GeomFromText(\"POLYGON((". $sqlcoords .
                		                  "))\")) ON DUPLICATE KEY UPDATE geometry=GeomFromText(\"POLYGON((".
        	        	                  $sqlcoords ."))\");";
				} else {
					$sqline = "INSERT INTO polygon(geometry) VALUES(".
                                                  "GeomFromText(\"POLYGON((". $sqlcoords .
                                                  "))\"));";
				}
			}
			if ($sqline != ""){
		                $sql = $sql.$sqline.PHP_EOL;
			}
		}
		return $sql;
	}

        private function kmlToSql($kml){
                $xml = simplexml_load_string($kml);
                $xml->registerXPathNamespace('x', 'http://www.opengis.net/kml/2.2');
                $polygons = $xml->xpath("//kml:Polygon");

                $sql = "";
                foreach ($polygons as $polygon){
			$polygon_id = trim((string)($polygon['id']));
			//$coords = trim((string) $polygon->xpath('//kml:coordinates')[0]);
			$coords = trim((string)($polygon->outerBoundaryIs->LinearRing->coordinates));
/*			foreach ($polygon->children() as $child){
				$coords = (string)($child->LinearRing->coordinates);//['outerBoundaryIs']['LinearRing']['coordinates']);
			}
*/

			$sqlcoords = "";
			$coords = explode(" ", $coords);
			foreach ($coords as $point){
				$point = explode(",", $point);
				$sqlcoords = $sqlcoords .$point[0]." ".$point[1].",";
			}
			$sqlcoords = rtrim($sqlcoords, ",");

			//$sqline = "REPLACE INTO polygon(id, geometry) VALUES(". $polygon_id .", GeomFromText(\"POLYGON((". $sqlcoords ."))\"));";
			$sqline = "INSERT INTO polygon(id, geometry) VALUES(". $polygon_id .
				  ", GeomFromText(\"POLYGON((". $sqlcoords .
				  "))\")) ON DUPLICATE KEY UPDATE geometry=GeomFromText(\"POLYGON((".
				  $sqlcoords ."))\");";
			$sql = $sql.$sqline.PHP_EOL;
			continue;

                        //$sqline = 'INSERT INTO polygon(geometry) VALUES(GeomFromText("POLYGON((';
                        foreach ($coords as $point){
                                $point = explode(",", $point);
                                $sqline = $sqline .$point[0]." ".$point[1].",";
                        }
			$sqline = rtrim($sqline, ",");
                        $sqline = $sqline.'))"));'.PHP_EOL;
			$sql .= $sqline;
                }
		return $sql;
        }

	private function getGpsDataRange($gpsdata1, $gpsdata2){
		$em = $this->getDoctrine()->getManager();
		$qb = $em->createQueryBuilder();
		$query = $qb->select('gpsdata')
			->from('AppBundle\Entity\Gpsdata','gpsdata');

		$query->where('gpsdata.id >= :gpsinicial')
			->andWhere('gpsdata.id <= :gpsfinal')
			->setParameter('gpsinicial', $gpsdata1)
			->setParameter('gpsfinal', $gpsdata2);
		$query = $query->getquery();
		return $query->getResult();
	}


	private function rot2d($sin, $cos, $xy){
		$newX = ($xy[0] * $cos) - ($xy[1] * $sin);
		$newY = ($xy[0] * $sin) + ($xy[1] * $cos);
		return [$newX, $newY];
	}

	private function isLatLngUncharted($lat, $lng){
		$sql = "SELECT count(id) as count from polygon WHERE ST_CONTAINS(polygon.geometry, 	GeomFromText('POINT(". $lng ." ". $lat .")'))";
		$rsm = new ResultSetMapping();
		$rsm->addScalarResult('count', 'count');

		$em = $this->getDoctrine()->getManager();
		$query = $em->createNativeQuery($sql, $rsm);
		$result = $query->getResult();
		
		if ($result[0]['count'] > 0) return false;
		return true;
	}

	private function isGpsDataInsideLastPolygon($gpsdata){
		if ($this->_lastPolygon == null){
			return false;
		}
		$lat = $gpsdata->getLatitud();
		$lng = $gpsdata->getLongitud();
		$sql = "SELECT ST_CONTAINS(".
			$this->_lastPolygon .", GeomFromText('POINT(".
			$lng ." ". $lat .")')) as inside";

		//$sql = "SELECT ST_CONTAINS(PolygonFromText('POLYGON((-1 -1, -1 1, 1 1, 1 -1, -1 -1))'), GeomFromText('POINT(0 0.9)')) as inside;";

		$rsm = new ResultSetMapping();
		$rsm->addScalarResult('inside', 'inside');

		$em = $this->getDoctrine()->getManager();
		$query = $em->createNativeQuery($sql, $rsm);
		$result = $query->getResult();
		
		if ($result[0]['inside'] == '1') return true;
		return false;
	}


	private function getPolygonFromPosition($gpsdata, $lastLat, $lastLng){
		$minimum_distance = 0.0002;
		$minimum_distance = 0.0003;

		$lat = $gpsdata->getLatitud();
		$lng = $gpsdata->getLongitud();
		$distance = 0.00008;  // Adjust to get bigger/smaller polygons


		// First polygon (square)
		if ($lastLat == 0){
			$this->_roadPolygonCount = 1;
			
			$a = [$lng - $distance, $lat - $distance];
			$b = [$lng + $distance, $lat - $distance];
			$c = [$lng + $distance, $lat + $distance];
			$d = [$lng - $distance, $lat + $distance];
			$e = [$lng - $distance, $lat - $distance];
			$this->_lastVertices = [$a, $b, $c, $d, $e];

			$this->_lastPolygon = "PolygonFromText('POLYGON((". 
				$a[0] ." ". $a[1] .",".
				$b[0] ." ". $b[1] .",".
				$c[0] ." ". $c[1] .",".
				$d[0] ." ". $d[1] .",".
				$e[0] ." ". $e[1] ."))')";
				
			return null; // No longer dealing with this first square
			return $this->Polygon(
				['description' => 'BEGIN',
				 'gpsid' => $gpsdata->getId(),
				 'strokeColor' => '#ff0000',
				 'strokeWeight' => '1.5',
				 'sqlspatial' => $this->_lastPolygon,
				 'kml' =>
					'<Placemark>'.PHP_EOL.
					'	<name></name>'.PHP_EOL.
					'	<styleUrl>#start</styleUrl>'.PHP_EOL.
					'	<description></description>'.PHP_EOL.
					'	<Polygon>'.PHP_EOL.
					'		<outerBoundaryIs>'.PHP_EOL.
					'			<LinearRing>'.PHP_EOL.
					'				<coordinates>'.PHP_EOL.
					'					'. $a[0] .','. $a[1] .',0'.PHP_EOL.
					'					'. $b[0] .','. $b[1] .',0'.PHP_EOL.
					'					'. $c[0] .','. $c[1] .',0'.PHP_EOL.
					'					'. $d[0] .','. $d[1] .',0'.PHP_EOL.
					'					'. $e[0] .','. $e[1] .',0'.PHP_EOL.
					'				</coordinates>'.PHP_EOL.
					'			</LinearRing>'.PHP_EOL.
					'		</outerBoundaryIs>'.PHP_EOL.
					'	</Polygon>'.PHP_EOL.
					'</Placemark>'.PHP_EOL
				],
				[ $this->_lastVertices ]);
		} else {
			$cad = ($lng - $lastLng);
			$cop = ($lat - $lastLat);
			$hip = sqrt(($cad*$cad) + ($cop*$cop));

			$distance_to_last_polygon = sqrt((($lng - $this->_lastPolygonGpsData['lng'])*($lng - $this->_lastPolygonGpsData['lng'])) 
				+ (($lat - $this->_lastPolygonGpsData['lat'])*($lat - $this->_lastPolygonGpsData['lat']	)));
			
			// Should skip polygon creation?
			if ($hip == 0) { return null; } // Please do not divide by zero
			if ($distance_to_last_polygon < $minimum_distance) { return null; } // Make sure all polygons meet a minimum distance

			$this->_lastPolygonGpsData['lat'] = $lastLat;
			$this->_lastPolygonGpsData['lng'] = $lastLng;

			
			$sin = $cad / $hip;
			$cos = $cop / $hip;
			
			$angle1 = asin($sin);
			$angle2 = acos($cos);

			$a = $this->rot2d($sin, $cos, [- $distance, - $distance]); // 2nd gpsdata - 1st polygon
			$b = $this->rot2d($sin, $cos, [+ $distance, - $distance]);
			//$c = $this->rot2d($sin, $cos, [+ ($distance*1.5), 0]); // 2nd gpsdata - 1st polygon
			$c = $this->rot2d($sin, $cos, [+ $distance, + $distance]);
			$d = $this->rot2d($sin, $cos, [- $distance, + $distance]);

			//$a = [$a[1]+ $lng, $a[0]+ $lat];
			$a = (($this->_roadPolygonCount != 1 && array_key_exists(1, $this->_lastVertices))? $this->_lastVertices[1]: [$a[1]+$lastLng, $a[0]+$lastLat]);
			$b = [$b[1]+ $lng, $b[0]+ $lat];
			$c = [$c[1]+ $lng, $c[0]+ $lat];
			$d = (($this->_roadPolygonCount != 1 && array_key_exists(2, $this->_lastVertices))? $this->_lastVertices[2]: [$d[1]+$lastLng, $d[0]+$lastLat]);
			//$d = [$d[1]+ $lng, $d[0]+ $lat];

			$this->_lastVertices = [$a, $b, $c, $d, $a];
			$this->_lastPolygonGpsData['lat'] = $lat;
			$this->_lastPolygonGpsData['lng'] = $lng;


			$this->_lastPolygon = "PolygonFromText('POLYGON((". 
				$a[0] ." ". $a[1] .",".
				$b[0] ." ". $b[1] .",".
				$c[0] ." ". $c[1] .",".
				$d[0] ." ". $d[1] .",".
				$a[0] ." ". $a[1] ."))')";
				
			$this->_roadPolygonCount++;

			return $this->Polygon(
				['description' => 'MOVING',
				 'gpsid' => $gpsdata->getId(),
				 'strokeColor' => (($this->_roadPolygonCount == 2)? '#ff0000':'#000000'),
				 'strokeWeight' => '1.5',
				 'sqlspatial' => $this->_lastPolygon,
 				 'kml' =>
					'<Placemark>'.PHP_EOL.
					'	<name></name>'.PHP_EOL.
					'	<styleUrl>#moving</styleUrl>'.PHP_EOL.
					'	<description></description>'.PHP_EOL.
					'	<Polygon>'.PHP_EOL.
					'		<outerBoundaryIs>'.PHP_EOL.
					'			<LinearRing>'.PHP_EOL.
					'				<coordinates>'.PHP_EOL.
					'					'. $a[0] .','. $a[1] .',0'.PHP_EOL.
					'					'. $b[0] .','. $b[1] .',0'.PHP_EOL.
					'					'. $c[0] .','. $c[1] .',0'.PHP_EOL.
					'					'. $d[0] .','. $d[1] .',0'.PHP_EOL.
					'					'. $a[0] .','. $a[1] .',0'.PHP_EOL.
					'				</coordinates>'.PHP_EOL.
					'			</LinearRing>'.PHP_EOL.
					'		</outerBoundaryIs>'.PHP_EOL.
					'	</Polygon>'.PHP_EOL.
					'</Placemark>'.PHP_EOL
				],
				[[ $a, $b, $c, $d, $a ]]);
		}
	}


	private function Point($properties, $lng, $lat){
		return array(
			"type" => "Feature",
			"geometry" => array(
				"type" => "Point",
				"coordinates" => array( $lng, $lat )
			),
			"properties" => $properties
		);
	}
	
	// "properties" follow Google Map properties: 
	// [ http://www.w3schools.com/googleapi/google_maps_overlays.asp ]
	private function LineString($properties, $latLngArray){
		return array(
			"type" => "Feature",
			"geometry" => array(
				"type" => "LineString",
				"coordinates" => $latLngArray
			),
			"properties" => $properties
		);
	}
	

	// "properties" follow Google Map properties: 
	// [ http://www.w3schools.com/googleapi/google_maps_overlays.asp ]
	private function Polygon($properties, $latLngArray){
		return array(
			"type" => "Feature",
			"geometry" => array(
				"type" => "Polygon",
				"coordinates" => $latLngArray
			),
			"properties" => $properties
		);
	}

	/**
	 * @Route("/polygon/show/all/kml", name="polygon_show_all_kml")
	 */
	public function showAllKmlAction(Request $request){
                $em = $this->getDoctrine()->getManager();
                $polygons = $em->getRepository('AppBundle:Polygon')->findAll();

                $features = [];
                foreach ($polygons as $polygon){
                        $feature = $polygon->getGeoJsonArray(array(
                                'polygon_id' => $polygon->getId(),
                                'strokeColor' => '#000000',
                                'strokeWeight' => '1.5',
				'kml' => $polygon->getKml()
                        ));
                        $features[] = $feature;
                }

		$this->_tmp = $features;

		$response = new StreamedResponse();
                $response->setCallback(function(){
                        $file = fopen('php://output', 'w+');
                        $chile = new \DateTimeZone("GMT-4"); // 'Chile/Continental' equals 'GMT-3'

                        // Beware: this one is a bit longer than usual.
                         fputs($file,
                                '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL.
                                '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">'.PHP_EOL.
                                '<Document>'.PHP_EOL.
                                '       <name>Polygon_show_all</name>'.PHP_EOL);

			fputs($file,
				'<Style id="moving">'.
				'	<LineStyle>'.
				'		<color>ee000000</color>'.
				'		<width>2</width>'.
				'	</LineStyle>'.
				'	<PolyStyle>'.
				'		<color>66000000</color>'.
				'		<colorMode>normal</colorMode>'.
				'		<fill>1</fill>'.
				'		<outline>1</outline>'.
				'	</PolyStyle>'.
				'</Style>'.PHP_EOL);

			foreach ($this->_tmp as $feature){
				fputs($file, $feature['properties']['kml']);
			}


                        fputs($file,
                                '       </Document>'.PHP_EOL.
                                '</kml>'.PHP_EOL);

                        fclose($file);

			$this->_tmp = null;
                });

                $response->setStatusCode('200');
                $response->headers->set('Content-Type', 'application/vnd.google-earth.kml+xml; charset=utf-8');
                $response->headers->set('Content-Disposition', 'attachment; filename="Polygon_show_all.kml"');
                return $response;
	}



	/**
     * @Route("/polygon/show/all", name="polygon_show_all")
     * @Template()
     */
	public function showAll(Request $request){
		$em = $this->getDoctrine()->getManager();
		$polygons = $em->getRepository('AppBundle:Polygon')->findAll();
		
		$features = [];
		foreach ($polygons as $polygon){
			$feature = $polygon->getGeoJsonArray(array(
				'polygon_id' => $polygon->getId(),
				'strokeColor' => '#000000',
				'strokeWeight' => '1.5',
			));
			$features[] = $feature;
		}
		// TODO: Include photodata infowindows
		$geojson = array(
			"type" => "FeatureCollection",
			"features" => $features
		);
		return new JsonResponse($geojson);
	}


	/**
     * @Route("/polygon/database/create", name="polygon_database_create")
     */
	public function polygonDatabaseCreate(Request $request){
		set_time_limit(0); // This can get slow
		$em = $this->getDoctrine()->getManager();
		$monitoreos = $em->getRepository('AppBundle:Monitoreo')->findAll();
		$em->getRepository('AppBundle:Polygon')->deleteAll();
		
		$totalPolys = 0;
		foreach ($monitoreos as $monitoreo){
			$gpsinicial = $monitoreo->getGpsinicial();
			$gpsfinal   = $monitoreo->getGpsfinal();

			// reset all internal counters
			$this->_lastPolygon = null;
			$this->_lastVertices = [];
			$this->_features = [];
			$this->_lastPolygonGpsData = null;
			$this->_roadPolygonCount = 0;
			
			$results = $this->getGpsDataRange($gpsinicial, $gpsfinal);			
			$lastLat = 0;
			$lastLng = 0;
		
			$counter = 0;
			foreach ($results as $gpsdata){
				$spacing = 1;
				
				if ($counter % $spacing != 0){ 
					$counter++; 
					continue; 
				}
				if ($this->isLatLngUncharted($gpsdata->getLatitud(), $gpsdata->getLongitud()) == false){
					$polygon = null;
				} else {
					$polygon = $this->getPolygonFromPosition($gpsdata, $lastLat, $lastLng);
				}
				if ($polygon != null){
					$em->getRepository('AppBundle:Polygon')->insert($polygon);
					$totalPolys++;
				}
				$lastLat = $gpsdata->getLatitud();
				$lastLng = $gpsdata->getLongitud();
				$counter++;
			}			
		}
		// TODO: Include photodata infowindows
		$geojson = array(
			"polygons" => $totalPolys,
		);
		return new JsonResponse($geojson);
	}


	/**
	 * @Route("/polygon/import/kml", name="polygon_import_kml")
	 */
	public function importKmlAction(Request $request){
		$files = $request->files;
		foreach ($files as $uploadedFile){
			$kml = file_get_contents($uploadedFile->getRealPath());
			//$sql = $this->kmlToSql($kml);
			//$sql = $this->eye3KmlToSql($kml);
			$result = $this->insertGeometryFromKml($kml);

			return new JsonResponse(["working"=>"yup", "result" => $result]);

$filteredsql = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($match) {
    return mb_convert_encoding(pack('H*', $match[1]), 'UTF-8', 'UCS-2BE');
}, $sql);

			$inserts = explode("\n", $filteredsql);

			$conn = $this->getDoctrine()->getConnection();
			//$conn->executeUpdate("delete from evento_mapa");
			//$conn->executeUpdate("delete from polygon");
			//$conn->executeUpdate("delete from geofence");
			//$conn->executeUpdate("alter table polygon AUTO_INCREMENT=1");
			//$conn->executeUpdate("alter table geofence AUTO_INCREMENT=1");

			$count = 0;
			foreach($inserts as $sqlinsert){
				if (trim($sqlinsert) != "") {
					$conn->executeUpdate($sqlinsert);
					$count++;
				}
			}

			return new JsonResponse([
				"status" => "OK",
				"polygon_count" => $count,
				"sql" => $sql,
				"kml" => $kml
			]);
		}
	}

	/**
	 * @Route("/polygon/new", name="polygon_new")
	 */
	public function polygonNew(Request $request){
		$data = $request->request->all();
		$coords = $data['coords'];
		//$sql = "SELECT id from polygon where ST_INTERSECTS(polygon.geometry, GeomFromText('POLYGON((". $coords ."))'))";
		$sql = "SELECT id, ST_AREA(ST_INTERSECTION(polygon.geometry, GeomFromText('POLYGON((". $coords ."))'))) AS area FROM polygon ORDER BY area DESC";

		$conn = $this->getDoctrine()->getConnection();
		$count = $conn->executeQuery($sql);
		$results = $count->fetchAll();

		if ($results == []){
			$sql = "INSERT INTO polygon(geometry) VALUES(GeomFromText('POLYGON((". $coords ."))'))";
			$count = $conn->executeUpdate($sql);
			return new JsonResponse(["status" => "INSERT_OK"]);
		}
		if ($results != [] && $results[0]['area'] < 0.00000001 ){
			$sql = "INSERT INTO polygon(geometry) VALUES(GeomFromText('POLYGON((". $coords ."))'))";
			$count = $conn->executeUpdate($sql);
			return new JsonResponse(["status" => "INSERT_OK"]);
		}
		return new JsonResponse(["status" => "INSERT_FAIL", "error" => "Proposed polygon intersects with polygon id=".$results[0]['id'].", intersection area: ". $results[0]['area']]);
	}


    /**
     * @Route("/polygon/write", name="polygon_write")
     */
	public function polygonWrite(Request $request){
		$data       = $request->request->all();
		$polygon_id = $data['polygon_id'];
		$vertices   = $data['vertices'];
		
		$sql = "";
		foreach ($vertices as $vertex){
			$sql = $sql . $vertex['lng'] ." ". $vertex['lat'] .",";
		}
		// Need to close the polygon repeating the first vertex
		$sql = $sql . $vertices[0]['lng'] ." ". $vertices[0]['lat'];
		$sql = "UPDATE polygon SET geometry=PolygonFromText('POLYGON((". $sql ."))') WHERE polygon.id=?";
		
		$conn = $this->getDoctrine()->getConnection();
		$count = $conn->executeUpdate($sql, array($polygon_id));

		return new JsonResponse(array("action"=>"polygon_write", "status" => "OK", "polygon_id" => $polygon_id, "vertices" => $vertices, "sql" => $sql, "count" => $count));
	}

	private function pmPolygonsToGeoJson($result){
		$gpsdata_grouping_time_limite = 60 * 5; // seconds
		$geojson = [];
		foreach ($result as $entry){
			$astext = substr($entry['geometry'],9,-2); // Remove 'POLYGON((' and '))'
			$vertices = split(',', $astext);
			$jsonvertices = [];
			foreach ($vertices as $pair){
				$newpair = explode(" ", $pair);
				$jsonvertices[] = [floatval($newpair[0]), floatval($newpair[1])];
			}

			// Making sure we have an entry for this polygon
			if (!array_key_exists($entry['polygon_id'], $geojson)){
				$geojson[$entry['polygon_id']] = $this->Polygon([
					'polygon_id' => $entry['polygon_id'],
					'last_timestamp' => $entry['timestamp'],
					], [ $jsonvertices ]);
			}

			if ($geojson[$entry['polygon_id']]['properties']['last_timestamp'] - $entry['timestamp'] < $gpsdata_grouping_time_limite){
				// Add information related to individual point/pmdata
				$geojson[$entry['polygon_id']]['properties']['points'][] = [
					"lat" => $entry['lat'],
					"lng" => $entry['lng'],
					"pm10lat" => $entry['pm10lat'],
					"pm25lat" => $entry['pm25lat'],
					"date" => $entry['date'],
					"timestamp" => $entry['timestamp']
				];
				$geojson[$entry['polygon_id']]['properties']['last_timestamp'] = $entry['timestamp'];
			}
		}
		$features = [];
		foreach ($geojson as $key => $value){
			// Calculate average values for pm10 / pm2.5
			$counter = 0;
			$pm10sum = 0;
			$pm25sum = 0;
			foreach ($value['properties']['points'] as $point){
				$pm10sum += $point['pm10lat'];
				$pm25sum += $point['pm25lat'];
				$counter++;
			}
			$pm10avg = $pm10sum / $counter;
			$pm25avg = $pm25sum / $counter;
			
			$value['properties']['pm10avg'] = $pm10avg;
			$value['properties']['pm25avg'] = $pm25avg;
			
			if ($pm10avg <= 299){ 
				$color = '#00FF00'; // GREEN: 0-299 pcc, no irrigation
				$irrigationLevel = 0;
			} else {
				if (($pm10avg > 299) && ($pm10avg < 600)){
					$color = '#FF8C00'; // ORANGE: 300-599 pcc, mild irrigation
					$irrigationLevel = 1;
				} else { 
					if ($pm10avg == "6527.9"){
						$color = '#FFFFFF'; // WHITE: NO DUSTMATE DATA, no irrigation
						$irrigationLevel = 0;
					} else {
						$color = '#FF0000'; // RED: 600+ pcc, full irrigation
						$irrigationLevel = 2;
					}
				}
			}
			$value['properties']['strokeColor'] = $color;
			$value['properties']['color'] = $color;
			$value['properties']['strokeWeight'] = '1.5';
			$value['properties']['fillColor'] = $color;
			$value['properties']['fillOpacity'] = 0.35;
			$value['properties']['irrigationLevel'] = $irrigationLevel;

			$features[] = $value;
		}
		return $features;
	}

/*	// "properties" follow Google Map properties: 
	// [ http://www.w3schools.com/googleapi/google_maps_overlays.asp ]
	private function Po	lygon($properties, $latLngArray){
		return array(
			"type" => "Feature",
			"geometry" => array(
				"type" => "Polygon",
				"coordinates" => $latLngArray
			),
			"properties" => $properties
		);
	}
*/	



	/**
	 * @Route("/polygon/show/all/sql", name="polygon_show_all_sql")
	 */
	public function polygonShowAllSqlAction(Request $request){
		$em = $this->getDoctrine()->getManager();
		$polygons = $em->getRepository('AppBundle:Polygon')->findAll();

		$sql = "";
		foreach ($polygons as $polygon){
			$sql = $sql . $polygon->getSQLInsert();
		}
		return new Response($sql);
	}



	/**
     * @Route("/polygon/show/last", name="polygon_show_last")
     * @Template()
     * Show PM values in polygons for the day with the most recent data
     */
	public function polygonLast(Request $request){
		$starttime = time();
		$em = $this->getDoctrine()->getManager();
		$lastuuid = $em->getRepository('AppBundle:Monitoreo')->findOneBy([], ["id" => "DESC"]);


		$sql = "SELECT gpsdata.id as gpsid, ".
			"polygon.id as polygon_id, ".
			"gpsdata.latitude as lat, ".
			"gpsdata.longitude as lng, ".
			"ST_ASTEXT(polygon.geometry) as geometry, ".
			"pmdata.pm10lat_d as pm10lat, ".
			"pmdata.pm25lat_d as pm25lat, ".
			"UNIX_TIMESTAMP(gpsdata.fecha) as timestamp, ".
			"gpsdata.fecha as date ".
			"FROM polygon JOIN gpsdata JOIN pmdata ".
			"where gpsdata.date='". $lastuuid->getFecha()->format('dmy') ."' ".
			"and pmdata.id_gps=gpsdata.id ".
			"and ST_CONTAINS(polygon.geometry, polygonfromtext(concat('point(', gpsdata.longitude, ' ', gpsdata.latitude, ')'))) ".
			"ORDER BY gpsid DESC";


		$rsm = new ResultSetMapping();
		$rsm->addScalarResult('date','date');
		$rsm->addScalarResult('timestamp','timestamp');
		$rsm->addScalarResult('gpsid', 'gpsid');
		$rsm->addScalarResult('lat', 'lat');
		$rsm->addScalarResult('lng', 'lng');
		$rsm->addScalarResult('polygon_id', 'polygon_id');
		$rsm->addScalarResult('geometry', 'geometry');
		$rsm->addScalarResult('pm10lat', 'pm10lat');
		$rsm->addScalarResult('pm25lat', 'pm25lat');

		$em = $this->getDoctrine()->getManager();
		$query = $em->createNativeQuery($sql, $rsm);
		$result = $query->getResult();


		$features = $this->pmPolygonsToGeoJson($result);
		$endtime = time();

		$geojson = array(
			"type" => "FeatureCollection",
			"features" => $features,
			"last_date" => $lastdate,
			"time_elapsed" => $endtime - $starttime
		);

		return new JsonResponse($geojson);
	}
}
