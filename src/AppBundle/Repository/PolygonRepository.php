<?php

namespace AppBundle\Repository;
use AppBundle\Entity\Polygon;
use Doctrine\ORM\Query\ResultSetMapping;

use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * PolygonRepository
 */
class PolygonRepository extends \Doctrine\ORM\EntityRepository
{
	public function deleteAll(){
		$result = $this->createQueryBuilder('polygon')
			->delete()->getQuery()->execute();
		return $result;
	}
	
	public function insert($data){
		$em = $this->getEntityManager();		
		$rsm = new ResultSetMapping();
		$sql = "INSERT INTO polygon(geometry) values(". $data['properties']['sqlspatial'] .")";
		$result = $em->getConnection()->prepare($sql)->execute();		
		$em->flush();
		return $result;
	}
	
	public function findOneByLngLat($em, $lng, $lat){
		$sql = "SELECT geometry from polygon WHERE ST_CONTAINS(polygon.geometry, GeomFromText('POINT(". $lng ." ". $lat .")')) LIMIT 1";
		$rsm = new ResultSetMapping();
		$rsm->addScalarResult('id', 'id');

		$query = $em->createNativeQuery($sql, $rsm);
		$result = $query->getResult();

		if ($result == []) return null;
		return $result[0];		
	}
	
}
